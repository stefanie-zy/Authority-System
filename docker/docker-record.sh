# docker 容器安装操作记录

# 下载镜像
docker pull adoptopenjdk/openjdk8:latest
docker pull mysql:latest
docker pull redis:latest
docker pull sjqzhang/go-fastdfs:latest
docker pull elasticsearch:7.6.2
docker pull kibana:7.6.2
docker pull logstash:7.6.2
# (nacos-2.2.3)
docker pull nacos:latest


# 容器创建

# stefanie-mysql
docker run --name stefanie-mysql -u 0 -p 3306:3306 --restart=always -e MYSQL_PASSWORD=root -v ./data:/var/lib/mysql -v /etc/localtime:/etc/localtime mysql:latest

# stefanie-redis
docker run --name stefanie-redis -u 0 -p 6379:6379 --restart=always -v ./data:/data -v /etc/localtime:/etc/localtime redis:latest

# stefanie-go-fastdfs
docker run --name stefanie-go-fastdfs -u 0 -p 3666:8080 --restart=always -v ./data:/usr/local/go-fastdfs/data sjqzhang/go-fastdfs:latest

# stefanie-nacos
# 见  ./docker-nacos.md


