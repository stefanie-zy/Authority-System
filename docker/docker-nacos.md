### 1-拉取镜像

docker pull nacos/nacos-server:latest

### 2-启动容器获取配置信息

docker run --name nacos -d nacos/nacos-server
docker cp nacos:/home/nacos/conf/ ./nacos/
docker cp nacos:/home/nacos/logs/ ./nacos/

### 3-创建数据库：选择mysql数据库创建库：nacos-config，执行mysql-schema.sql文件

### 4-修改配置文件：application.properties

spring.datasource.platform=mysql(官方配置文件缺少)
修改本地数据库地址，容器不能使用127.0.0.1

### 5-启动容器

#### 1)集群模式启动:

docker run --name nacos -u 0 -p 8848:8848 -p 9848:9848 -p 9849:9849 --restart=always -e JVM_XMS=256m -e JVM_XMX=256m -e
MODE=cluster -v ./conf/:/home/nacos/conf/ -v ./logs/:/home/nacos/logs/ nacos/nacos-server

#### 2)单机模式启动:

docker run --name stefanie-nacos -u 0 -p 8848:8848 -p 9848:9848 -p 9849:9849 --restart=always -e JVM_XMS=256m -e
JVM_XMX=256m -e MODE=standalone ./conf/:/home/nacos/conf/ -v ./logs/:/home/nacos/logs/ nacos/nacos-server