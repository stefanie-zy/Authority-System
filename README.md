# Stefanie

### 一、目录说明

~~~
com.stefanie.userserver
├── annotation                      // 注解
│       └── impl                    // 注解实现类
├── blocker                         // 拦截
│       └── aspect                  // 切面
│       └── filter                  // 过滤器
│       └── interceptor             // 拦截器
│       └── wrapper                 // 请求包分装
├── common                          // 公共
│       └── enums                   // 枚举
│           └── error               // 异常枚举
│           └── other               // 其他
│           └── sys                 // 系统模块
│       └── excel                   // Excel导入/出类
├── config                          // 配置
│       └── bean                    // 自动注入bean
│       └── filter                  // 过滤器配置
│       └── interceptor             // 拦截器配置
│       └── param                   // 配置文件参数
│       └── other                   // 系统配置
├── constant                        // 常量
│       └── bus                     // 业务
│       └── sys                     // 系统
├── controller                      // 控制层
│       └── cort                    // 核心层
│       └── sys                     // 系统
├── domain                          // 实体类
│       └── core                    // 核心公共
│       └── pojo                    // POJO
│           └── bo                  // 系统转换类
│           └── dto                 // 前端请求类
│           └── po                  // 数据库实体类
│           └── vo                  // 前端返回类
├── exception                       // 异常
│       └── sys                     // 系统异常
├── handle                          // 头部
├── init                            // 启动脚本
├── mapper                          // 持久层
│       └── sys                     // 系统
├── service                         // 服务接口层
│       └── impl                    // 实现层
│           └── sys                 // 系统
│       └── sys                     // 系统
├── utils                           // 工具类
│       └── automation              // 代码生成工具类
│       └── excel                   // Excel工具类
│       └── grep                    // 正则表达式工具类
│       └── http                    // Http工具类
│       └── ip                      // Ip工具类
│       └── mapstruct               // 对象转换
│       └── other                   // 其他
│       └── redis                   // Redis
│       └── security                // Sping Security安全工具类
│       └── uuid                    // uuid相关
resources                           
├── config                          // 配置文件
├── logback                         // 日志配置
├── lua                             // lua脚本
├── mapper                          // xml
├── templates                       // 代码生成工具模板
├──banner.txt                       // 启动Logo
~~~

### 二、注解说明

| 名称                                                   | 描述                                         |
|:-----------------------------------------------------|:-------------------------------------------|
| @Accessors(chain = true)                             | 类可连续使用.set                                 |
| @EqualsAndHashCode(callSuper = true)                 | 根据字符集公共属性生成hashCode进行比较                    |
| @NoArgsConstructor                                   | 无参构造                                       |
| @AllArgsConstructor                                  | 有参构造                                       |
| @ApiOperation(produces = "application/octet-stream") | 设置swagger可下载导出文档                           |
| @NotBlank/@NotNull                                   | @NotBlank 针对String格式<br/>@NotNull 针对基本数据类型 |

### 三、开发说明

| 序号 |     模块      | 描述                                                    |
|:--:|:-----------:|:------------------------------------------------------|
| 1  |    excel    | 1、excel导出Bean中Data数据均已String格式输出                      |
| 2  |   aspcet    | 1、接口限流  <br/>2、系统日志<br/>3、防重复                         |
| 3  |   filter    | 1、接口防重复：将http请求内容缓存下来，防止读取一次流之后，数据无法重复读取，搭配wrapper使用。 |
| 4  | interceptor | 1、接口防重复提交                                             |

### 四、异常信息

| 异常码首位  |   描述   |
|:------:|:------:|
| 0****  |  系统异常  |
| 1****  |  工具异常  |
| 2****  |  用户异常  |
| 3****  | SQL异常  |
| 4****  | 服务调用异常 |
| 5****  | 菜单角色异常 |
| 10**** |  其他异常  |

### 五、代码层次说明

| 层级  | 功能                                    |
|:---:|:--------------------------------------|
| 控制层 | 1、基础入参非空校验<br/>2、接口层异常输出<br/>3、基础权限校验 |
| 接口层 | 1、功能的独立封装                             |

### 六、版本说明

|         依赖         |    版本号     |
|:------------------:|:----------:|
|        java        |    1.8     |
|     springboot     |   2.7.6    |
|    springcloud     |  2021.0.5  |
| springcloudAlibaba | 2021.0.5.0 |
|      mybatis       |   2.3.0    |
|    mybatis-plus    |   3.5.2    |
|    maven-clear     |   3.0.0    |

### 十、注意

- 业务异常尽可能放置在最外层抛出
- 其他