-- ----------------------------
-- 系统用户：sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id`                    bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '用户ID',
  `user_name`             varchar(30)           NOT NULL                             COMMENT '用户名称',
  `nick_name`             varchar(30)           NULL DEFAULT NULL                    COMMENT '用户昵称',
  `login_name`            varchar(30)           NOT NULL                             COMMENT '用户登录名',
  `password`              varchar(100)          NOT NULL                             COMMENT '登录密码：默认123456',
  `user_type`             int                   NOT NULL DEFAULT 1                   COMMENT '用户类型：1-系统用户；2-业务用户',
  `phone_number`          varchar(50)           NULL DEFAULT NULL                    COMMENT '手机号',
  `email`                 varchar(50)           NULL DEFAULT NULL                    COMMENT '邮箱',
  `sex`                   int                   NOT NULL DEFAULT 1                   COMMENT '性别：1-男；2-女；3-未知',
  `id_card`               varchar(50)           NULL DEFAULT NULL                    COMMENT '身份证',
  `address`               varchar(255)          NULL DEFAULT NULL                    COMMENT '地址',
  `ico_url`               varchar(100)          NULL DEFAULT NULL                    COMMENT '头像地址',
  `status`                int                   NOT NULL DEFAULT 1                   COMMENT '状态：\n1-正常；2-锁定（密码输入错误6次）\n3-注销；4-停用',
  `del_flag`              int                   NOT NULL DEFAULT 1                   COMMENT '删除状态：1-未删除；2-删除',
  `login_status`          int                   NOT NULL DEFAULT 1                   COMMENT '登录状态：1-未登录；2-登录',
  `login_ip`              varchar(150)          NULL DEFAULT NULL                    COMMENT '最后一次登录ip',
  `login_time`            datetime(0)           NULL DEFAULT NULL                    COMMENT '最后一次登录时间',
  `wr_password_number`    int                   NOT NULL DEFAULT 0                   COMMENT '密码错误次数：输入错误6次账户锁定',
  `token`                 varchar(255)          NULL DEFAULT NULL                    COMMENT '用户token',
  `create_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`           datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`           datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`                varchar(500)          NULL DEFAULT NULL             COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统用户表';
SET FOREIGN_KEY_CHECKS = 1;
