-- ----------------------------
-- 系统角色-菜单关联表：sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`(
  `menu_id`               bigint(20)            NOT NULL              COMMENT '菜单Id',
  `role_id`               bigint(20)            NOT NULL              COMMENT '角色Id',
  PRIMARY KEY (role_id,menu_id) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统角色-菜单关联表'