-- ----------------------------
-- 系统用户-角色关联表：sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`(
  `user_id`               bigint(20)            NOT NULL              COMMENT '用户ID',
  `role_id`               bigint(20)            NOT NULL              COMMENT '角色Id',
  PRIMARY KEY (user_id, role_id) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统用户-角色关联表';