-- ----------------------------
-- 系统日志：sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`(
  `id`                  bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '日志Id',
  `title`               varchar(100)          NOT NULL                             COMMENT '模块标题',
  `business_type`       int                   NULL DEFAULT NULL                    COMMENT '业务类型：\n1-新增；2-修改；\n3-删除；4-其他',
  `method`              varchar(100)          NULL DEFAULT NULL                    COMMENT '方法名称',
  `request_method`      varchar(100)          NULL DEFAULT NULL                    COMMENT '请求方式',
  `login_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '操作者',
  `url`                 varchar(200)          NULL DEFAULT NULL                    COMMENT '请求url',
  `ip`                  varchar(100)          NULL DEFAULT NULL                    COMMENT 'Ip',
  `param`               varchar(2000)         NULL DEFAULT NULL                    COMMENT '请求参数',
  `result`              varchar(2000)         NULL DEFAULT NULL                    COMMENT '返回参数',
  `status`              int                   NOT NULL DEFAULT 1                   COMMENT '操作状态：1-正常；2-异常',
  `error_msg`           varchar(2000)         NULL DEFAULT NULL                    COMMENT '异常信息',
  `time`                datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '操作时间',
  `cost_time`           varchar(50)           NULL DEFAULT NULL                    COMMENT '耗时',
  `type`                int                   NOT NULL DEFAULT 1                   COMMENT '类型：1-操作日志；2-登录日志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统日志表';
SET FOREIGN_KEY_CHECKS = 1;