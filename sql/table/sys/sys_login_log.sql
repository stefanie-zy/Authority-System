-- ----------------------------
-- 系统登录日志：sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`
(
    `id`                  bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '登录日志Id',
    `login_name`          varchar(100)          NOT NULL                             COMMENT '登录名',
    `status`              int                   NOT NULL DEFAULT 1                   COMMENT '登录状态：\n1-成功；2-失败；',
    `address`             varchar(200)          NULL DEFAULT NULL                    COMMENT '登录地址',
    `ip`                  varchar(100)          NULL DEFAULT NULL                    COMMENT 'Ip',
    `time`                datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '登录时间',
    `browser`             varchar(50)           NULL DEFAULT NULL                    COMMENT '浏览器类型',
    `os`                  varchar(50)           NULL DEFAULT NULL                    COMMENT '操作系统',
    `type`                int                   NOT NULL DEFAULT 1                   COMMENT '登录类型：\n1-登录；2-退出；',

    PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统登录日志表';
SET
FOREIGN_KEY_CHECKS = 1;