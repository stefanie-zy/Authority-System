-- ----------------------------
-- 1、系统用户：sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id`                    bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '用户ID',
  `user_name`             varchar(30)           NOT NULL                             COMMENT '用户名称',
  `nick_name`             varchar(30)           NULL DEFAULT NULL                    COMMENT '用户昵称',
  `login_name`            varchar(30)           NOT NULL                             COMMENT '用户登录名',
  `password`              varchar(100)          NOT NULL                             COMMENT '登录密码：默认123456',
  `user_type`             int                   NOT NULL DEFAULT 1                   COMMENT '用户类型：1-系统用户；2-业务用户',
  `phone_number`          varchar(50)           NULL DEFAULT NULL                    COMMENT '手机号',
  `email`                 varchar(50)           NULL DEFAULT NULL                    COMMENT '邮箱',
  `sex`                   int                   NOT NULL DEFAULT 1                   COMMENT '性别：1-男；2-女；3-未知',
  `id_card`               varchar(50)           NULL DEFAULT NULL                    COMMENT '身份证',
  `address`               varchar(255)          NULL DEFAULT NULL                    COMMENT '地址',
  `ico_url`               varchar(100)          NULL DEFAULT NULL                    COMMENT '头像地址',
  `status`                int                   NOT NULL DEFAULT 1                   COMMENT '状态：\n1-正常；2-锁定（密码输入错误6次）\n3-注销；4-停用',
  `del_flag`              int                   NOT NULL DEFAULT 1                   COMMENT '删除状态：1-未删除；2-删除',
  `login_status`          int                   NOT NULL DEFAULT 1                   COMMENT '登录状态：1-未登录；2-登录',
  `login_ip`              varchar(150)          NULL DEFAULT NULL                    COMMENT '最后一次登录ip',
  `login_time`            datetime(0)           NULL DEFAULT NULL                    COMMENT '最后一次登录时间',
  `wr_password_number`    int                   NOT NULL DEFAULT 0                   COMMENT '密码错误次数：输入错误6次账户锁定',
  `token`                 varchar(255)          NULL DEFAULT NULL                    COMMENT '用户token',
  `create_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`           datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`           datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`                varchar(500)          NULL DEFAULT NULL             COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统用户表';
SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------
-- 2、系统角色：sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id`                    bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '角色Id',
  `name`                  varchar(30)           NOT NULL                             COMMENT '名称',
  `sort`                  int                   NOT NULL                             COMMENT '排序',
  `role_key`              varchar(100)          NOT NULL                             COMMENT '角色权限字符串',
  `status`                int                   NOT NULL DEFAULT 1                   COMMENT '状态：1-正常；2-停用',
  `del_flag`              int                   NULL DEFAULT 1                       COMMENT '删除状态：1-未删除；2-删除',
  `create_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`           datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`           datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`                varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统角色表';
SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- 3、系统菜单：sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id`                   bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '菜单Id',
  `name`                 varchar(30)           NOT NULL                             COMMENT '菜单名称',
  `parent_id`            bigint(20)            NOT NULL                             COMMENT '父级Id:一级菜单默认父级Id=0',
  `sort`                 int                   NOT NULL                             COMMENT '排序',
  `path`                 varchar(200)          NULL DEFAULT NULL                    COMMENT '路由地址',
  `component`            varchar(200)          NULL DEFAULT NULL                    COMMENT '组件路径',
  `query`                varchar(200)          NULL DEFAULT NULL                    COMMENT '路由参数',
  `is_frame`             int                   NULL DEFAULT 1                       COMMENT '是否为外链：1-是；2-否',
  `is_cache`             int                   NULL DEFAULT 1                       COMMENT '是否缓存：1-缓存；2-不缓存',
  `type`                 int                   NULL DEFAULT 1                       COMMENT '菜单类型：1-目录；2-菜单；3-按钮',
  `visible`              int                   NULL DEFAULT 1                       COMMENT '菜单状态（是否显示）：1-显示；2-隐藏',
  `status`               int                   NOT NULL  DEFAULT 1                  COMMENT '状态：1-正常；2-停用',
  `perms`                varchar(100)          NULL DEFAULT NULL                    COMMENT '权限标识',
  `icon`                 varchar(100)          NULL DEFAULT NULL                    COMMENT '菜单图标',
  `create_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`          datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`          datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`               varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统菜单表';
SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- 4、系统用户-角色关联表：sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`(
  `user_id`               bigint(20)            NOT NULL              COMMENT '用户ID',
  `role_id`               bigint(20)            NOT NULL              COMMENT '角色Id',
  PRIMARY KEY (user_id, role_id) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统用户-角色关联表';


-- ----------------------------
-- 5、系统角色-菜单关联表：sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`(
  `menu_id`               bigint(20)            NOT NULL              COMMENT '菜单Id',
  `role_id`               bigint(20)            NOT NULL              COMMENT '角色Id',
  PRIMARY KEY (role_id,menu_id) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统角色-菜单关联表';


-- ----------------------------
-- 6、系统日志：sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`(
  `id`                  bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '日志Id',
  `title`               varchar(100)          NOT NULL                             COMMENT '模块标题',
  `business_type`       int                   NULL DEFAULT NULL                    COMMENT '业务类型：\n1-新增；2-修改；\n3-删除；4-其他',
  `method`              varchar(100)          NULL DEFAULT NULL                    COMMENT '方法名称',
  `request_method`      varchar(100)          NULL DEFAULT NULL                    COMMENT '请求方式',
  `login_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '操作者',
  `url`                 varchar(200)          NULL DEFAULT NULL                    COMMENT '请求url',
  `ip`                  varchar(100)          NULL DEFAULT NULL                    COMMENT 'Ip',
  `param`               varchar(2000)         NULL DEFAULT NULL                    COMMENT '请求参数',
  `result`              varchar(2000)         NULL DEFAULT NULL                    COMMENT '返回参数',
  `status`              int                   NOT NULL DEFAULT 1                   COMMENT '操作状态：1-正常；2-异常',
  `error_msg`           varchar(2000)         NULL DEFAULT NULL                    COMMENT '异常信息',
  `time`                datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '操作时间',
  `cost_time`           varchar(50)           NULL DEFAULT NULL                    COMMENT '耗时',
  `type`                int                   NOT NULL DEFAULT 1                   COMMENT '类型：1-操作日志；2-登录日志',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统日志表';
SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- 7、系统登录日志：sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`
(
  `id`                  bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '登录日志Id',
  `login_name`          varchar(100)          NOT NULL                             COMMENT '登录名',
  `status`              int                   NOT NULL DEFAULT 1                   COMMENT '登录状态：\n1-成功；2-失败；',
  `address`             varchar(200)          NULL DEFAULT NULL                    COMMENT '登录地址',
  `ip`                  varchar(100)          NULL DEFAULT NULL                    COMMENT 'Ip',
  `time`                datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '登录时间',
  `browser`             varchar(50)           NULL DEFAULT NULL                    COMMENT '浏览器类型',
  `os`                  varchar(50)           NULL DEFAULT NULL                    COMMENT '操作系统',
  `type`                int                   NOT NULL DEFAULT 1                   COMMENT '登录类型：\n1-登录；2-退出；',

  PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统登录日志表';
SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- 8、系统字典类型：sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`(
  `id`                   bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '字典Id',
  `name`                 varchar(100)          NULL DEFAULT NULL                    COMMENT '名称',
  `type`                 varchar(100)          NULL DEFAULT NULL                    COMMENT '类型',
  `status`               int                   NOT NULL DEFAULT 1                   COMMENT '状态：1-正常；2-停用',
  `create_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`          datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`          datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`               varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
)ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统字典类型表';
SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- 9、系统字典数据表：sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`(
  `id`                     bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '字典ID',
  `dict_code`              varchar(100)          NOT NULL                             COMMENT '字段编码',
  `sort`                   int                   NOT NULL                             COMMENT '排序',
  `label`                  varchar(100)          NULL DEFAULT NULL                    COMMENT '标签',
  `value`                  varchar(100)          NULL DEFAULT NULL                    COMMENT '键值',
  `is_default`             int                   NOT NULL DEFAULT 1                   COMMENT '是否默认：1-否；2-是',
  `type`                   varchar(100)          NOT NULL                             COMMENT '类型',
  `status`                 int                   NOT NULL DEFAULT 1                   COMMENT '状态：1-正常；2-停用',
  `create_name`            varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`            datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`            varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`            datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`                 varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
)ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统字典数据表';
SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- 10、系统文件：sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`(
  `id`                  bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '文件ID',
  `name`                varchar(100)          NOT NULL                             COMMENT '文件名称',
  `path`                varchar(200)          NULL DEFAULT NULL                    COMMENT '文件路径',
  `url`                 varchar(200)          NULL DEFAULT NULL                    COMMENT '文件请求地址',
  `type`                varchar(100)          NULL DEFAULT NULL                    COMMENT '文件类型',
  `use_type`            int                   NULL DEFAULT 1                       COMMENT '用途类型',
  `size`                bigint(20)            NULL DEFAULT NULL                    COMMENT '文件大小',
  `download_count`      bigint(20)            NULL DEFAULT NULL                    COMMENT '下载次数',
  `md5`                 varchar(200)          NOT NULL                             COMMENT 'MD5',
  `create_name`         varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`         datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`         varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`         datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`              varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统文件表';
SET FOREIGN_KEY_CHECKS = 1;
