-- ----------------------------
-- 系统字典数据表：sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`(
  `id`                     bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '字典ID',
  `dict_code`              varchar(100)          NOT NULL                             COMMENT '字段编码',
  `sort`                   int                   NOT NULL                             COMMENT '排序',
  `label`                  varchar(100)          NULL DEFAULT NULL                    COMMENT '标签',
  `value`                  varchar(100)          NULL DEFAULT NULL                    COMMENT '键值',
  `is_default`             int                   NOT NULL DEFAULT 1                   COMMENT '是否默认：1-否；2-是',
  `type`                   varchar(100)          NOT NULL                             COMMENT '类型',
  `status`                 int                   NOT NULL DEFAULT 1                   COMMENT '状态：1-正常；2-停用',
  `create_name`            varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`            datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`            varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`            datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`                 varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
)ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统字典数据表';
SET FOREIGN_KEY_CHECKS = 1;