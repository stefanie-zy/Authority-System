-- ----------------------------
-- 系统角色：sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id`                    bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '角色Id',
  `name`                  varchar(30)           NOT NULL                             COMMENT '名称',
  `sort`                  int                   NOT NULL                             COMMENT '排序',
  `role_key`              varchar(100)          NOT NULL                             COMMENT '角色权限字符串',
  `status`                int                   NOT NULL DEFAULT 1                   COMMENT '状态：1-正常；2-停用',
  `del_flag`              int                   NULL DEFAULT 1                       COMMENT '删除状态：1-未删除；2-删除',
  `create_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`           datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`           varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`           datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`                varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统角色表';
SET FOREIGN_KEY_CHECKS = 1;
