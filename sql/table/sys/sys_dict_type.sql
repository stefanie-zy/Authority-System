-- ----------------------------
-- 系统字典类型：sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`(
  `id`                   bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '字典类型Id',
  `name`                 varchar(100)          NULL DEFAULT NULL                    COMMENT '名称',
  `type`                 varchar(100)          NULL DEFAULT NULL                    COMMENT '类型',
  `status`               int                   NOT NULL DEFAULT 1                   COMMENT '状态：1-正常；2-停用',
  `create_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`          datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`          datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`               varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
)ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统字典类型表';
SET FOREIGN_KEY_CHECKS = 1;