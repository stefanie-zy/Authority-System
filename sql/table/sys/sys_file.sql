-- ----------------------------
-- 系统文件：sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`(
  `id`                  bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '文件ID',
  `name`                varchar(100)          NOT NULL                             COMMENT '文件名称',
  `path`                varchar(200)          NULL DEFAULT NULL                    COMMENT '文件路径',
  `url`                 varchar(200)          NULL DEFAULT NULL                    COMMENT '文件请求地址',
  `type`                varchar(100)          NULL DEFAULT NULL                    COMMENT '文件类型',
  `use_type`            int                   NULL DEFAULT 1                       COMMENT '用途类型',
  `size`                bigint(20)            NULL DEFAULT NULL                    COMMENT '文件大小',
  `download_count`      bigint(20)            NULL DEFAULT NULL                    COMMENT '下载次数',
  `md5`                 varchar(200)          NOT NULL                             COMMENT 'MD5',
  `create_name`         varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`         datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`         varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`         datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`              varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统文件表';
SET FOREIGN_KEY_CHECKS = 1;