-- ----------------------------
-- 系统菜单：sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id`                   bigint(20)            NOT NULL AUTO_INCREMENT              COMMENT '菜单Id',
  `name`                 varchar(30)           NOT NULL                             COMMENT '菜单名称',
  `parent_id`            bigint(20)            NOT NULL                             COMMENT '父级Id:一级菜单默认父级Id=0',
  `sort`                 int                   NOT NULL                             COMMENT '排序',
  `path`                 varchar(200)          NULL DEFAULT NULL                    COMMENT '路由地址',
  `component`            varchar(200)          NULL DEFAULT NULL                    COMMENT '组件路径',
  `query`                varchar(200)          NULL DEFAULT NULL                    COMMENT '路由参数',
  `is_frame`             int                   NULL DEFAULT 1                       COMMENT '是否为外链：1-是；2-否',
  `is_cache`             int                   NULL DEFAULT 1                       COMMENT '是否缓存：1-缓存；2-不缓存',
  `type`                 int                   NULL DEFAULT 1                       COMMENT '菜单类型：1-目录；2-菜单；3-按钮',
  `visible`              int                   NULL DEFAULT 1                       COMMENT '菜单状态（是否显示）：1-显示；2-隐藏',
  `status`               int                   NOT NULL  DEFAULT 1                  COMMENT '状态：1-正常；2-停用',
  `perms`                varchar(100)          NULL DEFAULT NULL                    COMMENT '权限标识',
  `icon`                 varchar(100)          NULL DEFAULT NULL                    COMMENT '菜单图标',
  `create_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '创建者',
  `create_time`          datetime(0)           NULL DEFAULT CURRENT_TIMESTAMP(0)    COMMENT '创建时间',
  `update_name`          varchar(30)           NULL DEFAULT NULL                    COMMENT '更新者',
  `update_time`          datetime(0)           NULL DEFAULT NULL                    COMMENT '更新时间',
  `remark`               varchar(500)          NULL DEFAULT NULL                    COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE  = InnoDB ROW_FORMAT = Dynamic COMMENT = '系统菜单表';
SET FOREIGN_KEY_CHECKS = 1;
