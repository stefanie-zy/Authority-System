package com.stefanie.userserver.constant.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 8:52
 * @description: 静态参数-公共
 */
@ApiModel(value = "静态参数-公共")
public class CommonConstant {

    @ApiModelProperty(value = "唯一标识")
    public static final String UNIQUE = "1";
    @ApiModelProperty(value = "非唯一标识")
    public static final String NOT_UNIQUE = "2";
}
