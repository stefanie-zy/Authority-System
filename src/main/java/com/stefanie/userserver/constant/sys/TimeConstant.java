package com.stefanie.userserver.constant.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 20:56
 * @description 时间静态参数
 */
@ApiModel(value = "静态参数-时间")
public class TimeConstant implements Serializable {

    private static final long serialVersionUID = 8228143616659147795L;

    @ApiModelProperty(value = "一秒", notes = "单位：毫秒")
    public static final long MILLIS_SECOND = 1000;
    @ApiModelProperty(value = "一分钟", notes = "单位：毫秒")
    public static final long MILLIS_MINUTE = 60 * MILLIS_SECOND;
}
