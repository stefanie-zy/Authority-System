package com.stefanie.userserver.constant.sys;

import io.swagger.annotations.ApiModel;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-11 18:40
 * @description
 */
@ApiModel(value = "静态参数-缓存")
public class CacheConstant {

    public static final String CAFFEINE_LOCAL_NAME = "local_caffeine_cache";
}
