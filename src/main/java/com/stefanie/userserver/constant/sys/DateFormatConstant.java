package com.stefanie.userserver.constant.sys;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-01 22:30
 * @description 日期时间格式静态参数
 */
@ApiModel(value = "静态参数-日期时间格式")
public class DateFormatConstant implements Serializable {

    private static final long serialVersionUID = 7965949999970820183L;

    public static final String FORMAT_1 = "yyyy-MM-dd HH:mm:ss";

    public static final String FORMAT_2 = "yyyyMMddHHmmss";
}
