package com.stefanie.userserver.constant.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 18:37
 * @description 正则表达式格式参数
 */
@ApiModel(value = "静态参数-正则表达式格式")
public class GrepConstant implements Serializable {
    private static final long serialVersionUID = 937110396906368294L;

    @ApiModelProperty(value = "中文")
    public static final String CHINESE = "[一-龥]+";
    @ApiModelProperty(value = "手机号")
    public static final String PHONE_NUMBER = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(16[5,6])|(17[0-8])|(18[0-9])|(19[1、5、8、9]))\\d{8}$";
    @ApiModelProperty(value = "邮箱")
    public static final String EMAIL = "^(\\w+([-.][A-Za-z0-9]+)*){3,18}@\\w+([-.][A-Za-z0-9]+)*\\.\\w+([-.][A-Za-z0-9]+)*$";
    @ApiModelProperty(value = "身份证")
    public static final String ID_CARD = "(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|" +
            "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)";
    @ApiModelProperty(value = "字典类型")
    public static final String DICT_TYPE = "^[a-z][a-z0-9_]*$";
}
