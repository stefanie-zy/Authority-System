package com.stefanie.userserver.constant.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-10 15:13
 * @description 系统静态参数
 */
@ApiModel(value = "静态参数-其他")
public class OtherConstant implements Serializable {
    private static final long serialVersionUID = 4426268072815665882L;

    @ApiModelProperty(value = "IP地址查询")
    public static final String IP_URL = "http://whois.pconline.com.cn/ipJson.jsp";
    @ApiModelProperty(value = "内网IP")
    public static final String INTRANET_IP = "内网IP";
    @ApiModelProperty(value = "未知地址")
    public static final String UNKNOWN_ADDRESS = "XX XX";

}
