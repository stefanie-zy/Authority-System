package com.stefanie.userserver.constant.sys;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/06/06 9:21
 * @description: 编码集静态参数
 */
@ApiModel(value = "静态参数-编码字符集")
public class EncodingConstant implements Serializable {
    private static final long serialVersionUID = -1685600786885141770L;

    public static final String UTF_8 = "UTF-8";
    public static final String GBK = "GBK";
}
