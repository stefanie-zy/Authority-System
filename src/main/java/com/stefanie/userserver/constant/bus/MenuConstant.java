package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-02 19:38
 * @description
 */
@ApiModel(value = "静态参数-菜单")
public class MenuConstant implements Serializable {
    private static final long serialVersionUID = 2701278402252383656L;

    @ApiModelProperty(value = "超级父类ID")
    public static final long SUPER_PARENT_ID = 0L;
}
