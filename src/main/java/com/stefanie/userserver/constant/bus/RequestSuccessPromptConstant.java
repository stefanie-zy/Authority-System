package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 10:23
 * @description 请求成功返回提示语
 */
@ApiModel(value = "静态参数-请求成功提示")
public class RequestSuccessPromptConstant implements Serializable {

    private static final long serialVersionUID = -1027273719237742830L;

    @ApiModelProperty(value = "修改密码")
    public static final String UPDATE_PASSWORD = "温馨提示：密码已修改，下次登录生效";
    @ApiModelProperty(value = "重置密码提示")
    public static final String RESET_PASSWORD = "温馨提示：重置后的密码为：";
}
