package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 17:44
 * @description Swagger静态配置参数
 */
@ApiModel(value = "静态参数-swagger")
public class SwaggerConfigConstant implements Serializable {

    private static final long serialVersionUID = 5291948640990157643L;

    @ApiModelProperty(value = "系统接口包目录")
    public static final String SYS_PACKAGE_PATH = "com.stefanie.userserver.controller.sys";
    @ApiModelProperty(value = "系统接口名称")
    public static final String SYS_GROUP_NAME = "系统接口";

    @ApiModelProperty(value = "工具接口包目录")
    public static final String TOOL_PACKAGE_PATH = "com.stefanie.userserver.controller.tool";
    @ApiModelProperty(value = "工具接口名称")
    public static final String TOOL_GROUP_NAME = "工具接口";

}
