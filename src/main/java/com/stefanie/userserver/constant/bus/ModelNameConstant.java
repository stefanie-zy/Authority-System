package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 17:35
 * @description 系统模块名称（英文）
 */
@ApiModel(value = "静态参数-系统模块名称（英文）")
public class ModelNameConstant implements Serializable {
    private static final long serialVersionUID = 466299833297371450L;

    public static final String SQL = "SQL";

    public static final String SYS = "SYS";

}
