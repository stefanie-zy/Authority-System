package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-03 21:25
 * @description excel Excel表名静态参数
 */
@ApiModel(value = "静态参数-Excel表名")
public class ExcelNameConstant implements Serializable {
    private static final long serialVersionUID = 2070621444120563077L;

    public static final String SYS_USER = "系统用户表";

    public static final String SYS_USER_DEMO = "系统用户模板表";
}
