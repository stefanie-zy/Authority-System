package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-31 22:04
 * @description 用户静态参数
 */
@ApiModel(value = "静态参数-用户")
public class UserConstant implements Serializable {

    private static final long serialVersionUID = 8578116882431913453L;

    @ApiModelProperty(value = "超级用户-用户名")
    public static final String STEFANIE_NAME = "Stefanie";
    @ApiModelProperty(value = "超级用户-用户ID")
    public static final Long STEFANIE_ID = 1L;
    @ApiModelProperty(value = "用户允许操作")
    public static final String ALLOW = "1";
    @ApiModelProperty(value = "不允许操作")
    public static final String NOT_ALLOW = "2";

}
