package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-18 16:30
 * @description 请求异常返回提示语
 */
@ApiModel(value = "静态参数-请求异常提示")
public class RequestErrorPromptConstant implements Serializable {
    private static final long serialVersionUID = -674862488571006374L;
    @ApiModelProperty(value = "接口限流默认提示语", notes = "value==ErrorEnum.S_REQUEST_TOO_MUCH_ERROR")
    public static final String LIMIT_ERROR_MESSAGE = "请求过于频繁，请稍后重试";
    @ApiModelProperty(value = "接口防重复默认提示语", notes = "value=ErrorEnum.S_REPEAT_SUBMIT_ERROR")
    public static final String REPEAT_SUBMIT_MESSAGE = "不允许重复提交，请稍后重试";
    @ApiModelProperty(value = "Xss注入提示语")
    public static final String XSS_MESSAGE = "不允许任何脚本运行";

}
