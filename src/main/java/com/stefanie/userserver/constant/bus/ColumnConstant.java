package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 22:05
 * @description 字段静态参数
 */
@ApiModel(value = "静态参数-字段", description = "同数据库对应")
public class ColumnConstant implements Serializable {
    private static final long serialVersionUID = 346437412739941312L;

    // 用户
    @ApiModelProperty(value = "用户名称")
    public static final String USER_NAME = "user_name";
    @ApiModelProperty(value = "登录名称")
    public static final String LONIN_NAME = "login_name";
    @ApiModelProperty(value = "手机号")
    public static final String PHONE_NUMBER = "phone_number";
    @ApiModelProperty(value = "身份证")
    public static final String ID_CARD = "id_card";
    @ApiModelProperty(value = "登录状态")
    public static final String LOGIN_STATUS = "login_status";
    @ApiModelProperty(value = "删除标记")
    public static final String DEL_FLAG = "del_Flag";

    // 日志
    @ApiModelProperty(value = "IP")
    public static final String IP = "ip";
    @ApiModelProperty(value = "模块名称")
    public static final String TITLE = "title";
    @ApiModelProperty(value = "业务类型")
    public static final String BUSINESS_TYPE = "business_type";
    @ApiModelProperty(value = "状态")
    public static final String STATUS = "status";
    @ApiModelProperty(value = "时间")
    public static final String TIME = "time";
    @ApiModelProperty(value = "请求参数")
    public static final String PARAM = "param";
    @ApiModelProperty(value = "登录类型")
    public static final String TYPE = "type";

    // 角色
    @ApiModelProperty(value = "角色名称")
    public static final String NAME = "name";
    @ApiModelProperty(value = "角色权限字符串")
    public static final String ROLE_KEY = "role_key";

    //字典
    @ApiModelProperty(value = "字典名称")
    public static final String DICT_NAME = "name";
    @ApiModelProperty(value = "字典类型")
    public static final String DICT_TYPE = "type";
    @ApiModelProperty(value = "字典状态")
    public static final String DICT_STATUS = "status";
    @ApiModelProperty(value = "字典标签")
    public static final String DICT_LABEL = "label";
    @ApiModelProperty(value = "字典键值")
    public static final String DICT_VALUE = "value";
    @ApiModelProperty(value = "字典编码")
    public static final String DICT_CODE = "dict_code";

}
