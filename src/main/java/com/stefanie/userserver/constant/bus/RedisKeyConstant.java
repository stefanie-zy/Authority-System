package com.stefanie.userserver.constant.bus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-13 15:54
 * @description redis缓存key参数
 */

@ApiModel(value = "静态参数-Redis-Key")
public class RedisKeyConstant implements Serializable {
    private static final long serialVersionUID = -1888452790759113353L;

    @ApiModelProperty(value = "防重复")
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";
    @ApiModelProperty(value = "接口限流")
    public static final String LIMIT_KEY = "limit:";
    @ApiModelProperty(value = "令牌前缀")
    public static final String LOGIN_USER_KEY = "login_user_key";
}
