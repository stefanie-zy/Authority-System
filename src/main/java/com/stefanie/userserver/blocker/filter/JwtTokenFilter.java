package com.stefanie.userserver.blocker.filter;

import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.manager.TokenManager;
import com.stefanie.userserver.utils.other.StringUtil;
import com.stefanie.userserver.utils.security.SecurityUtils;
import io.swagger.annotations.ApiModel;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-05 21:45
 * @description JwtToken过滤器
 */
@Component
@ApiModel(value = "过滤器-JwtToekn")
@RequiredArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {

    private final TokenManager tokenManager;

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain filterChain) throws ServletException, IOException {
        LoginUserBo loginUserBo = tokenManager.getLoginUserBo(request);
        Authentication authentication = SecurityUtils.getAuthentication();
        // 判断是否为匿名用户或者当前系统内的用户是否为空
        if (loginUserBo != null && (StringUtil.isNull(authentication) || "anonymousUser".equals(authentication.getPrincipal()))) {
            // 更新Redis缓存
            tokenManager.updateRedisToken(loginUserBo);
            // 更新系统内的用户信息
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUserBo, null, loginUserBo.getAuthorities());
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        filterChain.doFilter(request, response);
    }
}
