package com.stefanie.userserver.blocker.filter;

import com.stefanie.userserver.blocker.wrapper.XssRequestWrapper;
import io.swagger.annotations.ApiModel;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-18 17:08
 * @description Xss过滤器
 */
@Component
@ApiModel(value = "过滤器-防Xss攻击")
public class XssFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        //传入重写后的Request
        filterChain.doFilter(new XssRequestWrapper(request), servletResponse);
    }
}

