package com.stefanie.userserver.blocker.interceptor.sub;

import cn.hutool.json.JSONUtil;
import com.stefanie.userserver.annotation.RepeatSubmit;
import com.stefanie.userserver.blocker.interceptor.StefanieInterceptor;
import com.stefanie.userserver.blocker.wrapper.RepeatRequestWrapper;
import com.stefanie.userserver.config.param.TokenConfig;
import com.stefanie.userserver.constant.bus.RedisKeyConstant;
import com.stefanie.userserver.utils.other.StringUtil;
import com.stefanie.userserver.utils.redis.RedisUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-12 20:15
 * @description 防重复拦截器
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class RepeatSubmitInterceptor extends StefanieInterceptor {

    private static final String REQUEST_PARAMS = "request_params";
    private static final String REQUEST_TIME = "request_time";

    private final RedisUtil redisUtil;

    private final TokenConfig tokenConfig;

    @Override
    public boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation) throws IOException {
        String nowParam = "";
        Map<String, Object> nowDataMap = new HashMap<String, Object>();

        nowParam = new RepeatRequestWrapper(request).getParam();
        if (StringUtil.isEmpty(nowParam)) {
            // 请求中获取Param
            nowParam = JSONUtil.toJsonStr(request.getParameterMap());
        }

        nowDataMap.put(REQUEST_PARAMS, nowParam);
        nowDataMap.put(REQUEST_TIME, System.currentTimeMillis());

        // 缓存key拼接
        String url = request.getRequestURI();
        String submitKey = StringUtil.trimToEmpty(request.getHeader(tokenConfig.getHeader()));
        String key = RedisKeyConstant.REPEAT_SUBMIT_KEY + url + submitKey;
        String redisValue = redisUtil.getStringValue(key);
        if (redisValue != null) {
            Map redisMap = JSONUtil.toBean(redisValue, Map.class);
            if (redisMap.containsKey(url)) {
                Map<String, Object> redisSessionMap = (Map<String, Object>) redisMap.get(url);
                // 参数比较、时间比较
                if (compareParam(nowDataMap, redisSessionMap) && !compareTime(nowDataMap, redisSessionMap, annotation.time())) {
                    return false;
                }
            }
        }

        // 请求未拦截刷新缓存s
        Map<String, Object> newRedisSessionMap = new HashMap<>();
        newRedisSessionMap.put(url, nowDataMap);
        redisUtil.setString(key, JSONUtil.toJsonStr(newRedisSessionMap), (long) annotation.time(), TimeUnit.SECONDS);
        return true;
    }


    /**
     * 参数比较
     */
    private boolean compareParam(Map<String, Object> nowMap, Map<String, Object> oldMap) {
        String nowParam = (String) nowMap.get(REQUEST_PARAMS);
        String oldParam = (String) oldMap.get(REQUEST_PARAMS);
        return nowParam.equals(oldParam);
    }

    /**
     * 时间比较（超过time范围请求不限制true;范围之内请求限制false）
     */
    private boolean compareTime(Map<String, Object> nowMap, Map<String, Object> oldMap, int time) {
        long nowTime = (long) nowMap.get(REQUEST_TIME);
        long oldTime = (long) oldMap.get(REQUEST_TIME);
        return (nowTime - oldTime) > time * 1000L;
    }
}
