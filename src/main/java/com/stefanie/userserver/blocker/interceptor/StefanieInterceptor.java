package com.stefanie.userserver.blocker.interceptor;

import com.stefanie.userserver.common.enums.error.ErrorEnum;
import com.stefanie.userserver.annotation.RepeatSubmit;
import com.stefanie.userserver.exception.sys.SysException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-12 19:51
 * @description 自定义拦截器
 */
@Component
@Slf4j
public abstract class StefanieInterceptor implements HandlerInterceptor {

    /**
     * 控制器方法之前执行
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            // 拦截防重复注解
            RepeatSubmit repeatSubmit = method.getAnnotation(RepeatSubmit.class);
            if (repeatSubmit != null) {
                if (!isRepeatSubmit(request, repeatSubmit)) {
                    throw new SysException(ErrorEnum.S_REPEAT_SUBMIT_ERROR.getCode(), repeatSubmit.message());
                }
                return true;
            }
            return true;
        }
        return true;
    }

    /**
     * 控制器方法之后执行
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
        return;
    }

    /**
     * 请求完成之后执行
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
        return;
    }

    /**
     * 防重复拦截器
     * false：请求重复，拦截
     * true：请求不重复，放行
     *
     * @param request
     * @param annotation
     * @return
     */
    public abstract boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation) throws IOException;
}
