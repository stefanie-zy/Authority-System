package com.stefanie.userserver.blocker.aspect;

import com.stefanie.userserver.annotation.Limit;
import com.stefanie.userserver.common.enums.error.ErrorEnum;
import com.stefanie.userserver.common.enums.error.UtilErrorEnum;
import com.stefanie.userserver.common.enums.other.LimitEnum;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.utils.ip.IpUtil;
import com.stefanie.userserver.utils.redis.RedisUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.lang.reflect.Method;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-10 15:16
 * @description 接口限流注解切面
 */
@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
@ApiModel(value = "切面-接口限流")
public class LimitAspect {

    private final RedisUtil redisUtil;

    @ApiOperation(value = "接口前置", notes = "限流处理")
    @Before("@annotation(limit)")
    public void before(JoinPoint joinPoint, Limit limit) {
        String key = limit.key();
        String message = limit.message();
        int time = limit.time();
        int count = limit.count();
        String combineKey = getCombineKey(joinPoint, limit, key);
        log.info("接口限流——combineKey:{}", combineKey);
        boolean result = false;
        try {
            result = redisUtil.limitIncr(combineKey, time, count);
        } catch (Exception e) {
            throw new SysException(UtilErrorEnum.S_UTIL_REDIS_ERROR);
        }
        if (!result) {
            log.info("接口限流——请求限流接口：{}，限流值：{}", key, count);
            throw new SysException(ErrorEnum.S_REQUEST_TOO_MUCH_ERROR.getCode(), message);
        }
    }

    @ApiOperation(value = "生成接口key")
    private String getCombineKey(JoinPoint joinPoint, Limit limit, String key) {
        StringBuilder sb = new StringBuilder(limit.key());
        if (limit.limitEnum() == LimitEnum.IP) {
            String ipAddr = IpUtil.getIpAddr((((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest()));
            sb.append(ipAddr).append("-");
        }
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Class<?> targetClass = method.getDeclaringClass();
        sb.append(targetClass.getName()).append("-").append(method.getName());
        return sb.toString();
    }

}
