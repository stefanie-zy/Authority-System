package com.stefanie.userserver.blocker.aspect;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.stefanie.userserver.annotation.Log;
import com.stefanie.userserver.common.enums.error.ErrorEnum;
import com.stefanie.userserver.common.enums.other.HttpMethodEnum;
import com.stefanie.userserver.common.enums.sys.OperationEnum;
import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.domain.pojo.po.sys.LogPo;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.mapper.sys.LogMapper;
import com.stefanie.userserver.utils.ip.IpUtil;
import com.stefanie.userserver.utils.other.ServletUtil;
import com.stefanie.userserver.utils.security.SecurityUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Map;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-25 21:16
 * @description 操作日志记录切面
 */
@Aspect
@Slf4j
@Component
@RequiredArgsConstructor
@ApiModel(value = "切面-日志")
public class LogAspect {

    private final LogMapper logMapper;

    @ApiOperation(value = "处理完请求后执行")
    @AfterReturning(pointcut = "@annotation(controllerLog)", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Log controllerLog, Object jsonResult) {
        handleLog(joinPoint, controllerLog, null, jsonResult);
    }

    @ApiOperation(value = "拦截异常操作")
    @AfterThrowing(value = "@annotation(controllerLog)", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Log controllerLog, Exception e) {
        handleLog(joinPoint, controllerLog, e, null);
    }

    @ApiOperation(value = "日志处理")
    protected void handleLog(final JoinPoint joinPoint, Log controllerLog, final Exception e, Object jsonResult) {
        try {
            // 获取当前登录用户信息
            LoginUserBo loginUserBo = SecurityUtils.getLoginUser();

            LogPo logPo = new LogPo();
            String ip = IpUtil.getIpAddr(ServletUtil.getRequest());
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            logPo.setIp(ip);
            logPo.setUrl(ServletUtil.getRequest().getRequestURI());
            if (loginUserBo != null) {
                logPo.setLoginName(loginUserBo.getUsername());
            }
            if (e != null) {
                logPo.setStatus(ErrorEnum.FAIL.getCode());
                logPo.setErrorMsg(StringUtils.substring(e.getMessage(), 0, 2000));
            }
            logPo.setMethod(className + "." + methodName + "()");
            // 处理请求参数
            logPo.setRequestMethod(ServletUtil.getRequest().getMethod());
            // 处理注解上的参数
            getControllerMethodDescription(joinPoint, controllerLog, logPo, jsonResult);
            // TODO 日志保存暂时使用同步的方式，后续调整为异步的方式
            logMapper.insert(logPo);
        } catch (Exception ex) {
            // 记录本地异常日志
            log.error("日志切面——前置通知异常,异常信息:{} ", ex.getMessage());
            ex.printStackTrace();
        }
    }

    @ApiOperation(value = "处理注解参数")
    public void getControllerMethodDescription(JoinPoint joinPoint, Log log, LogPo logPo, Object jsonResult) throws Exception {
        logPo.setBusinessType(log.businessTypeEnum().ordinal());
        logPo.setTitle(log.title());
        // 是否保存request
        if (log.isSaveRequestData()) {
            setRequestValue(joinPoint, logPo);
        }
        // 是否保存reponse
        if (log.isSaveResponseData() && jsonResult != null) {
            String str = StringUtils.substring(JSON.toJSONString(jsonResult), 0, 2000);
            Stefanie stefanie = JSONUtil.toBean(str, Stefanie.class);
            if (stefanie.getCode().equals(ErrorEnum.SUCCESS.getCode())) {
                logPo.setStatus(ErrorEnum.SUCCESS.getCode());
            } else {
                logPo.setStatus(ErrorEnum.FAIL.getCode());
                logPo.setErrorMsg(stefanie.getMessage());
            }
            logPo.setResult(str);
        }
    }

    @ApiOperation(value = "获取请求参数")
    private void setRequestValue(JoinPoint joinPoint, LogPo logPo) {
        String requestMethod = logPo.getRequestMethod();
        if (HttpMethodEnum.POST.name().equals(requestMethod) || HttpMethodEnum.PUT.name().equals(requestMethod)) {
            String params = argsArrayToString(joinPoint.getArgs());
            logPo.setParam(StringUtils.substring(params, 0, 2000));
        } else if (HttpMethodEnum.DELETE.name().equals(requestMethod) || HttpMethodEnum.GET.name().equals(requestMethod)) {
            Map<String, String[]> parameterMap = ServletUtil.getRequest().getParameterMap();
            logPo.setParam(JSONUtil.toJsonStr(parameterMap));
        } else {
            Map<?, ?> paramsMap = (Map<?, ?>) ServletUtil.getRequest().getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
            logPo.setParam(StringUtils.substring(paramsMap.toString(), 0, 2000));
        }
    }

    @ApiOperation(value = "Put、Post请求参数获取拼接")
    private String argsArrayToString(Object[] paramsArray) {
        StringBuilder params = new StringBuilder();
        if (paramsArray != null) {
            for (Object o : paramsArray) {
                if (o != null && !isFilterObject(o)) {
                    try {
                        Object jsonObj = JSON.toJSON(o);
                        params.append(jsonObj.toString()).append(" ");
                    } catch (Exception ignored) {
                    }
                }
            }
        }
        return params.toString();
    }

    @ApiOperation(value = "判断是否需要过滤的对象")
    @SuppressWarnings("rawtypes")
    public boolean isFilterObject(final Object o) {
        Class<?> clazz = o.getClass();
        if (clazz.isArray()) {
            return clazz.getComponentType().isAssignableFrom(MultipartFile.class);
        } else if (Collection.class.isAssignableFrom(clazz)) {
            Collection collection = (Collection) o;
            for (Object value : collection) {
                return value instanceof MultipartFile;
            }
        } else if (Map.class.isAssignableFrom(clazz)) {
            Map map = (Map) o;
            for (Object value : map.entrySet()) {
                Map.Entry entry = (Map.Entry) value;
                return entry.getValue() instanceof MultipartFile;
            }
        }
        return o instanceof MultipartFile || o instanceof HttpServletRequest || o instanceof HttpServletResponse || o instanceof BindingResult;
    }
}
