package com.stefanie.userserver.blocker.wrapper;

import com.stefanie.userserver.constant.sys.EncodingConstant;
import com.stefanie.userserver.utils.http.HttpStreamUtil;
import io.swagger.annotations.ApiModel;
import org.springframework.stereotype.Component;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-15 21:18
 * @description 构建可重复读取inputStream的request
 * 使用装饰模式，新增HttpServletRequestWrapper的重构，并赋予可重复读取流数据功能
 */
@ApiModel(description = "构建可重复读取inputStream的request使用装饰模式，新增HttpServletRequestWrapper的重构，并赋予可重复读取流数据功能")
public class RepeatRequestWrapper extends HttpServletRequestWrapper {

    private final byte[] body;
    private final String param;

    public RepeatRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        request.setCharacterEncoding(EncodingConstant.UTF_8);
        body = HttpStreamUtil.readParam(request).getBytes(EncodingConstant.UTF_8);
        param = new String(body, EncodingConstant.UTF_8);
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        final ByteArrayInputStream bais = new ByteArrayInputStream(body);
        return new ServletInputStream() {
            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }

            @Override
            public int read() throws IOException {
                return bais.read();
            }
        };
    }

    public byte[] getBody() {
        return body;
    }

    public String getParam() {
        return param;
    }
}
