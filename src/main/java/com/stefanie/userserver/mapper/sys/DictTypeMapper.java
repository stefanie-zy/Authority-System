package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.DictTypePo;

/**
 * @author: stefanie
 * @createTime: 2023/07/05 16:31
 * @description:
 */
public interface DictTypeMapper extends BaseMapper<DictTypePo> {
}
