package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.DictDataPo;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author: stefanie
 * @createTime: 2023/07/05 16:32
 * @description:
 */
public interface DictDataMapper extends BaseMapper<DictDataPo> {
    List<DictDataPo> listByType(@Param("type") String type);
}
