package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.MenuPo;
import com.stefanie.userserver.domain.pojo.po.sys.RoleAndMenuPo;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-02 22:25
 * @description
 */
public interface RoleAndMenuMapper extends BaseMapper<RoleAndMenuPo> {

    List<RoleAndMenuPo> listByRoleId(@Param("roleId") Long roleId);

    void deleteByRoleId(@Param("roleId") Long roleId);

    /*自定义批量插入方法比框架自带"saveBatch()"速度快*/
    void batchInsert(@Param("poList") List<RoleAndMenuPo> poList);

    List<Long> menuIdListByRoleIdIn(@Param("roleIdList") List<Long> roleIdList);

    void deleteByMenuId(@Param("menuId") Long menuId);

    void deleteByMenuIds(@Param("menuIdList") List<Long> menuIdList);
}
