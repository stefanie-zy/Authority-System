package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.MenuPo;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 16:50
 * @description
 */
public interface MenuMapper extends BaseMapper<MenuPo> {
}
