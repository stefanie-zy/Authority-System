package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.RolePo;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 17:02
 * @description
 */
public interface RoleMapper extends BaseMapper<RolePo> {
}
