package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.LogPo;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-20 19:37
 * @description
 */
public interface LogMapper extends BaseMapper<LogPo> {
}
