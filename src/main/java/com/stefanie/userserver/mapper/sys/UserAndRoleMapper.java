package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.UserAndRolePo;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-02 22:26
 * @description
 */
public interface UserAndRoleMapper extends BaseMapper<UserAndRolePo> {
    List<UserAndRolePo> listByRoleId(@Param("roleId") Long roleId);

    void deleteByRoleId(@Param("roleId") Long roleId);

    void deleteByUserId(@Param("userId") Long userId);

    /*自定义批量插入方法比框架自带"saveBatch()"速度快*/
    void batchInsert(@Param("poList") List<UserAndRolePo> poList);

    List<Long> roleIdlistByUserId(@Param("userId") Long userId);

    void deleteByUserIds(@Param("userIdList") List<Long> userIdList);
}
