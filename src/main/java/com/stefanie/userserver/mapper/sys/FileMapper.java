package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.FilePo;
import org.springframework.data.repository.query.Param;

/**
 * @author: stefanie
 * @createTime: 2023/07/13 14:29
 * @description:
 */
public interface FileMapper extends BaseMapper<FilePo> {
    void updateDownloadById(@Param("downloadCount") Long downloadCount, @Param("id") Long id);
}
