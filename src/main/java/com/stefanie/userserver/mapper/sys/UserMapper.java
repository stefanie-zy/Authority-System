package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.common.excel.UserExportExcel;
import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 17:04
 * @description 用户持久层
 */
@CacheNamespace
public interface UserMapper extends BaseMapper<UserPo> {

    List<UserExportExcel> selectExcelDataByDelFlag(@Param("delFlag") Integer delFlag);

    UserPo selectUserPoByLoginName(@Param("loginName") String loginName);

    Integer insertBatch(@Param("list") List<UserPo> poList);


}
