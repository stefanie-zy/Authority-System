package com.stefanie.userserver.mapper.sys;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stefanie.userserver.domain.pojo.po.sys.LoginLogPo;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 15:17
 * @description
 */
public interface LoginLogMapper extends BaseMapper<LoginLogPo> {
}
