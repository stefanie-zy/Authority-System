package com.stefanie.userserver.init;

import com.stefanie.userserver.common.enums.sys.*;
import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.constant.bus.UserConstant;
import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import com.stefanie.userserver.mapper.sys.UserMapper;
import com.stefanie.userserver.utils.other.EncryptionUtil;
import com.stefanie.userserver.utils.security.SecurityUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-03 21:41
 * @description 用户启动脚本
 */
@Component
@Slf4j
@ApiModel(value = "初始化-用户")
@RequiredArgsConstructor
public class UserInit implements CommandLineRunner {

    private final UserMapper userMapper;
    private final StefanieConfig stefanieConfig;

    @Override
    public void run(String... args) {
        log.info("初始化——用户信息初始化开始...");
        // 创建Stefanie用户
        creaeAdmin();
        log.info("初始化——用户信息初始化结束");
    }

    @ApiOperation(value = "新增Stefanie用户")
    private void creaeAdmin() {
        if (stefanieCheck() != null) {
            return;
        }
        log.info("用户初始化——添加Stefanie用户");
        UserPo userPo = new UserPo();
        userPo.setPassword(SecurityUtils.encryptPassword(stefanieConfig.getPassword())).setUserName(UserConstant.STEFANIE_NAME).setNickName("悄悄接").setLoginName(UserConstant.STEFANIE_NAME).setUserType(UserTypeEnum.SYS.getCode()).setPhoneNumber(EncryptionUtil.encrypt("18302912706")).setEmail(EncryptionUtil.encrypt("1342186001@qq.com")).setSex(UserSexEnum.MAN.getCode()).setIdCard(EncryptionUtil.encrypt("61272819941227021X")).setAddress(EncryptionUtil.encrypt("陕西省西安市雁塔区")).setStatus(UserStatusEnum.USABLE.getCode()).setDelFlag(DelFlagEnum.NO.getCode()).setLoginStatus(LoginStatusEnum.OUT.getCode()).setCreateName(UserConstant.STEFANIE_NAME).setCreateTime(new Date()).setId(UserConstant.STEFANIE_ID);
        userMapper.insert(userPo);
    }

    @ApiOperation(value = "查询超级用户信息")
    private UserPo stefanieCheck() {
        return userMapper.selectById(UserConstant.STEFANIE_ID);
    }
}
