package com.stefanie.userserver.config.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author: stefanie
 * @createTime: 2023/07/13 14:35
 * @description:
 */
@Component
@ConfigurationProperties(prefix = "file")
@Data
@ApiModel(value = "Yam参数-file")
public class FileConfig {

    @ApiModelProperty(value = "go-fastdfs文件管理")
    private GoFastdfs goFastdfs;

    @ApiModel(value = "go-fastdfs文件管理")
    @Data
    @ConfigurationProperties("file.go-fastdfs")
    @Configuration
    public static class GoFastdfs {
        @ApiModelProperty(value = "go-fastdfs服务地址")
        private String url;
        @ApiModelProperty(value = "go-fastdfs默认文件存储路径")
        private String defaultPath;
        @ApiModelProperty(value = "go-fastdfs输出格式")
        private String output;
        @ApiModelProperty(value = "go-fastdfs分组")
        private String group;
        @ApiModelProperty(value = "go-fastdfs上传")
        private String upload;
        @ApiModelProperty(value = "go-fastdfs删除")
        private String delete;
        @ApiModelProperty(value = "go-fastdfs文件详情")
        private String info;
        @ApiModelProperty(value = "go-fastdfs文件统计")
        private String stat;
    }
}
