package com.stefanie.userserver.config.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:12
 * @description 读取项目stefanie相关配置信息
 */
@Component
@ConfigurationProperties(prefix = "stefanie")
@Data
@ApiModel(value = "Yam参数-stefanie")
public class StefanieConfig {
    @ApiModelProperty(value = "项目名称")
    private String name;
    @ApiModelProperty(value = "项目版本")
    private String version;
    @ApiModelProperty(value = "作者")
    private String author;
    @ApiModelProperty(value = "默认密码")
    private String password;
    @ApiModelProperty(value = "默认分页查询个数")
    private Integer pageSize;
    @ApiModelProperty(value = "验证码开关")
    private Boolean verificationCode;
    @ApiModelProperty(value = "邮箱")
    private String email;
    @ApiModelProperty(value = "密码错误锁定次数")
    private Integer passwordWrNum;

}
