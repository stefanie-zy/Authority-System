package com.stefanie.userserver.config.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 20:51
 * @description 读取项目token相关配置信息
 */
@Component
@ConfigurationProperties(prefix = "token")
@Data
@ApiModel(value = "Yml参数-Token")
public class TokenConfig {
    @ApiModelProperty(value = "头部")
    private String header;
    @ApiModelProperty(value = "过期时间")
    private int expireTime;
    @ApiModelProperty(value = "秘钥")
    private String secretKey;
}
