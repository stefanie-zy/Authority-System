package com.stefanie.userserver.config.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import org.springframework.stereotype.Component;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-11 15:09
 * @description 接口限流相关脚本
 */
@Configuration
@Deprecated
@ApiModel(value = "配置-Redis-lua脚本调用")
public class RedisScript {

    protected final String limitPath = "./lua/limit.lua";

    @ApiOperation(value = "使用Redis执行lua脚本")
    @Bean
    public DefaultRedisScript<Long> limitScript() {
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
        redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource(limitPath)));
        redisScript.setResultType(Long.class);
        return redisScript;
    }

}
