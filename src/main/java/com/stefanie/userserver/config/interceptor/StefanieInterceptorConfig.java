package com.stefanie.userserver.config.interceptor;

import com.stefanie.userserver.blocker.interceptor.StefanieInterceptor;
import io.swagger.annotations.ApiModel;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-13 17:03
 * @description 自定义拦截器
 */
@ApiModel(value = "配置-自定义拦截器")
@Configuration
@RequiredArgsConstructor
public class StefanieInterceptorConfig implements WebMvcConfigurer {

    private final StefanieInterceptor stefanieInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(stefanieInterceptor).addPathPatterns("/**");
    }


}