package com.stefanie.userserver.config.other;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.google.common.collect.Lists;
import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.constant.bus.SwaggerConfigConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:10
 * @description swagger配置类
 */
@Component
@EnableSwagger2
@EnableKnife4j
@RequiredArgsConstructor
@ApiModel(value = "配置-swagger")
public class SwaggerConfig {

    private final StefanieConfig stefanieConfig;

    @Value("${swagger.enabled}")
    @ApiModelProperty(value = "是否开启swagger文档")
    private boolean enabled;

    @Bean(value = "sysDoc")
    public Docket sysApi() {
        return createRestApi(SwaggerConfigConstant.SYS_PACKAGE_PATH, SwaggerConfigConstant.SYS_GROUP_NAME, SwaggerConfigConstant.SYS_GROUP_NAME);
    }

    @Bean(value = "toolDoc")
    public Docket toolApi() {
        return createRestApi(SwaggerConfigConstant.TOOL_PACKAGE_PATH, SwaggerConfigConstant.TOOL_GROUP_NAME, SwaggerConfigConstant.TOOL_GROUP_NAME);
    }

    private Docket createRestApi(String packagePath, String desc, String groupName) {
        List<ResponseMessage> responseMessageList = Lists.newArrayList();

        return new Docket(DocumentationType.SWAGGER_2)
                // 设置请求类型
                .globalResponseMessage(RequestMethod.GET, responseMessageList).globalResponseMessage(RequestMethod.POST, responseMessageList).globalResponseMessage(RequestMethod.DELETE, responseMessageList).globalResponseMessage(RequestMethod.PUT, responseMessageList)
                // 是否开启swagger
                .enable(enabled)
                // 创建Api基本信息
                .apiInfo(apiInfo(desc))
                // 设置哪些接口暴露
                .select()
                // 设置所有注解的Api
                .apis(RequestHandlerSelectors.basePackage(packagePath)).paths(PathSelectors.any()).build().groupName(groupName)
                // 设置请求token
                .securitySchemes(securitySchemes())
                // 安全上下文
                .securityContexts(securityContexts());
    }

    @ApiOperation(value = "安全模式，通过token访问接口")
    private List<SecurityScheme> securitySchemes() {
        List<SecurityScheme> apiKeyList = new ArrayList<SecurityScheme>();
        apiKeyList.add(new ApiKey("Authorization", "Authorization", In.HEADER.toValue()));
        return apiKeyList;
    }

    @ApiOperation(value = "安全上下文")
    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts = new ArrayList<>();
        securityContexts.add(SecurityContext.builder().securityReferences(defaultAuth()).operationSelector(o -> o.requestMappingPattern().matches("/.*")).build());
        return securityContexts;
    }

    @ApiOperation(value = "默认的安全上引用")
    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences = new ArrayList<>();
        securityReferences.add(new SecurityReference("Authorization", authorizationScopes));
        return securityReferences;
    }

    @ApiOperation(value = "设置文档基础信息")
    private ApiInfo apiInfo(String desc) {
        return new ApiInfoBuilder().title("标题：Stefanie系统——接口文档").description(desc).contact(new Contact(stefanieConfig.getAuthor(), null, stefanieConfig.getEmail())).version("版本号" + stefanieConfig.getVersion()).build();
    }
}
