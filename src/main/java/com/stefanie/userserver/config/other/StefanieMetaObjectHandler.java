package com.stefanie.userserver.config.other;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import io.swagger.annotations.ApiModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-18 21:58
 * @description
 */
@ApiModel(value = "配置-字段默认值设置")
@Configuration
@Slf4j
public class StefanieMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        // 设置新增时间
        if (metaObject.hasSetter("createTime")) {
            Object createTime = getFieldValByName("createTime", metaObject);
            if (createTime == null) {
                this.strictInsertFill(metaObject, "createTime", Date.class, new Date());
                log.info("字段默认值——设置创建时间字段值为空,已经填充 ");
            }
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        // 设置新增时间
        if (metaObject.hasSetter("updateTime")) {
            Object updateTime = getFieldValByName("updateTime", metaObject);
            if (updateTime == null) {
                this.strictInsertFill(metaObject, "updateTime", Date.class, new Date());
                log.info("字段默认值——设置修改时间字段值为空,已经填充 ");
            }
        }
    }
}
