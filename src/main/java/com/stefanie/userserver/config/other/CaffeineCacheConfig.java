package com.stefanie.userserver.config.other;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.stefanie.userserver.constant.sys.CacheConstant;
import io.swagger.annotations.ApiModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-11 11:08
 * @description
 */
@EnableCaching
@Configuration
@ApiModel(value = "配置-Caffeine-一级缓存")
public class CaffeineCacheConfig {

    protected static final long MAXIMUM_SIZE = 500;
    protected static final long EXPIRE_AFTER_ACCESS = 600;

    @Bean
    public CacheManager localEntityCacheManager() {
        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
        Caffeine<Object, Object> caffeine = Caffeine.newBuilder()
                // 初始大小
                .initialCapacity(10)
                // 最大容量
                .maximumSize(MAXIMUM_SIZE)
                // 打开统计
                .recordStats()
                // 5分钟不访问自动丢弃
                .expireAfterAccess(EXPIRE_AFTER_ACCESS, TimeUnit.MINUTES);
        caffeineCacheManager.setCaffeine(caffeine);
        // 设定缓存器名称
        caffeineCacheManager.setCacheNames(getNames());
        // 值不可为空
        caffeineCacheManager.setAllowNullValues(false);
        return caffeineCacheManager;
    }

    private static List<String> getNames() {
        List<String> names = new ArrayList<>(1);
        names.add(CacheConstant.CAFFEINE_LOCAL_NAME);
        return names;
    }


}
