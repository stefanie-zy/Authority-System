package com.stefanie.userserver.config.filter;

import com.stefanie.userserver.blocker.filter.RepeatFilter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-17 17:33
 * @description 防重复过滤器配置类
 */
@Configuration
@ApiModel(value = "配置-防重复过滤器")
public class RepeatConfig {

    @Bean
    public FilterRegistrationBean httpServletRequestReplacedRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new RepeatFilter());
        registration.addUrlPatterns("/*");
        registration.setName("repeatFilter");
        registration.setOrder(1);
        return registration;
    }

}
