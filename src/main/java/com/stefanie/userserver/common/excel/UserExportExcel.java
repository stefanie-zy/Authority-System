package com.stefanie.userserver.common.excel;

import com.stefanie.userserver.annotation.ExcelExport;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-01 20:43
 * @description 系统用户导出Bean模板
 */
@Data
@ApiModel(value = "系统用户导出Excel模板")
public class UserExportExcel implements Serializable {
    private static final long serialVersionUID = 5939192168848632905L;
    @ExcelExport(value = "用户名称", example = "张三")
    private String userName;
    @ExcelExport(value = "用户昵称", example = "悄悄接")
    private String nickName;
    @ExcelExport(value = "登录名称", example = "test")
    private String loginName;
    @ExcelExport(value = "用户类型", kv = "1-系统用户;2-业务用户", example = "系统用户")
    private Integer userType;
    @ExcelExport(value = "手机号", example = "13333333333")
    private String phoneNumber;
    @ExcelExport(value = "邮箱", example = "13333333333@qq.com")
    private String email;
    @ExcelExport(value = "性别", kv = "1-男;2-女;3-未知", example = "男")
    private Integer sex;
    @ExcelExport(value = "身份证号", example = "133333333333333333")
    private String idCard;
    @ExcelExport(value = "住址", example = "北京市朝阳区xxx路xxx号")
    private String address;
    @ExcelExport(value = "状态", kv = "1-正常;2-锁定;3-注销", example = "正常")
    private Integer status;
    @ExcelExport(value = "创建者", example = "张三")
    private String createName;
    @ExcelExport(value = "创建时间", example = "2023-01-01 00:00:00")
    private Date createTime;
    @ExcelExport(value = "更新者", example = "张三")
    private String updateName;
    @ExcelExport(value = "更新时间", example = "2023-01-01 00:00:00")
    private Date updateTime;
    @ExcelExport(value = "备注", example = "暂无")
    private String remark;
}
