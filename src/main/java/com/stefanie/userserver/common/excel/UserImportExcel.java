package com.stefanie.userserver.common.excel;

import com.stefanie.userserver.annotation.ExcelExport;
import com.stefanie.userserver.annotation.ExcelImport;
import com.stefanie.userserver.annotation.ExcelImport;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-01 20:43
 * @description 系统用户导入Bean
 */
@Data
@ApiModel(value = "系统用户导入Excel模板")
public class UserImportExcel implements Serializable {
    private static final long serialVersionUID = 4421297630574281317L;

    @ExcelImport(value = "用户名称", unique = true, required = true)
    private String userName;
    @ExcelImport(value = "登录名称", unique = true, required = true)
    private String loginName;
    @ExcelImport(value = "用户类型", kv = "1-系统用户;2-业务用户", required = true)
    private Integer userType;
    @ExcelImport(value = "手机号")
    private String phoneNumber;
    @ExcelImport(value = "邮箱")
    private String email;
    @ExcelImport(value = "性别", kv = "1-男;2-女;3-未知")
    private Integer sex;
    @ExcelImport(value = "身份证号")
    private String idCard;
    @ExcelImport(value = "住址")
    private String address;
    @ExcelExport(value = "备注")
    private String remark;
}
