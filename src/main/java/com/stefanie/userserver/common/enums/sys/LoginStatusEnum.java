package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: stefanie
 * @createTime: 2023/06/02 11:32
 * @description: 登录状态枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-登录状态")
public enum LoginStatusEnum implements CommonEnum {

    IN(1, "登录"),
    OUT(2, "未登录"),
    ;

    private Integer code;
    private String message;

}
