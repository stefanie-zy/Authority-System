package com.stefanie.userserver.common.enums.error;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-07 22:19
 * @description 数据库查询错误异常枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-SQL异常")
public enum SqlErrorEnum implements CommonEnum {

    S_ONE_DATE_SELECT_ERROR(3000, "SQL异常：数据库单条数据查询异常"),
    ;
    private Integer code;
    private String message;
}
