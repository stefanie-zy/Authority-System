package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 21:32
 * @description 删除枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-删除标记")
public enum DelFlagEnum implements CommonEnum {

    NO(1,"未删除"),
    YES(2,"删除"),
    ;

    private Integer code;
    private String message;
}
