package com.stefanie.userserver.common.enums.error;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-24 15:35
 * @description 公共异常枚举值
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-异常枚举")
public enum ErrorEnum implements CommonEnum {

    SUCCESS(200, "成功"),
    BAD_REQUEST(400, "系统异常：错误的请求"),
    NOT_FOUND(404, "系统异常：请求地址不存在"),
    FAIL(500, "系统异常：系统繁忙"),

    S_ENUM_FORMAT_ERROR(600, "枚举值转换异常"),
    S_REQUEST_TOO_MUCH_ERROR(602, "请求过于频繁，请稍后重试"),
    S_LIMIT_ERROR(603, "服务器限流异常，请稍后重试"),
    S_REPEAT_SUBMIT_ERROR(604, "不允许重复提交，请稍后重试"),

    P_PARAM_CHECK_ERROR_ENUM(701, "参数校验异常"),
    P_FILE_EMPTY(702, "文件为空"),
    P_FILE_READ_ERROR(703,"Excel文件读取异常"),
    ;

    private Integer code;
    private String message;

}
