package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 14:44
 * @description:
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-是否默认标记")
public enum DefaultEnum implements CommonEnum {
    NO(1, "否"),
    YES(2, "是"),
    ;

    private Integer code;
    private String message;
}
