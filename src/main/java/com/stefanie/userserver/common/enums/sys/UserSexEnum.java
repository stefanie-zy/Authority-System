package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 20:31
 * @description 用户性别枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-性别")
public enum UserSexEnum implements CommonEnum {

    MAN(1, "男"),
    WOMEN(2, "女"),
    UNKNOW(3, "未知"),
    ;

    private Integer code;
    private String message;

}
