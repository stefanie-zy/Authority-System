package com.stefanie.userserver.common.enums.other;

import io.swagger.annotations.ApiModel;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-24 22:28
 * @description 业务操作类型枚举
 */
@ApiModel(value = "枚举-业务操作类型")
public enum BusinessTypeEnum {

    /**
     * 其他
     */
    OTHER,
    /**
     * 新增
     */
    INSERT,
    /**
     * 修改
     */
    UPDATE,
    /**
     * 删除
     */
    DELETE,
    /**
     * 授权
     */
    GRANT,
    /**
     * 导出
     */
    EXPORT,
    /**
     * 导入
     */
    IMPORT,
    /**
     * 强退
     */
    FORCE,
    /**
     * 清空数据
     */
    CLEAN,
    /**
     * 修改状态
     */
    UPDATE_STATUS,
    /**
     * 修改密码
     */
    UPDATE_PASSWORD;

}
