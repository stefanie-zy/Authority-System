package com.stefanie.userserver.common.enums.other;

import io.swagger.annotations.ApiModel;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-25 21:45
 * @description 请求方式枚举
 */
@ApiModel(value = "枚举-HTTP请求类型")
public enum HttpMethodEnum {

    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;
}
