package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-20 20:38
 * @description
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-删除标记")
public enum OperationEnum implements CommonEnum {

    SUCCESS(1, "成功"),
    ERROR(2, "失败"),
    ;

    private Integer code;
    private String message;
}
