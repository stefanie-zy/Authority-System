package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 17:33
 * @description
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-状态")
public enum StatusEnum implements CommonEnum {

    USABLE(1, "正常/可用"),
    BLOCK_UP(2, "停用"),
    ;

    private Integer code;
    private String message;
}
