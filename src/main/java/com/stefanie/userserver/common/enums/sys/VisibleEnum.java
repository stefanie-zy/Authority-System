package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-29 21:32
 * @description
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-隐藏类型")
public enum VisibleEnum implements CommonEnum {

    TRUE(1, "显示"),
    FALSE(2, "隐藏"),
    ;

    private Integer code;
    private String message;
}
