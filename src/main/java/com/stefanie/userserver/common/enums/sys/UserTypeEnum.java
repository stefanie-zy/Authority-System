package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 19:50
 * @description 用户类型枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-用户类型")
public enum UserTypeEnum implements CommonEnum {

    SYS(1, "系统用户"),
    BUSINESS(2, "业务用户"),
    ;

    private Integer code;
    private String message;
}
