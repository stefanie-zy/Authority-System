package com.stefanie.userserver.common.enums.sys;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-09 16:22
 * @description 用户状态枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-用户状态")
public enum UserStatusEnum implements CommonEnum {

    USABLE(1, "正常/可用"),
    LOCK(2, "锁定"),
    WRITE_OFF(3, "注销"),
    ;

    private Integer code;
    private String message;
}
