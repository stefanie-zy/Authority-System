package com.stefanie.userserver.common.enums.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

import java.util.*;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-24 15:44
 * @description 枚举基础接口
 */
@ApiModel(value = "枚举-公共接口")
public interface CommonEnum {

    String getMessage();

    Integer getCode();

    @ApiOperation(value = "根据枚举、枚举状态码获取枚举")
    static <E extends CommonEnum> E getEnumByCode(Class<E> enumClass, Integer code) {
        E e = null;
        if (enumClass != null && code != null && enumClass.isEnum()) {
            E[] enums = enumClass.getEnumConstants();
            for (E ec : enums) {
                if (ec.getCode().equals(code)) {
                    e = ec;
                    break;
                }
            }
        }
        return e;
    }

    @ApiOperation(value = "据枚举、枚举状态码获取枚举值")
    static <E extends CommonEnum> String getMessageByCode(Class<E> enumClass, Integer code) {
        String message = null;
        if (enumClass != null && code != null && enumClass.isEnum()) {
            E[] enums = enumClass.getEnumConstants();
            for (E ec : enums) {
                if (ec.getCode().equals(code)) {
                    message = ec.getMessage();
                    break;
                }
            }
        }
        return message;
    }

    @ApiOperation(value = "获取枚举键值对")
    static <E extends CommonEnum> List<Map<Integer, String>> getEnumList(Class<E> enumClass) {
        List<Map<Integer, String>> enumList = new ArrayList<>();
        if (enumClass != null && enumClass.isEnum()) {
            E[] enums = enumClass.getEnumConstants();
            for (E ec : enums) {
                Map<Integer, String> map = new HashMap<>();
                map.put(ec.getCode(), ec.getMessage());
                enumList.add(map);
            }
        }
        return enumList;
    }

    @ApiOperation(value = "获取枚举状态码列表")
    static <E extends CommonEnum> List<Integer> getCodeList(Class<E> enumClass) {
        List<Integer> codeList = new ArrayList<>();
        if (enumClass != null && enumClass.isEnum()) {
            E[] enums = enumClass.getEnumConstants();
            for (E ec : enums) {
                codeList.add(ec.getCode());
            }
        }
        return codeList;
    }

    @ApiOperation(value = "判断枚举是否包含值")
    static <E extends CommonEnum> Boolean check(Class<E> enumClass, Integer code) {
        List<Integer> codeList = getCodeList(enumClass);
        return codeList.contains(code);
    }

}
