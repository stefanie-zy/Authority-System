package com.stefanie.userserver.common.enums.error;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-05 22:29
 * @description
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-服务调用异常")
public enum ServerCallErrorEnum implements CommonEnum {

    S_REDIS_SELECT_ERROR(4000, "Redis异常：Redis查询异常"),
    ;

    private Integer code;
    private String message;
}
