package com.stefanie.userserver.common.enums.other;

import io.swagger.annotations.ApiModel;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-11 15:02
 * @description 接口限流类型没
 */

@ApiModel(value = "枚举-接口限流类型")
public enum LimitEnum {

    /**
     * ip限流
     */
    IP,
    /**
     * 默认策略限流
     */
    DEFAULT;

}
