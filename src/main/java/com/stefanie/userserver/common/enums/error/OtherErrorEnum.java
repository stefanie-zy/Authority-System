package com.stefanie.userserver.common.enums.error;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 9:00
 * @description:
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-其他异常")
public enum OtherErrorEnum implements CommonEnum {

    B_DICT_TYPE_NAME_EXIT(10001, "字典异常：字典类型名称已存在"),
    B_DICT_TYPE_EXIT(10002, "字典异常：字典类型已存在"),
    B_DICT_TYPE_NULL(10003, "字典异常：字典类型不存在"),
    B_DICT_TYPE_HAS_DATA(10004, "字典异常：字典类型下存在字典数据"),
    B_DICT_EXIT(10005, "字典异常：字典已存在"),
    B_DICT_NULL(10006, "字典异常：字典不存在"),

    S_DICT_TYPE_CREATE_ERROR(11001, "字典异常：字典类型添加异常"),
    S_DICT_TYPE_UPDATE_ERROR(11002, "字典异常：字典类型修改异常"),
    S_DICT_TYPE_DELETE_ERROR(11003, "字典异常：字典类型删除异常"),
    S_DICT_CREATE_ERROR(11004, "字典异常：字典添加异常"),
    S_DICT_UPDATE_ERROR(11005, "字典异常：字典修改异常"),
    S_DICT_DELETE_ERROR(11006, "字典异常：字典删除异常"),
    S_DICT_TYPE_ERROR(11007, "字典异常：字典类型值错误"),
    S_DICT_STATUS_ERROR(11008, "字典异常：字典状态值错误"),
    ;

    private Integer code;
    private String message;
}
