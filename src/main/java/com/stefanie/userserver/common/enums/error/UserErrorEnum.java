package com.stefanie.userserver.common.enums.error;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 16:22
 * @description 用户异常枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-用户异常")
public enum UserErrorEnum implements CommonEnum {

    P_USER_USER_NAME_NULL(2000, "用户异常：用户名称不能为空"),
    P_USER_LOGIN_NAME_NULL(2001, "用户异常：用户登录名不能为空"),
    P_USER_TYPE_NULL(2002, "用户异常：用户类型不能为空"),
    P_USER_TYPE_VALUE_ERROR(2003, "用户异常：用户类型值不符合基本类型值"),
    P_USER_SEX_VALUE_ERROR(2004, "用户异常：用户类性别不符合基本类型值"),
    P_USER_PARAM_NULL(2005, "用户异常：参数为空"),
    P_USER_USER_NAME_BE_CHINESERROR(2010, "用户异常：用户名不支持除中文外其他格式"),
    P_USER_LOGIN_NAME_CAN_NOT_BE_CHINESERROR(2011, "用户异常：用户登录名不支持中文格式"),
    P_USER_PHONE_NUMBER_FORMAT_ERROR(2012, "用户异常：手机号格式错误"),
    P_USER_ID_CARD_FORMAT_ERROR(2013, "用户异常：身份证格式错误"),
    P_USER_EMAIL_FORMAT_ERROR(2014, "用户异常：邮箱格式错误"),
    P_USER_PASSWORD_MATCH(2015, "用户异常：新旧密码一样"),
    P_USER_CONFIRM_PASSWORD_ERROR(2016, "用户异常：新密码两次输入不一致"),
    P_USER_OLD_PASSWORD_ERROR_ENUM(2017, "用户异常：旧密码错误"),
    P_USER_PASSWORD_ERROR(2018, "用户异常：密码错误"),
    P_USER_EXCEL_NULL(2019,"用户异常：导入用户信息为空"),

    B_USER_LOGIN_NAME_EXIT(2100, "用户异常：登录名已注册"),
    B_USER_PHONE_NUMBER_EXIT(2101, "用户异常：手机号已注册"),
    B_USER_ID_CARD_EXIT(2102, "用户异常：身份证已注册"),
    B_USER_NOT_EXIT(2103, "用户异常：用户不存在"),
    B_USER_LOCK(2104, "用户异常：用户被锁定"),
    B_USER_WRITE_OFF(2105, "用户异常：用户已注销"),
    B_USER_DELETE(2106, "用户异常：用户已删除"),
    B_USER_SECURITY_NULL(2107, "用户异常：安全框架查询登录用户异常"),
    B_USER_NO_PERMISSION_OPERATE(2108, "用户异常：无操作权限"),

    S_USER_CREATE_ERROR(2200, "用户异常，用户添加异常"),
    S_USER_UPDATE_ERROR(2201, "用户异常：用户修改异常"),
    S_USER_DELETE_ERROR(2202, "用户异常：用户删除异常"),
    S_USER_UPDATE_STATUS_ERROR(2203, "用户异常：用户修改状态异常"),
    S_USER_UPDATE_PASSWORD_ERROR(2204, "用户异常：用户修改密码异常"),
    S_USER_RESET_PASSWORD_ERROR(2205, "用户异常：用户重置密码异常"),
    S_USER_LOGIN_ERROR(2206, "用户异常：用户登录系统异常"),

    ;

    private Integer code;
    private String message;
}
