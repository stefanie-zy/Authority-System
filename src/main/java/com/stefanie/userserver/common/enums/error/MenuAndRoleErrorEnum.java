package com.stefanie.userserver.common.enums.error;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 16:58
 * @description
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-菜单异常")
public enum MenuAndRoleErrorEnum implements CommonEnum {

    P_MENU_PARENT_ID_ERROR(5000, "菜单异常：父级菜单ID不能为负"),
    P_MENU_PARAM_NULL(5001, "菜单异常：菜单参数为空"),
    P_ROLE_PARAM_NULL(5002, "角色异常：角色参数为空"),

    B_MENU_NAME_EXIT(5100, "菜单异常：菜单名称已存在"),
    B_ROLE_NAME_EXIT(5101, "角色异常：角色名称已存在"),
    B_ROLE_KEY_EXIT(5102, "角色异常：角色权限字符串已存在"),
    B_PARENT_MENU_NOT_EXIT(5103, "菜单异常：父级菜单不存在"),
    B_MENU_NOT_EXIT(5104, "菜单异常：菜单不存在"),
    B_MENU_PARENT_ID_ERROR(5105, "菜单异常：父级节点ID不能等于当前菜单ID"),
    B_USER_AND_ROLE_NOT_NULL(5106, "角色异常：角色下存在绑定用户"),
    B_ROLE_NULL(5107, "角色异常：角色不存在"),

    S_MENU_CREATE_ERROR(5200, "菜单异常，菜单添加异常"),
    S_MENU_UPDATE_ERROR(5201, "菜单异常：菜单修改异常"),
    S_MENU_DELETE_ERROR(5202, "菜单异常：菜单删除异常"),
    S_ROLE_CREATE_ERROR(5203, "角色异常，角色添加异常"),
    S_ROLE_UPDATE_ERROR(5204, "角色异常：角色修改异常"),
    S_ROLE_DELETE_ERROR(5205, "角色异常：角色删除异常"),
    S_MENU_UPDATE_STATUS_ERROR(5206, "菜单异常：修改菜单状态异常"),
    S_MENU_UPDATE_VISIBLE_ERROR(5207, "菜单异常：修改菜单隐藏异常"),
    ;


    private Integer code;
    private String message;
}
