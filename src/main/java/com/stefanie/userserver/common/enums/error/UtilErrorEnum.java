package com.stefanie.userserver.common.enums.error;

import com.stefanie.userserver.common.enums.core.CommonEnum;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 18:31
 * @description 工具类错误枚举
 */
@AllArgsConstructor
@Getter
@ApiModel(value = "枚举-工具类异常")
public enum UtilErrorEnum implements CommonEnum {

    S_UTIL_GREP_ERROR(1000, "工具类异常：正则表达式判断异常"),
    S_UTIL_REDIS_ERROR(1001, "工具类异常：Redis操作异常"),
    S_UTIL_UUID_ERROR(1002, "工具类异常：UUID操作异常"),
    S_UTIL_ENCRYPTION_ERROR(1003, "工具类异常：加密异常"),
    S_UTIL_DECODE_ERROR(1004, "工具类异常：解密异常"),
    S_UTIL_FILE_MD5_ERROR(1005, "文件工具类异常：MD5计算失败"),
    S_UTIL_FILE_UPLOAD_ERROR(1006, "文件工具类异常：GO-FASTDFS上传文件失败"),
    S_UTIL_FILE_MD5_NULL(1006, "文件工具类异常：MD5为空"),
    S_UTIL_FILE_PATH_NULL(1007, "文件工具类异常：PATH为空"),
    ;

    private Integer code;
    private String message;
}
