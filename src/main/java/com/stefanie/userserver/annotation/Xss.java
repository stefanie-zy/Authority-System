package com.stefanie.userserver.annotation;

import com.stefanie.userserver.annotation.impl.XssValidator;
import com.stefanie.userserver.constant.bus.RequestErrorPromptConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-18 16:15
 * @description 防止xss注入注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Constraint(validatedBy = {XssValidator.class})
@ApiModel(value = "注解-防XSS注入")
public @interface Xss {

    @ApiModelProperty(value = "提示语")
    String message() default RequestErrorPromptConstant.XSS_MESSAGE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
