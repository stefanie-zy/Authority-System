package com.stefanie.userserver.annotation;

import com.stefanie.userserver.common.enums.other.LimitEnum;
import com.stefanie.userserver.constant.bus.RedisKeyConstant;
import com.stefanie.userserver.constant.bus.RequestErrorPromptConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.lang.annotation.*;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-10 15:05
 * @description 接口限流注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ApiModel(value = "注解-接口限流")
public @interface Limit {

    @ApiModelProperty(value = "限流key")
    String key() default RedisKeyConstant.LIMIT_KEY;

    @ApiModelProperty(value = "限流时间", notes = "单位：秒")
    int time() default 60;

    @ApiModelProperty(value = "限流次数")
    int count() default 100;

    @ApiModelProperty(value = "提示语")
    String message() default RequestErrorPromptConstant.LIMIT_ERROR_MESSAGE;

    @ApiModelProperty(value = "限流类型")
    LimitEnum limitEnum() default LimitEnum.DEFAULT;

}
