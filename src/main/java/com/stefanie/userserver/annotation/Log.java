package com.stefanie.userserver.annotation;

import com.stefanie.userserver.common.enums.other.BusinessTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.lang.annotation.*;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-24 22:26
 * @description 日志注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ApiModel(value = "注解-日志")
public @interface Log {

    @ApiModelProperty(value = "模块名称")
    String title() default "";

    @ApiModelProperty(value = "业务操作类型")
    BusinessTypeEnum businessTypeEnum() default BusinessTypeEnum.OTHER;

    @ApiModelProperty(value = "是否保存请求参数")
    boolean isSaveRequestData() default true;

    @ApiModelProperty(value = "是否保存响应参数")
    boolean isSaveResponseData() default true;

}
