package com.stefanie.userserver.annotation;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-03 15:45
 * @description Excel导入工具类
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@ApiModel(value = "注解-Excel导入属性")
public @interface ExcelImport {

    @ApiModelProperty(value = "字段名称")
    String value();

    @ApiModelProperty(value = "导出映射，格式如：0-未知;1-男;2-女")
    String kv() default "";

    @ApiModelProperty(value = " 是否为必填字段（默认为非必填）")
    boolean required() default false;

    @ApiModelProperty(value = "最大长度（默认255）")
    int maxLength() default 255;

    @ApiModelProperty(value = "导入唯一性验证（多个字段则取联合验证）")
    boolean unique() default false;
}
