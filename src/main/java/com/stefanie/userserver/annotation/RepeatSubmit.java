package com.stefanie.userserver.annotation;

import com.stefanie.userserver.constant.bus.RequestErrorPromptConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Value;

import java.lang.annotation.*;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-12 17:53
 * @description 防重复操作注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ApiModel(value = "注解-防重复操作")
public @interface RepeatSubmit {

    @ApiModelProperty(value = "时间",notes = "单位：秒")
    int time() default 2;

    @ApiModelProperty(value = "提示语")
    String message() default RequestErrorPromptConstant.REPEAT_SUBMIT_MESSAGE;
}
