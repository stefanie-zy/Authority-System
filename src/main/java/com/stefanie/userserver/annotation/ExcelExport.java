package com.stefanie.userserver.annotation;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-03 15:38
 * @description Excel导出工具类
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@ApiModel(value = "注解-Excel导出属性")
public @interface ExcelExport {

    @ApiModelProperty(value = "字段名称")
    String value();

    @ApiModelProperty(value = "导出排序先后: 数字越小越靠前（默认按Java类字段顺序导出）")
    int sort() default 0;

    @ApiModelProperty(value = "导出映射，格式如：0-未知;1-男;2-女")
    String kv() default "";

    @ApiModelProperty(value = "导出模板示例值（有值的话，直接取该值，不做映射）")
    String example() default "";

}
