package com.stefanie.userserver.controller.core;

import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.utils.security.SecurityUtils;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-24 21:22
 * @description 通用数据处理层
 */
@Slf4j
@RestController
public class BaseController {

    @ApiOperation(value = "获取当前用户名称")
    public String getCurrUserName() {
        return getLoginUser().getUsername();
    }

    @ApiOperation(value = "获取用户缓存信息")
    public LoginUserBo getLoginUser() {
        return SecurityUtils.getLoginUser();
    }

    @ApiOperation(value = "获取用户Id")
    public Long getUserId() {
        return getLoginUser().getUserPo().getId();
    }
}
