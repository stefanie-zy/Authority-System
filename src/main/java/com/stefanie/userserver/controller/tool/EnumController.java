package com.stefanie.userserver.controller.tool;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.common.enums.core.CommonEnum;
import com.stefanie.userserver.common.enums.error.*;
import com.stefanie.userserver.common.enums.sys.*;
import com.stefanie.userserver.handle.Stefanie;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author: stefanie
 * @createTime: 2023/07/05 10:46
 * @description:
 */
@Api(tags = "枚举接口")
@ApiSort(1)
@RequestMapping(value = "/api-enum")
@RequiredArgsConstructor
@RestController
@Slf4j
public class EnumController {

    // 异常枚举

    @ApiOperation(value = "异常-系统异常枚举值")
    @GetMapping(value = "/sysError")
    public Stefanie<List<Map<Integer, String>>> listSysError() {
        return Stefanie.success(CommonEnum.getEnumList(ErrorEnum.class));
    }

    @ApiOperation(value = "异常-用户异常枚举值")
    @GetMapping(value = "/userError")
    public Stefanie<List<Map<Integer, String>>> listUserError() {
        return Stefanie.success(CommonEnum.getEnumList(UserErrorEnum.class));
    }

    @ApiOperation(value = "异常-SQL异常枚举值")
    @GetMapping(value = "/sqlError")
    public Stefanie<List<Map<Integer, String>>> listSqlError() {
        return Stefanie.success(CommonEnum.getEnumList(SqlErrorEnum.class));
    }

    @ApiOperation(value = "异常-工具类异常枚举值")
    @GetMapping(value = "/utilError")
    public Stefanie<List<Map<Integer, String>>> listUtilError() {
        return Stefanie.success(CommonEnum.getEnumList(UtilErrorEnum.class));
    }

    @ApiOperation(value = "异常-服务调用异常枚举值")
    @GetMapping(value = "/serverError")
    public Stefanie<List<Map<Integer, String>>> listServerError() {
        return Stefanie.success(CommonEnum.getEnumList(ServerCallErrorEnum.class));
    }

    @ApiOperation(value = "异常-角色菜单异常枚举值")
    @GetMapping(value = "/roleAndMenuError")
    public Stefanie<List<Map<Integer, String>>> listRoleAndMenuError() {
        return Stefanie.success(CommonEnum.getEnumList(MenuAndRoleErrorEnum.class));
    }

    // 系统枚举

    @ApiOperation(value = "系统-删除标记枚举值")
    @GetMapping(value = "/delFlag")
    public Stefanie<List<Map<Integer, String>>> listDelFlag() {
        return Stefanie.success(CommonEnum.getEnumList(DelFlagEnum.class));
    }

    @ApiOperation(value = "系统-登录状态枚举值")
    @GetMapping(value = "/loginStatus")
    public Stefanie<List<Map<Integer, String>>> listLoginStatus() {
        return Stefanie.success(CommonEnum.getEnumList(LoginStatusEnum.class));
    }

    @ApiOperation(value = "系统-菜单状态枚举值")
    @GetMapping(value = "/menuStatus")
    public Stefanie<List<Map<Integer, String>>> listMenuStatus() {
        return Stefanie.success(CommonEnum.getEnumList(StatusEnum.class));
    }

    @ApiOperation(value = "系统-操作状态枚举值")
    @GetMapping(value = "/operation")
    public Stefanie<List<Map<Integer, String>>> listOperation() {
        return Stefanie.success(CommonEnum.getEnumList(OperationEnum.class));
    }

    @ApiOperation(value = "系统-用户性别枚举值")
    @GetMapping(value = "/userSex")
    public Stefanie<List<Map<Integer, String>>> listUserSex() {
        return Stefanie.success(CommonEnum.getEnumList(UserSexEnum.class));
    }

    @ApiOperation(value = "系统-用户状态枚举值")
    @GetMapping(value = "/userStatus")
    public Stefanie<List<Map<Integer, String>>> listUserStatus() {
        return Stefanie.success(CommonEnum.getEnumList(UserStatusEnum.class));
    }

    @ApiOperation(value = "系统-用户类型枚举值")
    @GetMapping(value = "/userType")
    public Stefanie<List<Map<Integer, String>>> listUserType() {
        return Stefanie.success(CommonEnum.getEnumList(UserTypeEnum.class));
    }

    @ApiOperation(value = "系统-隐藏/显示枚举值")
    @GetMapping(value = "/visible")
    public Stefanie<List<Map<Integer, String>>> listVisible() {
        return Stefanie.success(CommonEnum.getEnumList(VisibleEnum.class));
    }
}
