package com.stefanie.userserver.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.domain.pojo.dto.sys.LogPageDto;
import com.stefanie.userserver.domain.pojo.vo.sys.LogVo;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.LogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-20 19:34
 * @description
 */
@Api(tags = "系统日志接口")
@ApiSort(3)
@RequestMapping(value = "/api-log")
@RequiredArgsConstructor
@RestController
@Slf4j
public class LogController {

    private final LogService logService;

    @ApiOperation(value = "分页查询系统日志", notes = "默认查询10条数据，不足pageSize<10，按照10条查询，>10按实际数值查询")
    @PostMapping(value = "/page")
    public Stefanie<IPage<LogVo>> page(@RequestBody @Validated LogPageDto logPageDto) {
        return Stefanie.success(logService.page(logPageDto));
    }

    @ApiOperation(value = "清空所有日志", notes = "物理删除所有数据")
    @DeleteMapping(value = "/all")
    public Stefanie<?> clear() {
        logService.clear();
        return Stefanie.success();
    }
}
