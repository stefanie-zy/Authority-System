package com.stefanie.userserver.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.annotation.Limit;
import com.stefanie.userserver.annotation.Log;
import com.stefanie.userserver.annotation.RepeatSubmit;
import com.stefanie.userserver.common.enums.error.UserErrorEnum;
import com.stefanie.userserver.common.enums.other.BusinessTypeEnum;
import com.stefanie.userserver.common.enums.other.LimitEnum;
import com.stefanie.userserver.common.excel.UserExportExcel;
import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.bus.ExcelNameConstant;
import com.stefanie.userserver.constant.bus.RequestSuccessPromptConstant;
import com.stefanie.userserver.constant.bus.UserConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.controller.core.BaseController;
import com.stefanie.userserver.domain.pojo.dto.sys.UserCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserPageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserUpdatePasswordDto;
import com.stefanie.userserver.domain.pojo.vo.sys.ExcelResultVo;
import com.stefanie.userserver.domain.pojo.vo.sys.UserVo;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.UserExcelService;
import com.stefanie.userserver.service.sys.UserService;
import com.stefanie.userserver.utils.excel.ExcelUtils;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:51
 * @description
 */
@Api(tags = "系统用户接口")
@ApiSort(1)
@RequestMapping(value = "/api-user")
@RequiredArgsConstructor
@RestController
@Slf4j
public class UserController extends BaseController {

    private final UserService userService;
    private final UserExcelService userExcelService;
    private final StefanieConfig stefanieConfig;

    @Limit(key = "create_user:", count = 5, limitEnum = LimitEnum.IP, message = "新增系统用户过于频繁，请稍后重试！")
    @RepeatSubmit(time = 60)
    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.INSERT)
    @ApiOperation(value = "新增系统用户")
    @PostMapping(value = "")
    public Stefanie<?> create(@RequestBody @Validated UserCreateDto userCreateDto) {
        userCreateDto.setCreateName(getCurrUserName());
        UserException.check(userService.create(userCreateDto), UserErrorEnum.S_USER_CREATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.UPDATE)
    @ApiOperation(value = "修改系统用户")
    @PutMapping(value = "")
    public Stefanie<?> update(@RequestBody @Validated UserUpdateDto userUpdateDto) {
        // 用户是否允许修改校验
        UserException.check(UserConstant.ALLOW.equals(userService.userAllowedCheck(userUpdateDto.getId())), UserErrorEnum.B_USER_NO_PERMISSION_OPERATE);
        UserException.check(userService.update(userUpdateDto), UserErrorEnum.S_USER_UPDATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.UPDATE_PASSWORD)
    @ApiOperation(value = "修改密码", notes = "用户密码需要进行传输加密")
    @PutMapping(value = "/password")
    public Stefanie<String> updatePassword(@RequestBody @Validated UserUpdatePasswordDto userUpdatePasswordDto) {
        // 是否为本人操作校验
        UserException.check(getLoginUser().getUserPo().getId().longValue() != userUpdatePasswordDto.getId().longValue(), UserErrorEnum.B_USER_NO_PERMISSION_OPERATE);
        UserException.check(userService.updatePassword(userUpdatePasswordDto), UserErrorEnum.S_USER_UPDATE_PASSWORD_ERROR);
        return Stefanie.success(RequestSuccessPromptConstant.UPDATE_PASSWORD);
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.UPDATE_PASSWORD)
    @ApiOperation(value = "重置密码")
    @PatchMapping(value = "/password/{id}")
    public Stefanie<String> resetPassword(@PathVariable Long id) {
        UserException.check(userService.resetPassword(id, getCurrUserName()), UserErrorEnum.S_USER_UPDATE_PASSWORD_ERROR);
        return Stefanie.success(RequestSuccessPromptConstant.UPDATE_PASSWORD + stefanieConfig.getPassword());
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.DELETE)
    @ApiOperation(value = "删除系统用户", notes = "删除所有的关联关系")
    @DeleteMapping(value = "/{id}")
    public Stefanie<?> delete(@PathVariable Long id) {
        // 用户是否允许删除校验
        UserException.check(UserConstant.ALLOW.equals(userService.userAllowedCheck(id)), UserErrorEnum.B_USER_NO_PERMISSION_OPERATE);
        UserException.check(userService.delete(id, getCurrUserName()), UserErrorEnum.S_USER_DELETE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.DELETE)
    @ApiOperation(value = "批量删除系统用户", notes = "删除所有的关联关系")
    @DeleteMapping(value = "/batch/{ids}")
    public Stefanie<String> deleteByIds(@PathVariable Long[] ids) {
        UserException.check(ids.length == 0, UserErrorEnum.P_USER_PARAM_NULL);
        return Stefanie.success(userService.deltetByIds(ids, getCurrUserName()));
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.UPDATE_STATUS)
    @ApiOperation(value = "修改用户状态")
    @PatchMapping(value = "/status/{id}/{status}")
    public Stefanie<?> updateStatus(@PathVariable Long id, @PathVariable Integer status) {
        // 用户是否允许修改校验
        UserException.check(UserConstant.ALLOW.equals(userService.userAllowedCheck(id)), UserErrorEnum.B_USER_NO_PERMISSION_OPERATE);
        // 用户状态是否可以变更
        userService.userStatusCheck(id, status);
        UserException.check(userService.updateStatus(id, status, getCurrUserName()), UserErrorEnum.S_USER_UPDATE_STATUS_ERROR);
        return Stefanie.success();
    }

    @ApiOperation(value = "分页查询系统用户", notes = "默认查询10条数据，不足pageSize<10，按照10条查询，>10按实际数值查询")
    @PostMapping(value = "/page")
    public Stefanie<IPage<UserVo>> page(@RequestBody @Validated UserPageDto userPageDto) {
        return Stefanie.success(userService.page(userPageDto));
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.INSERT)
    @ApiOperation(value = "根据用户ID查询用户")
    @GetMapping(value = "/{id}")
    public Stefanie<UserVo> selectById(@PathVariable Long id) {
        UserVo userVo = userService.selectById(id);
        UserException.check(StringUtil.isNull(userVo), UserErrorEnum.B_USER_NOT_EXIT);
        return Stefanie.success(userVo);
    }

    @Log(title = "用户管理", businessTypeEnum = BusinessTypeEnum.EXPORT)
    @ApiOperation(value = "导出系统用户", produces = "application/octet-stream")
    @GetMapping(value = "/export")
    public void export(HttpServletResponse response) {
        List<UserExportExcel> userExcelList = userService.exportUserExcelList();
        ExcelUtils.export(response, ExcelNameConstant.SYS_USER, userExcelList, UserExportExcel.class);
    }

    @ApiOperation(value = "导出系统用户模板", produces = "application/octet-stream")
    @GetMapping(value = "/export/demo")
    public void exportDemo(HttpServletResponse response) {
        ExcelUtils.exportTemplate(response, ExcelNameConstant.SYS_USER_DEMO, UserExportExcel.class, true);
    }

    @ApiOperation(value = "导入用户")
    @PostMapping(value = "/import/user")
    public Stefanie<ExcelResultVo> importUser(@ApiParam(value = "文件", required = true) @RequestPart MultipartFile file) throws ExecutionException, InterruptedException {
        return Stefanie.success(userExcelService.importUser(file, getCurrUserName()));
    }

    @ApiOperation(value = "登录名唯一校验", notes = "新增：id=0L；修改：id=被修改用户ID")
    @GetMapping(value = "/check/loginName/{loginName}/{id}")
    public Stefanie<?> loginNameCheck(@PathVariable String loginName, @PathVariable Long id) {
        UserException.check(CommonConstant.UNIQUE.equals(userService.userColumnCheck(ColumnConstant.LONIN_NAME, loginName, id)), UserErrorEnum.B_USER_LOGIN_NAME_EXIT);
        return Stefanie.success();
    }

    @ApiOperation(value = "手机号唯一校验", notes = "新增：id=0L；修改：id=被修改用户ID")
    @GetMapping(value = "/check/phoneNumber/{phoneNumber}/{id}")
    public Stefanie<?> phoneNumberCheck(@PathVariable String phoneNumber, @PathVariable Long id) {
        UserException.check(CommonConstant.UNIQUE.equals(userService.userColumnCheck(ColumnConstant.PHONE_NUMBER, phoneNumber, id)), UserErrorEnum.B_USER_PHONE_NUMBER_EXIT);
        return Stefanie.success();
    }

    @ApiOperation(value = "身份证唯一校验", notes = "新增：id=0L；修改：id=被修改用户ID")
    @GetMapping(value = "/check/idCard/{idCard}/{id}")
    public Stefanie<?> idCardCheck(@PathVariable String idCard, @PathVariable Long id) {
        UserException.check(CommonConstant.UNIQUE.equals(userService.userColumnCheck(ColumnConstant.ID_CARD, idCard, id)), UserErrorEnum.B_USER_ID_CARD_EXIT);
        return Stefanie.success();
    }

    @ApiOperation(value = "旧密码校验", notes = "用户密码需要进行传输加密")
    @GetMapping(value = "/check/password/{id}/{oldPassword}")
    public Stefanie<?> oldPasswordCheck(@PathVariable Long id, @PathVariable String oldPassword) {
        userService.oldPasswordCheck(id, oldPassword);
        return Stefanie.success();
    }

}
