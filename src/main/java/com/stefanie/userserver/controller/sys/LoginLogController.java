package com.stefanie.userserver.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.domain.pojo.dto.sys.LoginLogPageDto;
import com.stefanie.userserver.domain.pojo.vo.sys.LoginLogVo;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.LoginLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 15:15
 * @description
 */
@Api(tags = "登录日志接口")
@ApiSort(4)
@RequestMapping(value = "/api-login-log")
@RequiredArgsConstructor
@RestController
@Slf4j
public class LoginLogController {

    private final LoginLogService loginLogService;

    @ApiOperation(value = "分页查询登录日志", notes = "默认查询10条数据，不足pageSize<10，按照10条查询，>10按实际数值查询")
    @PostMapping(value = "/page")
    public IPage<LoginLogVo> page(@RequestBody @Validated LoginLogPageDto loginLogPageDto) {
        return loginLogService.page(loginLogPageDto);
    }

    @ApiOperation(value = "清空所有日志", notes = "物理删除所有数据")
    @DeleteMapping(value = "/all")
    public Stefanie<?> clear() {
        loginLogService.clear();
        return Stefanie.success();
    }
}
