package com.stefanie.userserver.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.annotation.Log;
import com.stefanie.userserver.common.enums.error.OtherErrorEnum;
import com.stefanie.userserver.common.enums.other.BusinessTypeEnum;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.controller.core.BaseController;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataPageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataUpdateDto;
import com.stefanie.userserver.domain.pojo.vo.sys.DictDataVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.DictDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-05 22:13
 * @description
 */
@Api(tags = "字典控制层")
@ApiSort(8)
@RequestMapping(value = "/api-dict-data")
@RequiredArgsConstructor
@RestController
@Slf4j
public class DictDataController extends BaseController {

    private final DictDataService dictDataService;

    @Log(title = "字典管理", businessTypeEnum = BusinessTypeEnum.INSERT)
    @ApiOperation(value = "新增字典")
    @PostMapping(value = "")
    public Stefanie<?> create(@RequestBody @Validated DictDataCreateDto dictDataCreateDto) {
        dictDataCreateDto.setCreateName(getCurrUserName());
        SysException.check(dictDataService.create(dictDataCreateDto), OtherErrorEnum.S_DICT_CREATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "字典管理", businessTypeEnum = BusinessTypeEnum.UPDATE)
    @ApiOperation(value = "修改字典")
    @PutMapping(value = "")
    public Stefanie<?> update(@RequestBody @Validated DictDataUpdateDto dictDataUpdateDto) {
        dictDataUpdateDto.setUpdateName(getCurrUserName());
        SysException.check(dictDataService.update(dictDataUpdateDto), OtherErrorEnum.S_DICT_UPDATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "字典管理", businessTypeEnum = BusinessTypeEnum.DELETE)
    @ApiOperation(value = "删除字典")
    @DeleteMapping(value = "/{id}")
    public Stefanie<?> delete(@PathVariable Long id) {
        dictDataService.delete(id);
        return Stefanie.success();
    }

    @ApiOperation(value = "分页查询字典", notes = "默认查询10条数据，不足pageSize<10，按照10条查询，>10按实际数值查询")
    @PostMapping(value = "/page")
    public Stefanie<IPage<DictDataVo>> page(@RequestBody @Validated DictDataPageDto dictDataPageDto) {
        return Stefanie.success(dictDataService.page(dictDataPageDto));
    }

    @ApiOperation(value = "修改字典状态")
    @PatchMapping(value = "/status/{id}/{status}")
    public Stefanie<?> updateStatus(@PathVariable Long id, @PathVariable Integer status) {
        dictDataService.updateStatus(id, status, getCurrUserName());
        return Stefanie.success();
    }

    @ApiOperation(value = "字典唯一性校验")
    @GetMapping(value = "/check/{type}/{label}/{value}/{id}")
    public Stefanie<?> check(@PathVariable String type, @PathVariable String label, @PathVariable String value, @PathVariable Long id) {
        SysException.check(CommonConstant.UNIQUE.equals(dictDataService.check(type, label, value, id)), OtherErrorEnum.B_DICT_EXIT);
        return Stefanie.success();
    }

    @ApiOperation(value = "根据字典类型获取字典列表")
    @GetMapping(value = "/list/{type}")
    public Stefanie<List<DictDataVo>> listByType(@PathVariable String type) {
        return Stefanie.success(dictDataService.listByType(type));
    }
}
