package com.stefanie.userserver.controller.sys;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.annotation.Limit;
import com.stefanie.userserver.annotation.Log;
import com.stefanie.userserver.annotation.RepeatSubmit;
import com.stefanie.userserver.common.enums.error.MenuAndRoleErrorEnum;
import com.stefanie.userserver.common.enums.other.BusinessTypeEnum;
import com.stefanie.userserver.common.enums.other.LimitEnum;
import com.stefanie.userserver.controller.core.BaseController;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserAndRoleDto;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 16:57
 * @description
 */
@Api(tags = "系统角色接口")
@ApiSort(5)
@RequestMapping(value = "/api-role")
@RequiredArgsConstructor
@RestController
@Slf4j
public class RoleController extends BaseController {

    private final RoleService roleService;

    @Limit(key = "create_role:", count = 5, limitEnum = LimitEnum.IP, message = "新增角色过于频繁，请稍后重试！")
    @RepeatSubmit(time = 60)
    @Log(title = "角色管理", businessTypeEnum = BusinessTypeEnum.INSERT)
    @ApiOperation(value = "新增角色", notes = "添加角色基础信息")
    @PostMapping(value = "")
    public Stefanie<?> page(@RequestBody @Validated RoleCreateDto roleCreateDto) {
        roleCreateDto.setCreateName(getCurrUserName());
        UserException.check(roleService.create(roleCreateDto), MenuAndRoleErrorEnum.S_ROLE_CREATE_ERROR);
        return Stefanie.success();
    }

    @ApiOperation(value = "修改角色")
    @Log(title = "修改角色", businessTypeEnum = BusinessTypeEnum.UPDATE)
    @PutMapping(value = "")
    public Stefanie<?> update(@RequestBody @Validated RoleUpdateDto roleUpdateDto) {
        roleUpdateDto.setUpdateName(getCurrUserName());
        UserException.check(roleService.update(roleUpdateDto), MenuAndRoleErrorEnum.S_ROLE_UPDATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "删除角色", businessTypeEnum = BusinessTypeEnum.DELETE)
    @ApiOperation(value = "删除角色", notes = "删除所有的关联关系")
    @DeleteMapping(value = "/{id}")
    public Stefanie<?> delete(@PathVariable Long id) {
        roleService.delete(id);
        return Stefanie.success();
    }

    @ApiOperation(value = "用户-角色绑定", notes = "新增/修改公用；删除不可用")
    @PutMapping(value = "/build")
    public Stefanie<?> build(@RequestBody @Validated UserAndRoleDto userAndRoleDto) {
        roleService.build(userAndRoleDto);
        return Stefanie.success();
    }

    @ApiOperation(value = "用户-角色解绑")
    @PatchMapping(value = "/unbuild/{userId}")
    public Stefanie<?> unbuild(@PathVariable Long userId) {
        roleService.unbuild(userId);
        return Stefanie.success();
    }

}
