package com.stefanie.userserver.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.annotation.Log;
import com.stefanie.userserver.common.enums.error.OtherErrorEnum;
import com.stefanie.userserver.common.enums.other.BusinessTypeEnum;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.controller.core.BaseController;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypeCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypePageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypeUpdateDto;
import com.stefanie.userserver.domain.pojo.vo.sys.DictTypeVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.DictTypeServie;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: stefanie
 * @createTime: 2023/07/05 16:17
 * @description:
 */
@Api(tags = "字典类型控制层")
@ApiSort(7)
@RequestMapping(value = "/api-dict-type")
@RequiredArgsConstructor
@RestController
@Slf4j
public class DictTypeController extends BaseController {

    private final DictTypeServie dictTypeServie;

    @Log(title = "字典类型管理", businessTypeEnum = BusinessTypeEnum.INSERT)
    @ApiOperation(value = "新增字典类型")
    @PostMapping(value = "")
    public Stefanie<?> create(@RequestBody @Validated DictTypeCreateDto dictTypeCreateDto) {
        dictTypeCreateDto.setCreateName(getCurrUserName());
        SysException.check(dictTypeServie.create(dictTypeCreateDto), OtherErrorEnum.S_DICT_TYPE_CREATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "字典类型管理", businessTypeEnum = BusinessTypeEnum.UPDATE)
    @ApiOperation(value = "修改字典类型")
    @PutMapping(value = "")
    public Stefanie<?> update(@RequestBody @Validated DictTypeUpdateDto dictTypeUpdateDto) {
        dictTypeUpdateDto.setUpdateName(getCurrUserName());
        SysException.check(dictTypeServie.update(dictTypeUpdateDto), OtherErrorEnum.S_DICT_TYPE_UPDATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "字典类型管理", businessTypeEnum = BusinessTypeEnum.DELETE)
    @ApiOperation(value = "删除字典类型")
    @DeleteMapping(value = "/{id}")
    public Stefanie<?> delete(@PathVariable Long id) {
        dictTypeServie.delete(id);
        return Stefanie.success();
    }

    @ApiOperation(value = "查询字典列表")
    @GetMapping(value = "/list")
    public Stefanie<List<DictTypeVo>> list() {
        return Stefanie.success(dictTypeServie.voList());
    }

    @ApiOperation(value = "分页查询字典类型", notes = "默认查询10条数据，不足pageSize<10，按照10条查询，>10按实际数值查询")
    @PostMapping(value = "/page")
    public Stefanie<IPage<DictTypeVo>> page(@RequestBody @Validated DictTypePageDto dictTypePageDto) {
        return Stefanie.success(dictTypeServie.page(dictTypePageDto));
    }

    @ApiOperation(value = "修改字典类型状态", notes = "1-正常；2-停用")
    @PatchMapping(value = "/status/{id}/{status}")
    public Stefanie<?> updateStatus(@PathVariable Long id, @PathVariable Integer status) {
        dictTypeServie.updateStatus(id, status, getCurrUserName());
        return Stefanie.success();
    }

    @ApiOperation(value = "名称唯一校验", notes = "新增：id=0L；修改：id=被修改字典类型Id")
    @GetMapping(value = "/check/name/{name}/{id}")
    public Stefanie<?> nameCheck(@PathVariable String name, @PathVariable Long id) {
        SysException.check(CommonConstant.UNIQUE.equals(dictTypeServie.dictTypeColumnCheck(ColumnConstant.DICT_NAME, name, id)), OtherErrorEnum.B_DICT_TYPE_NAME_EXIT);
        return Stefanie.success();
    }

    @ApiOperation(value = "字典类型唯一校验", notes = "新增：id=0L；修改：id=被修改字典类型Id")
    @GetMapping(value = "/check/type/{type}/{id}")
    public Stefanie<?> typeCheck(@PathVariable String type, @PathVariable Long id) {
        SysException.check(CommonConstant.UNIQUE.equals(dictTypeServie.dictTypeColumnCheck(ColumnConstant.DICT_TYPE, type, id)), OtherErrorEnum.B_DICT_TYPE_EXIT);
        return Stefanie.success();
    }

}
