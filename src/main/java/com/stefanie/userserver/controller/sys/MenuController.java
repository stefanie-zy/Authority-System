package com.stefanie.userserver.controller.sys;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.annotation.Log;
import com.stefanie.userserver.annotation.RepeatSubmit;
import com.stefanie.userserver.common.enums.error.MenuAndRoleErrorEnum;
import com.stefanie.userserver.common.enums.other.BusinessTypeEnum;
import com.stefanie.userserver.controller.core.BaseController;
import com.stefanie.userserver.domain.pojo.dto.sys.MenuCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.MenuUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleAndMenuDto;
import com.stefanie.userserver.domain.pojo.vo.sys.MenuVo;
import com.stefanie.userserver.domain.pojo.vo.sys.TreeMenuVo;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 16:48
 * @description
 */
@Api(tags = "系统菜单接口")
@ApiSort(6)
@RequestMapping(value = "/api-menu")
@RequiredArgsConstructor
@RestController
@Slf4j
public class MenuController extends BaseController {

    private final MenuService menuService;

    @ApiOperation(value = "新增系统菜单")
    @RepeatSubmit(time = 60)
    @Log(title = "系统菜单", businessTypeEnum = BusinessTypeEnum.INSERT)
    @PostMapping(value = "")
    public Stefanie<?> create(@RequestBody @Validated MenuCreateDto menuCreateDto) {
        menuCreateDto.setCreateName(getCurrUserName());
        UserException.check(menuService.create(menuCreateDto), MenuAndRoleErrorEnum.S_MENU_CREATE_ERROR);
        return Stefanie.success();
    }

    @ApiOperation(value = "修改系统菜单")
    @Log(title = "系统菜单", businessTypeEnum = BusinessTypeEnum.UPDATE)
    @PutMapping(value = "")
    public Stefanie<?> update(@RequestBody @Validated MenuUpdateDto menuUpdateDto) {
        menuUpdateDto.setUpdateName(getCurrUserName());
        UserException.check(menuService.update(menuUpdateDto), MenuAndRoleErrorEnum.S_MENU_UPDATE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "菜单管理", businessTypeEnum = BusinessTypeEnum.UPDATE_STATUS)
    @ApiOperation(value = "修改菜单状态", notes = "菜单状态说明：可查看公共接口-枚举接口-菜单枚举-菜单状态枚举")
    @PatchMapping(value = "/status/{id}/{status}")
    public Stefanie<?> updateStatus(@PathVariable Long id, @PathVariable Integer status) {
        UserException.check(menuService.updateStatus(id, status, getCurrUserName()), MenuAndRoleErrorEnum.S_MENU_UPDATE_STATUS_ERROR);
        return Stefanie.success();
    }

    @Log(title = "菜单管理", businessTypeEnum = BusinessTypeEnum.UPDATE_STATUS)
    @ApiOperation(value = "修改菜单隐藏状态", notes = "菜单隐藏状态说明：可查看公共接口-枚举接口-菜单枚举-菜单隐藏状态枚举")
    @PatchMapping(value = "/visible/{id}/{visible}")
    public Stefanie<?> updateVisible(@PathVariable Long id, @PathVariable Integer visible) {
        UserException.check(menuService.updateVisible(id, visible, getCurrUserName()), MenuAndRoleErrorEnum.S_MENU_UPDATE_VISIBLE_ERROR);
        return Stefanie.success();
    }

    @Log(title = "菜单管理", businessTypeEnum = BusinessTypeEnum.DELETE)
    @ApiOperation(value = "删除菜单")
    @DeleteMapping(value = "/{id}")
    public Stefanie<?> delete(@PathVariable Long id) {
        menuService.delete(id);
        return Stefanie.success();
    }

    @Log(title = "菜单管理", businessTypeEnum = BusinessTypeEnum.DELETE)
    @ApiOperation(value = "批量删除菜单")
    @DeleteMapping(value = "/batch/{ids}")
    public Stefanie<?> deleteByIds(@PathVariable Long[] ids) {
        UserException.check(ids.length == 0, MenuAndRoleErrorEnum.P_MENU_PARAM_NULL);
        menuService.deleteByIds(ids);
        return Stefanie.success();
    }

    @ApiOperation(value = "查询树状菜单列表")
    @GetMapping(value = "/listTree")
    public Stefanie<List<TreeMenuVo>> listTree() {
        return Stefanie.success(menuService.listTree(getUserId()));
    }

    @ApiOperation(value = "查询菜单列表", notes = "非树状结构")
    @GetMapping(value = "/list")
    public Stefanie<List<MenuVo>> list() {
        return Stefanie.success(menuService.list(getUserId()));
    }

    @ApiOperation(value = "角色-菜单解绑")
    @PatchMapping(value = "/unbuild/{roleId}")
    public Stefanie<?> unbuild(@PathVariable Long roleId) {
        menuService.unbuild(roleId);
        return Stefanie.success();
    }

    @ApiOperation(value = "角色-菜单绑定", notes = "新增/修改公用；删除不可用")
    @PatchMapping(value = "/build")
    public Stefanie<?> build(@RequestBody @Validated RoleAndMenuDto roleAndMenuDto) {
        menuService.build(roleAndMenuDto);
        return Stefanie.success();
    }
}
