package com.stefanie.userserver.controller.sys;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.domain.pojo.dto.sys.LoginDto;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.manager.TokenManager;
import com.stefanie.userserver.service.sys.LoginService;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 16:30
 * @description
 */
@Api(tags = "登录接口")
@ApiSort(2)
@RequestMapping(value = "/api-login")
@RequiredArgsConstructor
@RestController
@Slf4j
public class LoginController {

    private final LoginService loginService;
    private final TokenManager tokenManager;

    @ApiOperation(value = "用户登录")
    @PostMapping(value = "/login")
    public Stefanie<String> login(@RequestBody @Validated LoginDto loginDto) {
        String token = loginService.login(loginDto);
        return Stefanie.success(token);
    }

    @ApiOperation(value = "用户退出登录")
    @GetMapping(value = "/logout")
    public Stefanie<?> logout(HttpServletRequest request) {
        LoginUserBo loginUserBo = tokenManager.getLoginUserBo(request);
        if (!StringUtil.isNull(loginUserBo)) {
            loginService.logout(loginUserBo);
        }
        return Stefanie.success();
    }
}
