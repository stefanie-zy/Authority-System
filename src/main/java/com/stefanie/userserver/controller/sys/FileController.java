package com.stefanie.userserver.controller.sys;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.stefanie.userserver.annotation.Log;
import com.stefanie.userserver.common.enums.other.BusinessTypeEnum;
import com.stefanie.userserver.controller.core.BaseController;
import com.stefanie.userserver.domain.pojo.dto.sys.FileCreateDto;
import com.stefanie.userserver.domain.pojo.vo.sys.FileVo;
import com.stefanie.userserver.handle.Stefanie;
import com.stefanie.userserver.service.sys.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author: stefanie
 * @createTime: 2023/07/13 14:24
 * @description:
 */
@Api(tags = "文件接口")
@ApiSort(9)
@RequestMapping(value = "/api-file")
@RequiredArgsConstructor
@RestController
@Slf4j
public class FileController extends BaseController {

    private final FileService fileService;

    @ApiOperation(value = "文件上传-不上传文件系统", notes = "上传至Go-Fastdfs服务器;\npath为空上传默认路径（路径无需拼接”/“）")
    @PostMapping(value = "/upload")
    public Stefanie<String> upload(@ApiParam(value = "文件", required = true) @RequestPart MultipartFile file, String path) {
        return Stefanie.success(fileService.upload(file, path));
    }

    @ApiOperation(value = "新增文件")
    @Log(title = "系统文件", businessTypeEnum = BusinessTypeEnum.INSERT)
    @PostMapping(value = "")
    public Stefanie<FileVo> create(@ApiParam(value = "文件", required = true) @RequestPart MultipartFile file, @RequestBody @Validated FileCreateDto fileCreateDto) {
        fileCreateDto.setCreateName(getCurrUserName());
        return Stefanie.success(fileService.create(fileCreateDto, file));
    }

    @ApiOperation(value = "删除文件")
    @DeleteMapping(value = "/{id}")
    public Stefanie<?> delete(@PathVariable Long id) {
        fileService.delete(id);
        return Stefanie.success();
    }

    @ApiOperation(value = "更新文件下载次数", notes = "每次点击下载的时候调用")
    @PatchMapping(value = "/download/{id}")
    public Stefanie<?> download(@PathVariable Long id) {
        fileService.download(id);
        return Stefanie.success();
    }
}
