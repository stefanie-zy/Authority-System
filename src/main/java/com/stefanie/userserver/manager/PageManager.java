package com.stefanie.userserver.manager;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-14 21:34
 * @description
 */
@Slf4j
@Component
@ApiModel(value = "Manager-Page")
public class PageManager<T> {

    @ApiOperation(value = "分页结果转换")
    public static <T> IPage<T> poToVo(IPage<T> po, IPage<T> vo, List<T> data) {
        vo.setRecords(data);
        vo.setTotal(po.getTotal());
        vo.setCurrent(po.getCurrent());
        vo.setSize(po.getSize());
        return vo;
    }
}
