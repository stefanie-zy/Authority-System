package com.stefanie.userserver.manager;

import cn.hutool.core.io.resource.InputStreamResource;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.stefanie.userserver.common.enums.core.CommonEnum;
import com.stefanie.userserver.common.enums.error.UtilErrorEnum;
import com.stefanie.userserver.config.param.FileConfig;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.handle.result.GoFastdfsResult;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: stefanie
 * @createTime: 2023/07/14 15:57
 * @description:
 */
@Component
@Slf4j
@RequiredArgsConstructor
@ApiModel(value = "Manager-GoFastdfs")
public class GoFastdfsManager {

    @ApiModel(value = "内部枚举-GoFastdfs接口类型")
    @AllArgsConstructor
    @Getter
    private static enum TypeEnum implements CommonEnum {
        UPLOAD(1, "上传"),
        DELETE(2, "删除"),
        INFO(3, "文件详情"),
        COUNT(4, "统计"),
        ;
        private Integer code;
        private String message;
    }

    private final FileConfig fileConfig;

    @ApiOperation(value = "文件上传：返回实体")
    public GoFastdfsResult upload(MultipartFile file, String path) {
        GoFastdfsResult goFastdfsResult = new GoFastdfsResult();
        try {
            InputStreamResource in = new InputStreamResource(file.getInputStream(), file.getOriginalFilename());
            Map<String, Object> params = new HashMap<>();
            params.put("file", in);
            params.put("path", path);
            params.put("output", fileConfig.getGoFastdfs().getOutput());
            String result = HttpUtil.post(getUrl(TypeEnum.UPLOAD.getCode()), params);
            log.info("请求地址：{},请求结果：{}", getUrl(TypeEnum.UPLOAD.getCode()), result);
            goFastdfsResult = JSONUtil.toBean(result, GoFastdfsResult.class);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Manager-GoFastdfs：文件上传失败");
        }
        SysException.check((goFastdfsResult.getRetcode() == 0), UtilErrorEnum.S_UTIL_FILE_UPLOAD_ERROR);
        return goFastdfsResult;
    }

    @ApiOperation(value = "根据MD5删除服务器文件")
    public void deleteByMd5(String md5) {
        try {
            SysException.check(StringUtil.isNotEmpty(md5), UtilErrorEnum.S_UTIL_FILE_MD5_NULL);
            Map<String, Object> map = new HashMap<>();
            map.put("md5", md5);
            String result = HttpUtil.post(getUrl(TypeEnum.DELETE.getCode()), map);
            log.info("Manager-GoFastdfs：文件删除结果：{}", result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Manager-GoFastdfs：文件删除失败");
        }
    }

    @ApiOperation(value = "根据path删除服务器文件")
    public void deleteByPath(String path) {
        try {
            SysException.check(StringUtil.isNotEmpty(path), UtilErrorEnum.S_UTIL_FILE_PATH_NULL);
            Map<String, Object> map = new HashMap<>();
            map.put("path", path);
            String result = HttpUtil.post(getUrl(TypeEnum.DELETE.getCode()), map);
            log.info("Manager-GoFastdfs：文件删除结果：{}", result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Manager-GoFastdfs：文件删除失败");
        }
    }

    @ApiOperation(value = "根据MD5查询文件详情")
    public void getFileInfoByMd5(String md5) {
        try {
            SysException.check(StringUtil.isNotEmpty(md5), UtilErrorEnum.S_UTIL_FILE_MD5_NULL);
            Map<String, Object> map = new HashMap<>();
            map.put("md5", md5);
            String result = HttpUtil.post(getUrl(TypeEnum.INFO.getCode()), map);
            log.info("Manager-GoFastdfs：文件详情：{}", result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Manager-GoFastdfs：查询文件详情失败");
        }
    }

    @ApiOperation(value = "根据path查询文件详情")
    public void getFileInfoByPath(String path) {
        try {
            SysException.check(StringUtil.isNotEmpty(path), UtilErrorEnum.S_UTIL_FILE_PATH_NULL);
            Map<String, Object> map = new HashMap<>();
            map.put("path", path);
            String result = HttpUtil.post(getUrl(TypeEnum.INFO.getCode()), map);
            log.info("Manager-GoFastdfs：文件详情：{}", result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Manager-GoFastdfs：查询文件详情失败");
        }
    }

    @ApiOperation(value = "统计文件信息")
    public void getFileStat() {
        try {
            String result = HttpUtil.post(getUrl(TypeEnum.COUNT.getCode()), new HashMap<>());
            log.info("Manager-GoFastdfs：统计文件信息：{}", result);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Manager-GoFastdfs：统计文件信息失败！");
        }
    }


    @ApiOperation(value = "获取服务的请求地址")
    private String getUrl(Integer type) {
        String url = fileConfig.getGoFastdfs().getUrl() + fileConfig.getGoFastdfs().getGroup();
        TypeEnum enumByCode = CommonEnum.getEnumByCode(TypeEnum.class, type);
        switch (enumByCode) {
            case UPLOAD:
                return url + fileConfig.getGoFastdfs().getUpload();
            case DELETE:
                return url + fileConfig.getGoFastdfs().getDelete();
            case INFO:
                return url + fileConfig.getGoFastdfs().getInfo();
            case COUNT:
                return url + fileConfig.getGoFastdfs().getStat();
            default:
                return null;
        }
    }
}
