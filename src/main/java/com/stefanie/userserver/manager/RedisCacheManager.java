package com.stefanie.userserver.manager;

import cn.hutool.json.JSONUtil;
import com.stefanie.userserver.domain.core.CommonPo;
import com.stefanie.userserver.utils.other.StringUtil;
import com.stefanie.userserver.utils.redis.RedisUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-11 19:06
 * @description Redis缓存业务转换
 */
@Component
@Slf4j
@ApiModel(value = "Manager-Redis缓存业务转换")
@RequiredArgsConstructor
public class RedisCacheManager<T> {

    private final RedisUtil redisUtil;

    @ApiOperation(value = "添加-任意Bean", notes = "time为null默认不设置过期时间；timeUnit为null默认为秒")
    public void saveBean(String key, T t, long time, TimeUnit timeUnit) {
        if (StringUtil.isNull(t) || StringUtil.isEmpty(key)) {
            log.error("Manager-Redis——添加Bean失败！");
            return;
        }
        String value = JSONUtil.toJsonStr(t);
        if (time == 0) {
            redisUtil.setString(key, value);
            return;
        }
        redisUtil.setString(key, value, time, ((StringUtil.isNull(timeUnit) ? TimeUnit.SECONDS : timeUnit)));
    }

    @ApiOperation(value = "添加-Bean(Po)。key默认为：className+id", notes = "time为null默认不设置过期时间；timeUnit为null默认为秒")
    public <T extends CommonPo> void saveBean(T t, long time, TimeUnit timeUnit) {
        if (StringUtil.isNull(t)) {
            return;
        }
        String key = getKey(t.getClass().getName(), t.getId());
        String value = JSONUtil.toJsonStr(t);
        if (time == 0) {
            redisUtil.setString(key, value);
            return;
        }
        redisUtil.setString(key, value, time, ((StringUtil.isNull(timeUnit) ? TimeUnit.SECONDS : timeUnit)));
    }

    @ApiOperation(value = "查询-任意Bean")
    public <T> T getBean(String key, Class<T> tClass) {
        String value = redisUtil.getStringValue(key);
        if (StringUtil.isEmpty(value)) {
            return null;
        }
        T bean = JSONUtil.parseObj(value).toBean(tClass);
        if (!StringUtil.isNull(bean)) {
            return bean;
        }
        return null;
    }

    @ApiOperation(value = "查询-Bean(Po)。key默认为：className+id")
    public <T extends CommonPo> T getBean(Long id, Class<T> tClass) {
        String key = getKey(tClass.getName(), id);
        String value = redisUtil.getStringValue(key);
        if (StringUtil.isEmpty(value)) {
            return null;
        }
        T bean = JSONUtil.parseObj(value).toBean(tClass);
        if (!StringUtil.isNull(bean)) {
            return bean;
        }
        return null;
    }

    @ApiOperation(value = "删除-Key")
    public void delete(String key) {
        if (redisUtil.hasKey(key) && StringUtil.isNotEmpty(key)) {
            redisUtil.delete(key);
        }
    }

    @ApiOperation(value = "拼接Key")
    private String getKey(String className, Long id) {
        return className + ";id=" + id;
    }


}
