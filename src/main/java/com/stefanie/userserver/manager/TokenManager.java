package com.stefanie.userserver.manager;

import com.stefanie.userserver.common.enums.error.ServerCallErrorEnum;
import com.stefanie.userserver.config.param.TokenConfig;
import com.stefanie.userserver.constant.bus.RedisKeyConstant;
import com.stefanie.userserver.constant.sys.TimeConstant;
import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.mapper.sys.UserMapper;
import com.stefanie.userserver.utils.ip.AddressUtil;
import com.stefanie.userserver.utils.ip.IpUtil;
import com.stefanie.userserver.utils.other.DateUtil;
import com.stefanie.userserver.utils.other.ServletUtil;
import com.stefanie.userserver.utils.uuid.IdUtil;
import eu.bitwalker.useragentutils.UserAgent;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 17:02
 * @description 令牌管理器
 */
@Component
@Slf4j
@RequiredArgsConstructor
@ApiModel(value = "Manager-Token")
public class TokenManager {

    private final UserMapper userMapper;
    private final TokenConfig tokenConfig;
    private final RedisCacheManager redisCacheManager;

    @ApiOperation(value = "创建令牌")
    public String createToken(LoginUserBo loginUserBo) {
        String token = IdUtil.fastUUID();
        loginUserBo.setToken(token);
        updateUserAgent(loginUserBo);
        updateRedisToken(loginUserBo);
        updateUserToken(loginUserBo);

        Map<String, Object> claims = new HashMap<>();
        claims.put(RedisKeyConstant.LOGIN_USER_KEY, token);
        return createToken(claims);
    }

    @ApiOperation(value = "更新Redis缓存")
    public void updateRedisToken(LoginUserBo loginUserBo) {
        long expireTime = System.currentTimeMillis() + tokenConfig.getExpireTime() * TimeConstant.MILLIS_MINUTE;
        loginUserBo.setExpireTime(DateUtil.getDateTime(expireTime));
        // redis缓存key
        String userKey = getToken(loginUserBo.getToken());
        redisCacheManager.saveBean(userKey, loginUserBo, expireTime, TimeUnit.MINUTES);
    }

    @ApiOperation(value = "从Redis中获取登录用户信息")
    public LoginUserBo getLoginUserBo(HttpServletRequest request) {
        LoginUserBo loginUserBo = null;
        String token = getToken(request);
        if (token != null) {
            try {
                Claims claims = parseToken(token);
                // 解析对应的用户信息
                String uuid = (String) claims.get(RedisKeyConstant.LOGIN_USER_KEY);
                // 获取redis缓存key
                String userKey = getToken(uuid);
                loginUserBo = (LoginUserBo) redisCacheManager.getBean(userKey, LoginUserBo.class);
            } catch (Exception e) {
                throw new SysException(ServerCallErrorEnum.S_REDIS_SELECT_ERROR);
            }
        }
        return loginUserBo;
    }

    @ApiOperation(value = "删除Redis-Token")
    public void deleteRedisToken(String token) {
        redisCacheManager.delete(getToken(token));
    }

    @ApiOperation(value = "设置代理用户")
    private void updateUserAgent(LoginUserBo loginUserBo) {
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtil.getRequest().getHeader("User-Agent"));
        String ip = IpUtil.getIpAddr(ServletUtil.getRequest());
        loginUserBo.setIp(ip);
        loginUserBo.setBrowser(userAgent.getBrowser().getName());
        loginUserBo.setOs(userAgent.getOperatingSystem().getName());
        loginUserBo.setLoginAddr(AddressUtil.getRealAddressByIp(ip));
    }

    @ApiOperation(value = "更新用户基础信息Token")
    private void updateUserToken(LoginUserBo loginUserBo) {
        UserPo userPo = userMapper.selectById(loginUserBo.getUserPo().getId());
        userPo.setToken(loginUserBo.getToken());
        userMapper.updateById(userPo);
    }

    @ApiOperation(value = "获取请求头中的token")
    private String getToken(HttpServletRequest request) {
        return request.getHeader(tokenConfig.getHeader());
    }

    @ApiOperation(value = "从令牌中获取数据声明")
    private Claims parseToken(String token) {
        return Jwts.parser().setSigningKey(tokenConfig.getSecretKey()).parseClaimsJws(token).getBody();
    }

    @ApiOperation(value = "从数据声明生成令牌")
    private String createToken(Map<String, Object> claims) {
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, tokenConfig.getSecretKey()).compact();
    }

    @ApiOperation(value = "拼接token-key")
    private String getToken(String uuid) {
        return RedisKeyConstant.LOGIN_USER_KEY + uuid;
    }

}
