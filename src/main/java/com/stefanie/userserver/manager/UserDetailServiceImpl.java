package com.stefanie.userserver.manager;

import com.stefanie.userserver.common.enums.error.UserErrorEnum;
import com.stefanie.userserver.common.enums.sys.DelFlagEnum;
import com.stefanie.userserver.common.enums.sys.UserStatusEnum;
import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.mapper.sys.UserMapper;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-05 20:12
 * @description 用户验证处理
 */
@Service
@Slf4j
@RequiredArgsConstructor
@ApiModel(value = "Manager-用户授权登录", description = "Security-授权实现类")
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserMapper userMapper;
    private final StefanieConfig stefanieConfig;

    @ApiOperation(value = "用户登录")
    @Override
    public UserDetails loadUserByUsername(String loginName) throws UsernameNotFoundException {
        UserPo userPo = userMapper.selectUserPoByLoginName(loginName);
        if (StringUtil.isNull(userPo)) {
            log.info("登录用户：{} 不存在", loginName);
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        if (DelFlagEnum.YES.getCode().equals(userPo.getDelFlag())) {
            log.info("登录用户：{} 已被删除", loginName);
            throw new UserException(UserErrorEnum.B_USER_DELETE);
        }
        if (UserStatusEnum.LOCK.getCode().equals(userPo.getStatus())) {
            log.info("登录用户：{} 已被锁定", loginName);
            throw new UserException(UserErrorEnum.B_USER_LOCK);
        }
        if (UserStatusEnum.WRITE_OFF.getCode().equals(userPo.getSex())) {
            log.info("登录用户：{} 已被注销", loginName);
            throw new UserException(UserErrorEnum.B_USER_WRITE_OFF);
        }
        return createLoginUser(userPo);
    }

    @ApiOperation(value = "创建缓存用户")
    public UserDetails createLoginUser(UserPo userPo) {
        return new LoginUserBo(userPo);
    }

}
