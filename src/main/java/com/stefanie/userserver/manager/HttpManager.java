package com.stefanie.userserver.manager;

import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.constant.sys.EncodingConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 18:22
 * @description Http调用
 */
@Component
@Slf4j
@RequiredArgsConstructor
@ApiModel(value = "Manager-Http")
public class HttpManager {

    @ApiOperation(value = "Http-Get请求", notes = "url:基础请求地址")
    public static String sendGet(String url) {
        return sendGet(url, null, EncodingConstant.UTF_8);
    }

    @ApiOperation(value = "Http-Get请求", notes = "url:基础请求地址;contentType:编码类型")
    public static String sendGet(String url, String contentType) {
        return sendGet(url, null, contentType);
    }

    @ApiOperation(value = "Http-Get请求", notes = "url:基础请求地址;paramMap:请求参数;contentType:编码类型")
    public static String sendGet(String url, Map<String, String> paramMap, String contentType) {
        StringBuilder result = new StringBuilder();
        String requestParam = mapToStr(paramMap);
        BufferedReader in = null;
        String readUrl = url + "?" + requestParam;

        try {
            log.info("Http-Get请求：{}", readUrl);
            URLConnection connection = getUrlConnection(readUrl);
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), contentType));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
            log.info("Http-Get请求：{}，请求结果：{}", readUrl, result);
        } catch (ConnectException e) {
            log.error("Http-Get请求：{}，请求连接异常：{}", readUrl, e.getMessage());
        } catch (SocketTimeoutException e) {
            log.error("Http-Get请求：{}，请求超时异常：{}", readUrl, e.getMessage());
        } catch (IOException e) {
            log.error("Http-Get请求：{}，请求IO异常：{}", readUrl, e.getMessage());
        } catch (Exception e) {
            log.error("Http-Get请求：{}，请求异常：{}", readUrl, e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                log.error("Http-Get请求：{}，流关闭异常：{}", readUrl, ex.getMessage());
            }
        }
        return result.toString();
    }

    @ApiOperation(value = "Http-Post请求", notes = "url:基础请求地址;paramMap:请求参数;contentType:编码类型")
    public static String sendPost(String url, Map<String, String> paramMap, String contentType) {
        PrintWriter out = null;
        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        String requestParam = mapToStr(paramMap);

        try {
            log.info("Http-Post请求：{},请求参数：{}", url, requestParam);
            URLConnection connection = getUrlConnection(url);
            connection.setRequestProperty("Accept-Charset", "utf-8");
            connection.setRequestProperty("contentType", "utf-8");
            connection.setDoOutput(true);
            connection.setDoInput(true);

            out = new PrintWriter(connection.getOutputStream());
            out.print(requestParam);
            out.flush();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
            log.info("Http-Post请求：{}，请求结果：{}", url, result);
        } catch (ConnectException e) {
            log.error("Http-Post请求：{}，请求连接异常：{}", url, e.getMessage());
        } catch (SocketTimeoutException e) {
            log.error("Http-Post请求：{}，请求超时异常：{}", url, e.getMessage());
        } catch (IOException e) {
            log.error("Http-Post请求：{}，请求IO异常：{}", url, e.getMessage());
        } catch (Exception e) {
            log.error("Http-Post请求：{}，请求异常：{}", url, e.getMessage());
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                log.error("Http-Post请求：{}，流关闭异常：{}", url, ex.getMessage());
            }
        }
        return result.toString();

    }

    @ApiOperation(value = "请求连接")
    private static URLConnection getUrlConnection(String url) throws IOException {
        URL realUrl = new URL(url);
        URLConnection urlConnection = realUrl.openConnection();
        urlConnection.setConnectTimeout(30000);
        urlConnection.setRequestProperty("accept", "*/*");
        urlConnection.setRequestProperty("connection", "Keep-Alive");
        urlConnection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        return urlConnection;

    }

    @ApiOperation(value = "解析请求参数")
    private static String mapToStr(Map<String, String> paramMap) {
        StringBuilder reqStr = new StringBuilder();
        if (paramMap != null && !paramMap.isEmpty()) {
            for (Map.Entry<String, String> entry : paramMap.entrySet()) {
                reqStr.insert(0, entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        return reqStr.toString();
    }
}
