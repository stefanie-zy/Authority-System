package com.stefanie.userserver.utils.uuid;

import com.stefanie.userserver.common.enums.error.UtilErrorEnum;
import com.stefanie.userserver.exception.sys.SysException;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 17:07
 * @description 提供通用的唯一识别码
 */
public final class UUID implements Serializable, Comparable<UUID> {

    // TODO 没搞懂这块内容

    private static final long serialVersionUID = 3488787083698368677L;

    /**
     * 此UUID的最高64有效位
     */
    private final long mostSigBits;

    /**
     * 此UUID的最低64有效位
     */
    private final long leastSigBits;

    private static class Holder {
        static final SecureRandom numberGenerator = getSecureRandom();
    }

    public static SecureRandom getSecureRandom() {
        try {
            return SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            throw new SysException(UtilErrorEnum.S_UTIL_UUID_ERROR);
        }
    }

    public static UUID fastUUID() {
        return randomUUID(false);
    }

    public static UUID randomUUID(boolean isSecure) {
        final Random ng = isSecure ? Holder.numberGenerator : getRandom();

        byte[] randomBytes = new byte[16];
        ng.nextBytes(randomBytes);
        randomBytes[6] &= 0x0f; /* clear version */
        randomBytes[6] |= 0x40; /* set to version 4 */
        randomBytes[8] &= 0x3f; /* clear variant */
        randomBytes[8] |= 0x80; /* set to IETF variant */
        return new UUID(randomBytes);
    }

    public static ThreadLocalRandom getRandom() {
        return ThreadLocalRandom.current();
    }

    private UUID(byte[] data) {
        long msb = 0;
        long lsb = 0;
        assert data.length == 16 : "data must be 16 bytes in length";
        for (int i = 0; i < 8; i++) {
            msb = (msb << 8) | (data[i] & 0xff);
        }
        for (int i = 8; i < 16; i++) {
            lsb = (lsb << 8) | (data[i] & 0xff);
        }
        this.mostSigBits = msb;
        this.leastSigBits = lsb;
    }


    @Override
    public int compareTo(@NotNull UUID val) {
        return (this.mostSigBits < val.mostSigBits ? -1 : (this.mostSigBits > val.mostSigBits ? 1 : (Long.compare(this.leastSigBits, val.leastSigBits))));
    }
}
