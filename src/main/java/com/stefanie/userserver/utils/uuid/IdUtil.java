package com.stefanie.userserver.utils.uuid;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 17:05
 * @description uuid工具类
 */
@Component
@ApiModel(value = "工具类-Id")
@Slf4j
public class IdUtil {

    @ApiOperation(value = "生成fastUUID")
    public static String fastUUID() {
        return UUID.fastUUID().toString();
    }
}
