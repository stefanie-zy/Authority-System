package com.stefanie.userserver.utils.security;

import com.stefanie.userserver.common.enums.error.UserErrorEnum;
import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.exception.sys.UserException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-24 21:54
 * @description 安全工具类
 */
@Component
@Slf4j
@ApiModel(value = "工具类-安全")
public class SecurityUtils {

    @ApiOperation(value = "获取Authentication")
    public static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @ApiOperation(value = "获取用户")
    public static LoginUserBo getLoginUser() {
        try {
            return (LoginUserBo) getAuthentication().getPrincipal();
        } catch (Exception e) {
            throw new UserException(UserErrorEnum.B_USER_SECURITY_NULL);
        }
    }

    @ApiOperation(value = "生成BCryptPasswordEncoder密码")
    public static String encryptPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    @ApiOperation(value = "判断密码是否相同")
    public static Boolean matchsPassword(String rawPassword, String encodedPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }
}
