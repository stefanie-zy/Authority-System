package com.stefanie.userserver.utils.automation;


import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import io.swagger.annotations.ApiModel;

import java.util.Collections;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-25 20:13
 * @description 代码生成工具
 */
@ApiModel(value = "工具类-代码生成")
public class GeneratorUtil {

    public static void main(String[] args) {
        generator();
    }

    private static void generator() {
        FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/stefanie?servetTimezone=GMT%2B8", "root", "root").globalConfig(builder -> {
                    builder.author("zy") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("G:\\01-code\\01-stefanie\\userserver\\src\\main\\java\\"); // 指定输出目录
                }).packageConfig(builder -> {
                    builder.parent("com.stefanie.userserver") // 设置父包名
                            .moduleName("") // 设置父包模块名
                            .entity("domain")// 设置entity
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "G:\\01-code\\01-stefanie\\userserver\\src\\main\\resources\\mapper\\sys\\")); // 设置mapperXml生成路径
                }).strategyConfig(builder -> {
                    builder.entityBuilder().enableLombok();// 开启Lombok
                    builder.entityBuilder().enableTableFieldAnnotation();
                    builder.mapperBuilder().enableMapperAnnotation().build();
                    builder.controllerBuilder().enableHyphenStyle().enableRestStyle();
                    builder.addInclude("sys_menu")
                            .addTablePrefix("t_", "sys_"); // 设置过滤表前缀
                })
//                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
