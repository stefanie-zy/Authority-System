package com.stefanie.userserver.utils.grep;


import cn.hutool.core.util.DesensitizedUtil;
import com.stefanie.userserver.common.enums.error.UtilErrorEnum;
import com.stefanie.userserver.constant.sys.GrepConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 18:25
 * @description 正则表达式工具类
 */
@Slf4j
@Component
@ApiModel(value = "工具类-正则表达式")
public class GrepUtil {

    @ApiOperation(value = "是否全是中文", notes = "true:全是中文；false:不全是")
    public static boolean isAllChinese(String str) {
        return isCommon(str, GrepConstant.CHINESE);
    }

    @ApiOperation(value = "是否包含中文", notes = "true:包含；false:不包含")
    public static boolean isContainsChinese(String str) {
        boolean result = false;
        try {
            if (str != null) {
                Pattern pattern = Pattern.compile(GrepConstant.CHINESE);
                Matcher matcher = pattern.matcher(str);
                result = matcher.find();
            }
        } catch (Exception e) {
            log.error("正则表达式工具类——{}", UtilErrorEnum.S_UTIL_GREP_ERROR.getMessage());
        }
        return result;
    }

    @ApiOperation(value = "是否为手机号", notes = "true:是；false:不是")
    public static boolean isPhoneNumber(String str) {
        return isCommon(str, GrepConstant.PHONE_NUMBER);
    }

    @ApiOperation(value = "是否为邮箱", notes = "true:是；false:不是")
    public static boolean isEmail(String str) {
        return isCommon(str, GrepConstant.EMAIL);
    }

    @ApiOperation(value = "是否为身份证", notes = "true:是；false:不是")
    public static boolean isIdCard(String str) {
        return isCommon(str, GrepConstant.ID_CARD);
    }

    @ApiOperation(value = "手机号脱敏",notes = "默认展示前3位后4位")
    public static String phoneNumberHide(String phoneNumber) {
        return DesensitizedUtil.mobilePhone(phoneNumber);
    }

    @ApiOperation(value = "邮箱脱敏")
    public static String emailHide(String email) {
        return DesensitizedUtil.email(email);
    }

    @ApiOperation(value = "身份证脱敏", notes = "默认展示前3位后4位")
    public static String idCardHide(String idCard) {
        return DesensitizedUtil.idCardNum(idCard, 3, 4);
    }

    @ApiOperation(value = "地址脱敏", notes = "默认隐藏后6位")
    public static String addressHide(String address) {
        return DesensitizedUtil.address(address, 6);
    }

    @ApiOperation(value = "公共规则校验，只支持matcher.matches()方法", notes = "true:一致；false:不一致")
    private static boolean isCommon(String str, String grepStr) {
        boolean result = false;
        try {
            if (!StringUtils.isEmpty(str) && !StringUtils.isEmpty(grepStr)) {
                Pattern pattern = Pattern.compile(grepStr);
                Matcher matcher = pattern.matcher(str);
                result = matcher.matches();
            }
        } catch (Exception e) {
            log.error("正则表达式工具类——{}", UtilErrorEnum.S_UTIL_GREP_ERROR.getMessage());
        }
        return result;
    }
}
