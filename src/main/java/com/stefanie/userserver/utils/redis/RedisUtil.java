package com.stefanie.userserver.utils.redis;

import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-10 10:43
 * @description
 */
@Component
@ApiModel(value = "工具类-Redis")
@RequiredArgsConstructor
@Slf4j
public class RedisUtil {

    private final RedisTemplate redisTemplate;

    protected final String STR_NULL = "";

    protected final Long ZERO = 0L;

    ///#region Key

    @ApiOperation(value = "判断Key是否存在")
    public boolean hasKey(String key) {
        return !StringUtil.isEmpty(key) && Boolean.TRUE.equals(redisTemplate.hasKey(key));
    }

    @ApiOperation(value = "删除Key")
    public boolean delete(String key) {
        return !StringUtil.isEmpty(key) && Boolean.TRUE.equals(redisTemplate.delete(key));
    }

    @ApiOperation(value = "批量删除Key")
    public Long delete(Collection<String> keys) {
        return keys.size() == 0 ? Long.valueOf(0L) : redisTemplate.delete(keys);
    }

    @ApiOperation(value = "获取key的过期时间")
    public long getExpireTime(String key, TimeUnit timeUnit) {
        if (hasKey(key)) {
            return 0;
        }
        return redisTemplate.getExpire(key, timeUnit);
    }

    @ApiOperation(value = "给指定Key添加过期时间")
    public boolean expire(String key, long time, TimeUnit timeUnit) {
        return (time != 0) && Boolean.TRUE.equals(redisTemplate.expire(key, time, timeUnit));
    }

    @ApiOperation(value = "移除Key的过期时间", notes = "Key将保持永久")
    public boolean persist(String key) {
        return hasKey(key) && Boolean.TRUE.equals(redisTemplate.persist(key));
    }

    @ApiOperation(value = "查询Key的类型")
    public DataType type(String key) {
        return redisTemplate.type(key);
    }

    @ApiOperation(value = "修改Key名称")
    public void reName(String oldName, String newName) {
        redisTemplate.rename(oldName, newName);
    }


    @ApiOperation(value = "给指定Key赋值，并设置过期时间")
    public <T> void set(String key, T value, long time, TimeUnit timeUnit) {
        if (StringUtil.isNull(value) || time == 0) {
            return;
        }
        redisTemplate.opsForValue().set(key, value, time, timeUnit);
    }

    ///#endregion Key

    ///#region String

    @ApiOperation(value = "String-给指定Key赋值")
    public void setString(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    @ApiOperation(value = "String-给指定Key赋值，并设置超时时间")
    public void setString(String key, String value, long time, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, time, timeUnit);
    }

    @ApiOperation(value = "String-批量添加")
    public void mulitSet(Map<String, String> map) {
        redisTemplate.opsForValue().multiSet(map);
    }

    @ApiOperation(value = "String-获取指定Key的值")
    public String getStringValue(String key) {
        return (String) redisTemplate.opsForValue().get(key);
    }

    ///#endregion String

    ///#region Set

    @ApiOperation(value = "Set-给指定KetSet赋值")
    public Long setSet(String key, String value) {
        if (!StringUtil.isEmpty(key)) {
            return redisTemplate.opsForSet().add(key, value);
        }
        return ZERO;
    }

    @ApiOperation(value = "Set-获取指定Key值的Set长度")
    public Long sizeSet(String key) {
        if (StringUtil.isEmpty(key)) {
            return redisTemplate.opsForSet().size(key);
        }
        return ZERO;
    }

    @ApiOperation(value = "Set-判断指定Key值是否存在")
    public boolean isSetMember(String key, String value) {
        if (hasKey(key) || StringUtil.isNull(value)) {
            return redisTemplate.opsForSet().isMember(key, value);
        }
        return Boolean.FALSE;
    }

    @ApiOperation(value = "Set-获取指定Key的值")
    public Set<String> getSet(String key) {
        if (hasKey(key)) {
            return redisTemplate.opsForSet().members(key);
        }
        return null;
    }

    @ApiOperation(value = "Set-删除指定Key的值")
    public Long deleteSet(String key, String value) {
        if (hasKey(key) && !StringUtil.isNull(value)) {
            return redisTemplate.opsForSet().remove(key, value);
        }
        return ZERO;

    }

    ///#endregion Set

    ///#region 自定义

    @ApiOperation(value = "接口限流处理", notes = "参数:time-限流时间范围;count-请求次数\n返回结果:true-正常访问;false-限流处理")
    public boolean limitIncr(String key, long time, int count) {
        if (time < 0 || count < 0) {
            return false;
        }
        Long curcount = redisTemplate.opsForValue().increment(key, 1);
        if (Long.valueOf(1).equals(curcount)) {
            set(key, 1, time, TimeUnit.SECONDS);
        }
        return curcount <= (long) count;
    }

    ///#endregion 自定义

}

