package com.stefanie.userserver.utils.ip;


import com.alibaba.fastjson.JSONObject;
import com.stefanie.userserver.constant.sys.EncodingConstant;
import com.stefanie.userserver.constant.sys.OtherConstant;
import com.stefanie.userserver.manager.HttpManager;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 17:50
 * @description 获取地址工具类
 */
@Component
@Slf4j
@ApiModel(value = "工具类-获取地址")
public class AddressUtil {

    @ApiOperation(value = "根据IP获取请求地理位置")
    public static String getRealAddressByIp(String ip) {
        // 判断是否为内网
        if (IpUtil.internalIp(ip)) {
            return OtherConstant.IP_URL;
        }
        // 请求参数拼接
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("ip", ip);
        requestMap.put("json", "true");
        try {
            // 获取地址
            String requestResult = HttpManager.sendGet(OtherConstant.IP_URL, requestMap, EncodingConstant.GBK);
            if (StringUtil.isEmpty(requestResult)) {
                log.error("地址工具类——获取地理位置信息异常：{}", ip);
                return OtherConstant.UNKNOWN_ADDRESS;
            }
            JSONObject obj = JSONObject.parseObject(requestResult);
            String region = obj.getString("pro");
            String city = obj.getString("city");
            return String.format("%s %s", region, city);
        } catch (Exception e) {
            log.error("地址工具类——获取地理位置信息异常：{}", ip);
        }
        return OtherConstant.UNKNOWN_ADDRESS;
    }

}
