package com.stefanie.userserver.utils.mapstruct;

import com.stefanie.userserver.domain.pojo.dto.sys.*;
import com.stefanie.userserver.domain.pojo.po.sys.*;
import com.stefanie.userserver.domain.pojo.vo.sys.*;
import io.swagger.annotations.ApiModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 15:21
 * @description 系统用户系统对象转换接口
 */
@ApiModel(value = "系统用户系统对象转换接口")
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BeanMapStructMapper {

    /**
     * 注解说明
     * Mapping(target = "createTime", source = "createTime", dateFormat = "yyyy-MM-dd")//给日期设置格式
     * Mapping(target = "age", source = "age", numberFormat = "#0.00")//字符串转数字，做处理
     * Mapping(target = "id", ignore = true) //字段赋值没有了
     * Mapping(target = "userVerified", defaultValue = "defaultValue-2")//userVerified：如果为null赋值默认值
     * Mapping(target = "email", expression = "java(com.stefanie.userserver.utils.other.EncryptionUtil.decode(po.getEmail()))
     * })
     */

    UserPo createDtoToPo(UserCreateDto dto);

    UserVo createPoToVo(UserPo po);

    LogVo createPoToVo(LogPo po);

    LoginLogVo createPoToVo(LoginLogPo po);

    RolePo createDtoToPo(RoleCreateDto dto);

    MenuPo createDtoToPo(MenuCreateDto dto);

    TreeMenuVo createPoToTreeVo(MenuPo po);

    MenuVo createPoToVo(MenuPo po);

    DictTypePo createDtoToPo(DictTypeCreateDto dto);

    DictTypeVo createPoToVo(DictTypePo po);

    DictDataPo createDtoToPo(DictDataCreateDto dto);

    DictDataVo createPoToVo(DictDataPo po);

    FileVo createPoToVo(FilePo po);

}
