package com.stefanie.userserver.utils.other;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-13 20:34
 * @description 加密工具类
 */
@ApiModel(value = "工具-加解密")
public class EncryptionUtil {

    @ApiOperation(value = "字符串加密", notes = "简单加密")
    public static String encrypt(String str) {
        return Base64.getEncoder().encodeToString(str.getBytes(StandardCharsets.UTF_8));
    }

    @ApiOperation(value = "字符串解密", notes = "简单解密")
    public static String decode(String str) {
        return new String(Base64.getDecoder().decode(str.getBytes(StandardCharsets.UTF_8)));
    }
}
