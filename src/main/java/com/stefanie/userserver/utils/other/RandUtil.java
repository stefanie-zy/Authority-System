package com.stefanie.userserver.utils.other;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import java.security.SecureRandom;

/**
 * @author: stefanie
 * @createTime: 2023/09/21 9:07
 * @description: 随机数工具类
 */
@ApiModel(value = "工具类-随机数")
@Slf4j
public class RandUtil {
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String DIGITS = "0123456789";
    private static final String SPECIAL = "!@#$%^&*()-_+=<>?";


    @ApiOperation(value = "根据长度获取随机数（可能重复）")
    public static String getRandByLength(int length) {
        String allCharacters = UPPER + LOWER + DIGITS + SPECIAL;
        SecureRandom random = new SecureRandom();
        StringBuilder randomStr = new StringBuilder(length);
        while (randomStr.length() < length) {
            char randomChar = allCharacters.charAt(random.nextInt(allCharacters.length()));
            randomStr.append(randomChar);
        }
        return randomStr.toString();
    }
}
