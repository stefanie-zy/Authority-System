package com.stefanie.userserver.utils.other;

import com.stefanie.userserver.constant.sys.DateFormatConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-01 22:24
 * @description 时间工具类
 */
@ApiModel(value = "工具类-日期/时间")
@Component
public class DateUtil {

    @ApiOperation(value = "获取“yyyy-MM-dd HH:mm:ss”格式的当前日期")
    public static String getDfDateTime() {
        return new SimpleDateFormat(DateFormatConstant.FORMAT_1).format(new Date());
    }

    @ApiOperation(value = "获取“yyyy-MM-dd HH:mm:ss”格式的特定日期")
    public static String getDfDateTime(Date date) {
        return new SimpleDateFormat(DateFormatConstant.FORMAT_1).format(date);
    }

    @ApiOperation(value = "将“yyyyMMddHHmmss”格式的时间转换为“yyyy-MM-dd HH:mm:ss”的时间")
    @SneakyThrows
    public static String getDfDateTimeByDate(String dateTime) {
        SimpleDateFormat simpleDateFormatDf = new SimpleDateFormat(DateFormatConstant.FORMAT_1);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateFormatConstant.FORMAT_2);
        Date date = simpleDateFormat.parse(dateTime);
        return simpleDateFormatDf.format(date);
    }

    @ApiOperation(value = "获取特定格式的当前日期")
    public static String getDateTime(String format) {
        return new SimpleDateFormat(format).format(new Date());
    }

    @ApiOperation(value = "时间戳转特定的时间格式")
    public static String getDateTime(long time, String format) {
        return new SimpleDateFormat(format).format(time);
    }

    @ApiOperation(value = "特定格式之间的时间转换")
    @SneakyThrows
    public static String getDateTime(String time, String sourceFormat, String targetFormat) {
        Date data = new SimpleDateFormat(sourceFormat).parse(time);
        return new SimpleDateFormat(targetFormat).format(data);
    }

    @ApiOperation(value = "根据时间戳获取日期")
    public static Date getDateTime(long time) {
        return new Date(time);
    }

    @ApiOperation(value = "时间差", notes = "单位：天")
    @SneakyThrows
    public static Long compare(String startStr, String endStr, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date startDate = simpleDateFormat.parse(startStr);
        Date endDate = simpleDateFormat.parse(endStr);
        return (endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);
    }

}
