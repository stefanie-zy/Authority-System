package com.stefanie.userserver.utils.other;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023/06/09 11:00
 * @description 字符串工具类
 */
@ApiModel(value = "工具类-对象/字符串")
public class StringUtil extends StringUtils {

    @ApiModelProperty(value = "空字符串")
    private static final String STR_NULL = "";

    @ApiModelProperty(value = "下划线")
    private static final String SEPARATOR = "_";

    @ApiOperation(value = "判断对象是否为NULL")
    public static boolean isNull(Object object) {
        return null == object;
    }

    @ApiOperation(value = "判断对象不为NULL")
    public static boolean isNotNull(Object object) {
        return !isNull(object);
    }

    @ApiOperation(value = "判断字符串是否为空")
    public static boolean isEmpty(String str) {
        return isNull(str) || STR_NULL.equals(str.trim());
    }

    @ApiOperation(value = "判断字符串不为空")
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    @ApiOperation(value = "判断数组是否为空")
    public static boolean isEmpty(Object[] objects) {
        return isNull(objects) || (objects.length == 0);
    }

    @ApiOperation(value = "判断数组不为空")
    public static boolean isNotEmpty(Object[] objects) {
        return !isNull(objects);
    }

    @ApiOperation(value = "判断Map是否为空")
    public static boolean isEmpty(Map<?, ?> map) {
        return isNull(map) || map.isEmpty();
    }

    @ApiOperation(value = "判断Map不为空")
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

    @ApiOperation(value = "去字符串空格")
    public static String trim(String str) {
        return (str == null ? "" : str.trim());
    }

    @ApiOperation(value = "截取字符串")
    public static String subString(String str, int start) {
        if (isEmpty(str)) {
            return STR_NULL;
        }
        if (!(0 <= start)) {
            start = str.length() + start;
        }
        if (!(0 <= start)) {
            start = 0;
        }
        if (start > str.length()) {
            return STR_NULL;
        }
        return str.substring(start);
    }

    @ApiOperation(value = "截取字符串")
    public static String subString(String str, int start, int end) {
        if (isEmpty(str)) {
            return STR_NULL;
        }
        if (!(0 <= end)) {
            end = str.length() + end;
        }
        if (!(0 <= start)) {
            start = str.length() + start;
        }
        if (end > str.length()) {
            end = str.length();
        }
        if (start > end) {
            return STR_NULL;
        }
        if (!(0 <= start)) {
            start = 0;
        }
        if (!(0 <= end)) {
            end = 0;
        }
        return str.substring(start, end);
    }
}
