package com.stefanie.userserver.domain.pojo.dto.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 16:36
 * @description Dto-登录
 */
@Data
@ApiModel(value = "Dto-登录")
@Accessors(chain = true)
public class LoginDto implements Serializable {
    private static final long serialVersionUID = -3812444974482531097L;

    @NotBlank(message = "请输入用户登录名称")
    @ApiModelProperty(value = "登录名称", required = true)
    private String loginName;
    @NotBlank(message = "请输入用户密码")
    @ApiModelProperty(value = "密码", required = true)
    private String password;
    @ApiModelProperty(value = "验证码")
    private String code;
    @ApiModelProperty(value = "标识")
    private String uuid;
}
