package com.stefanie.userserver.domain.pojo.vo.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 15:13
 * @description
 */
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-登录日志")
public class LoginLogVo implements Serializable {
    private static final long serialVersionUID = 2968721927842501717L;

    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "登录名称")
    private String loginName;
    @ApiModelProperty(value = "状态：1-成功；2-失败")
    private Integer status;
    @ApiModelProperty(value = "登录地址")
    private String address;
    @ApiModelProperty(value = "ip")
    private String ip;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "登录时间")
    private Date time;
    @ApiModelProperty(value = "浏览器类型")
    private String browser;
    @ApiModelProperty(value = "操作系统")
    private String os;
    @ApiModelProperty(value = "登录类型：1-登录；2-退出")
    private Integer type;
}
