package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.constant.sys.GrepConstant;
import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 14:16
 * @description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-创建字典")
public class DictDataCreateDto extends CommonDto implements Serializable {
    private static final long serialVersionUID = 5881483350970279959L;

    @ApiModelProperty(value = "字典编码", required = true)
    @NotBlank(message = "请输入字典编码")
    private String dictCode;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "标签", required = true)
    @NotBlank(message = "请输入标签")
    private String label;
    @ApiModelProperty(value = "键值", required = true)
    @NotBlank(message = "请输入键值")
    private String value;
    @ApiModelProperty(value = "是否默认值", notes = "1-否；2-是", required = true)
    @NotNull(message = "请选择是否是默认值")
    private Integer isDefault;
    @ApiModelProperty(value = "类型：对应sys_dict_type中的'type'", required = true)
    @NotBlank(message = "请输入字典类型")
    @Pattern(regexp = GrepConstant.DICT_TYPE, message = "字典类型必须以字母开头，且只能为（小写字母，数字，下滑线）")
    private String type;
}
