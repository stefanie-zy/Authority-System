package com.stefanie.userserver.domain.pojo.po.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.stefanie.userserver.domain.core.CommonPo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-12 22:08
 * @description
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_file")
@ApiModel(value = "Po-文件")
public class FilePo extends CommonPo implements Serializable {

    private static final long serialVersionUID = 1971096178169163344L;

    @ApiModelProperty(value = "文件名称")
    private String name;
    @ApiModelProperty(value = "文件路径")
    private String path;
    @ApiModelProperty(value = "文件访问地址")
    private String url;
    @ApiModelProperty(value = "文件类型")
    private String type;
    @ApiModelProperty(value = "用途")
    private Integer userType;
    @ApiModelProperty(value = "文件大小")
    private Long size;
    @ApiModelProperty(value = "下载次数")
    private Long downloadCount;
    @ApiModelProperty(value = "MD5")
    private String md5;
}
