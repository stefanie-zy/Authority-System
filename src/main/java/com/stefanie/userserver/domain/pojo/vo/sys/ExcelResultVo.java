package com.stefanie.userserver.domain.pojo.vo.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-12 22:14
 * @description
 */

@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-Excel导入结果")
public class ExcelResultVo implements Serializable {

    private static final long serialVersionUID = 8029108233807166918L;

    @ApiModelProperty(value = "导入失败信息")
    private String errorMessage;
    @ApiModelProperty(value = "导入成功信息")
    private String successMessage;
    @ApiModelProperty(value = "导入失败个数")
    private Integer errorCount;
    @ApiModelProperty(value = "导入成功个数")
    private Integer successCount;
    @ApiModelProperty(value = "耗时-单位：ms")
    private String time;
    @ApiModelProperty(value = "备注说明")
    private String remark;
}
