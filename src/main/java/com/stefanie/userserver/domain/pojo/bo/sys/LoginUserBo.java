package com.stefanie.userserver.domain.pojo.bo.sys;

import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-24 21:34
 * @description Bo-登录缓存用户
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "Bo-登录缓存用户")
//public class LoginUserBo extends CommonPo implements UserDetails {
public class LoginUserBo implements UserDetails {

    private static final long serialVersionUID = 1407547847894279572L;

    @ApiModelProperty(value = "用户token")
    private String token;
    @ApiModelProperty(value = "过期时间")
    private Date expireTime;
    @ApiModelProperty(value = "登录ip地址")
    private String ip;
    @ApiModelProperty(value = "登录地址")
    private String loginAddr;
    @ApiModelProperty(value = "浏览器类型")
    private String browser;
    @ApiModelProperty(value = "操作系统")
    private String os;
    @ApiModelProperty(value = "授权列表")
    private Set<String> permissions;
    @ApiModelProperty(value = "用户信息")
    private UserPo userPo;

    // 自定义构造方法
    public LoginUserBo(UserPo userPo) {
        this.userPo = userPo;
    }

    public LoginUserBo(UserPo userPo, Set<String> permissions) {
        this.userPo = userPo;
        this.permissions = permissions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return userPo.getPassword();
    }

    @Override
    public String getUsername() {
        return userPo.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
