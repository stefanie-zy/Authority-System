package com.stefanie.userserver.domain.pojo.dto.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author: stefanie
 * @createTime: 2023/07/05 8:47
 * @description: Dto-角色菜单绑定
 */
@Data
@ApiModel(value = "Dto-角色菜单绑定")
public class RoleAndMenuDto implements Serializable {
    private static final long serialVersionUID = 2790506322639050276L;

    @ApiModelProperty(value = "角色Id",required = true)
    @NotNull(message = "请输入角色Id")
    private Long roleId;
    @ApiModelProperty(value = "菜单Id列表",required = true)
    @NotBlank(message = "请输入菜单Id")
    private List<Long> menuIds;
}
