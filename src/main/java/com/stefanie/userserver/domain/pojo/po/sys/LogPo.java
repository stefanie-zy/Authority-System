package com.stefanie.userserver.domain.pojo.po.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-30 17:14
 * @description Po-日志
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_log")
@ApiModel(value = "Po-日志")
public class LogPo implements Serializable {

    private static final long serialVersionUID = -291350840593971242L;

    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "模块标题")
    private String title;
    @ApiModelProperty(value = "业务类型：1-新增；2-修改;3-删除；4-其他")
    private Integer businessType;
    @ApiModelProperty(value = "方法名称")
    private String method;
    @ApiModelProperty(value = "请求方式")
    private String requestMethod;
    @ApiModelProperty(value = "操作者")
    private String loginName;
    @ApiModelProperty(value = "请求Url")
    private String url;
    @ApiModelProperty(value = "ip")
    private String ip;
    @ApiModelProperty(value = "请求参数")
    private String param;
    @ApiModelProperty(value = "返回参数")
    private String result;
    @ApiModelProperty(value = "状态：1-正常；2-异常")
    private Integer status;
    @ApiModelProperty(value = "异常信息")
    private String errorMsg;
    @ApiModelProperty(value = "操作时间")
    private Date time;
    @ApiModelProperty(value = "耗时")
    private String costTime;
    @ApiModelProperty(value = "日志类型：1-操作日志；2-登录日志")
    private Integer type;
}
