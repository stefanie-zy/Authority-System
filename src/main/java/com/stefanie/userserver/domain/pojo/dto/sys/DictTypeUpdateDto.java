package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.constant.sys.GrepConstant;
import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 10:02
 * @description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-修改字典类型")
public class DictTypeUpdateDto extends CommonDto implements Serializable {
    private static final long serialVersionUID = -1376695333029260292L;

    @NotNull(message = "请输入字典类型Id")
    @ApiModelProperty(value = "字典类型Id", notes = "不支持修改", required = true)
    private Long id;
    @ApiModelProperty(value = "名称", required = true)
    @NotBlank(message = "请输入字典类型名称")
    private String name;
    @ApiModelProperty(value = "类型", required = true)
    @NotBlank(message = "请输入字典类型")
    @Pattern(regexp = GrepConstant.DICT_TYPE, message = "字典类型必须以字母开头，且只能为（小写字母，数字，下滑线）")
    private String type;
}
