package com.stefanie.userserver.domain.pojo.vo.sys;

import com.stefanie.userserver.domain.core.CommonVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-05 22:17
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-字典类型")
public class DictTypeVo extends CommonVo implements Serializable {
    private static final long serialVersionUID = 372231263761066679L;

    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "类型")
    private String type;
    @ApiModelProperty(value = "状态：1-正常；2-停用")
    private Integer status;
}
