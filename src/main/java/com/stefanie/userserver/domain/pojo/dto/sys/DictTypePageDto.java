package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonPageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 11:16
 * @description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-字典类型分页查询")
public class DictTypePageDto extends CommonPageDto implements Serializable {

    private static final long serialVersionUID = 4233464343978163542L;

    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "类型")
    private String type;
    @ApiModelProperty(value = "状态：1-正常；2-停用")
    private Integer status;
}
