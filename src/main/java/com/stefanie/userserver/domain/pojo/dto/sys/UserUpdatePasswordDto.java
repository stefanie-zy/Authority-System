package com.stefanie.userserver.domain.pojo.dto.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 10:02
 * @description Dto-修改密码
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-修改密码")
public class UserUpdatePasswordDto implements Serializable {
    private static final long serialVersionUID = -1388695730614469223L;

    @NotBlank(message = "请输入旧密码")
    @ApiModelProperty(value = "旧密码", required = true)
    private String oldPassword;
    @NotBlank(message = "请输入新密码")
    @ApiModelProperty(value = "新密码", required = true)
    private String newPassword;
    @NotBlank(message = "请再次输入新密码")
    @ApiModelProperty(value = "确认新密码", required = true)
    private String confirmPassword;
    @ApiModelProperty(value = "用户Id", required = true)
    private Long id;
}
