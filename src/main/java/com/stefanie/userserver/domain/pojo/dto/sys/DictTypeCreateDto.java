package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.constant.sys.GrepConstant;
import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-05 22:15
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-创建字典类型")
public class DictTypeCreateDto extends CommonDto implements Serializable {

    private static final long serialVersionUID = -7110506170230968540L;

    @ApiModelProperty(value = "名称", required = true)
    @NotBlank(message = "请输入字典类型名称")
    private String name;
    @ApiModelProperty(value = "类型", required = true)
    @NotBlank(message = "请输入字典类型")
    @Pattern(regexp = GrepConstant.DICT_TYPE, message = "字典类型必须以字母开头，且只能为（小写字母，数字，下滑线）")
    private String type;
}
