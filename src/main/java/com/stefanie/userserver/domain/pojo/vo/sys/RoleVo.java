package com.stefanie.userserver.domain.pojo.vo.sys;

import com.stefanie.userserver.domain.core.CommonVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 16:54
 * @description
 */
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Vo-角色")
public class RoleVo extends CommonVo implements Serializable {
    private static final long serialVersionUID = 7016532902798794180L;

    @ApiModelProperty(value = "角色名称")
    private String name;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "角色权限字符串")
    private String roleKey;
    @ApiModelProperty(value = "状态：1-正常；2-停用")
    private Integer status;
}
