package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 17:07
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-创建角色")
public class RoleCreateDto extends CommonDto implements Serializable {

    private static final long serialVersionUID = -682995233393176815L;

    @NotBlank(message = "请输入角色名称")
    @ApiModelProperty(value = "角色名称", required = true)
    private String name;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @NotBlank(message = "请输入角色权限")
    @ApiModelProperty(value = "角色权限字符串")
    private String roleKey;
}
