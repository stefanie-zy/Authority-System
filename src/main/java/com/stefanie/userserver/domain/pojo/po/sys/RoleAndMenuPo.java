package com.stefanie.userserver.domain.pojo.po.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-02 22:21
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role_menu")
@ApiModel(value = "Po-角色/菜单")
public class RoleAndMenuPo implements Serializable {
    private static final long serialVersionUID = -5894826929079820587L;

    @ApiModelProperty(value = "菜单ID")
    private Long menuId;
    @ApiModelProperty(value = "角色ID")
    private Long roleId;
}
