package com.stefanie.userserver.domain.pojo.vo.sys;

import com.stefanie.userserver.domain.core.CommonVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 15:32
 * @description:
 */
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-字典")
public class DictDataVo extends CommonVo implements Serializable {

    private static final long serialVersionUID = 5941553769810900705L;
    @ApiModelProperty(value = "字典编码")
    private String dictCode;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "标签")
    private String label;
    @ApiModelProperty(value = "键值")
    private String value;
    @ApiModelProperty(value = "类型：对应sys_dict_type中的'type'")
    private String type;
    @ApiModelProperty(value = "是否默认：1-否；2-是")
    private Integer isDefault;
    @ApiModelProperty(value = "状态：1-正常；2-停用")
    private Integer status;
}
