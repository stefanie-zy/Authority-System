package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonPageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 14:25
 * @description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-字典分页查询")
public class DictDataPageDto extends CommonPageDto implements Serializable {
    private static final long serialVersionUID = -4031159750088718836L;

    @ApiModelProperty(value = "字典编码")
    private String dictCode;
    @ApiModelProperty(value = "标签")
    private String label;
    @ApiModelProperty(value = "键值")
    private String value;
    @ApiModelProperty(value = "类型：对应sys_dict_type中的'type'")
    private String type;
}
