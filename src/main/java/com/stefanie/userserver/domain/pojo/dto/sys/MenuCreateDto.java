package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 16:42
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-创建菜单")
public class MenuCreateDto extends CommonDto implements Serializable {
    private static final long serialVersionUID = -305186595420588892L;

    @NotBlank(message = "请输入菜单名称")
    @ApiModelProperty(value = "菜单名称", required = true)
    private String name;
    @NotNull(message = "请输入父级菜单ID")
    @Min(value = 0L, message = "请输入正确的父级ID")
    @ApiModelProperty(value = "父级Id", notes = "根菜单父级为0",required = true)
    private Long parentId;
    @ApiModelProperty(value = "排序",required = true)
    private Integer sort;
    @ApiModelProperty(value = "路由地址")
    private String path;
    @ApiModelProperty(value = "组件路径")
    private String component;
    @ApiModelProperty(value = "路由参数")
    private String query;
    @NotNull(message = "请输入是否为外链")
    @ApiModelProperty(value = "是否为外链：1-否；2-是",required = true)
    private Integer isFrame;
    @ApiModelProperty(value = "是否缓存：1-缓存；2-不缓存", hidden = true)
    private Integer isCache;
    @NotNull(message = "请输入类型")
    @ApiModelProperty(value = "类型：1-目录；2-菜单；3-按钮",required = true)
    private Integer type;
    @ApiModelProperty(value = "权限标识")
    private String perms;
    @ApiModelProperty(value = "菜单图标")
    private String icon;
}
