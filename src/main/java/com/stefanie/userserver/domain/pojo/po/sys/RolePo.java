package com.stefanie.userserver.domain.pojo.po.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.stefanie.userserver.domain.core.CommonPo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-30 16:52
 * @description Po-角色
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_role")
@ApiModel(value = "Po-角色")
public class RolePo extends CommonPo implements Serializable {

    private static final long serialVersionUID = -2776082041463339395L;

    @ApiModelProperty(value = "角色名称")
    private String name;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "角色权限字符串", notes = "系统默认不支持使用key，因此改为roleKey")
    private String roleKey;
    @ApiModelProperty(value = "状态：1-正常；2-停用")
    private Integer status;
    @ApiModelProperty(value = "删除标记：1-未删除；2-删除")
    private Integer delFlag;
}
