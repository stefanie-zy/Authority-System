package com.stefanie.userserver.domain.pojo.dto.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author: stefanie
 * @createTime: 2023/07/04 15:55
 * @description: Dto-用户角色绑定
 */
@Data
@ApiModel(value = "Dto-用户角色绑定")
public class UserAndRoleDto implements Serializable {
    private static final long serialVersionUID = -367574145673250597L;

    @ApiModelProperty(value = "用户Id",required = true)
    @NotNull(message = "请输入用户Id")
    private Long userId;
    @ApiModelProperty(value = "角色Id列表",required = true)
    @NotBlank(message = "请输入角色Id")
    private List<Long> roleIds;
}
