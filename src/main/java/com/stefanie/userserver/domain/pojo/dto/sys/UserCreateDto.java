package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.annotation.Xss;
import com.stefanie.userserver.domain.core.CommonDto;
import com.stefanie.userserver.constant.sys.GrepConstant;
import com.stefanie.userserver.utils.other.EncryptionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:48
 * @description Dto-创建用户
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-创建用户")
public class UserCreateDto extends CommonDto implements Serializable {

    private static final long serialVersionUID = -6626065662373290733L;

    @NotBlank(message = "请输入用户名称")
    @Pattern(regexp = GrepConstant.CHINESE, message = "用户名称必须是中文")
    @ApiModelProperty(value = "用户名称", required = true)
    private String userName;

    @Xss(message = "用户昵称不允许有脚本符号")
    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @Xss(message = "登录名称不允许有脚本符号")
    @NotBlank(message = "请输入登录名称")
    @ApiModelProperty(value = "登录名称", required = true)
    private String loginName;

    @NotNull(message = "请输入用户类型")
    @Min(value = 1, message = "用户类型值错误")
    @Max(value = 2, message = "用户类型值错误")
    @ApiModelProperty(value = "用户类型：1-系统用户；2-业务用户", required = true, example = "1")
    private Integer userType;

    @Pattern(regexp = GrepConstant.PHONE_NUMBER, message = "请输入正确的手机号格式")
    @ApiModelProperty(value = "手机号", allowableValues = "11")
    private String phoneNumber;

    @Email
    @ApiModelProperty(value = "邮箱")
    private String email;

    @Min(value = 1, message = "性别值错误")
    @Max(value = 2, message = "性别值错误")
    @ApiModelProperty(value = "性别：1-男；2-女；3-未知", example = "1", dataType = "int")
    private Integer sex;

    @Pattern(regexp = GrepConstant.ID_CARD, message = "请输入正确的身份证格式")
    @ApiModelProperty(value = "身份证号")
    private String idCard;

    @ApiModelProperty(value = "住址")
    private String address;

    @ApiModelProperty(value = "头像地址")
    private String icoUrl;

    public String getPhoneNumber() {
        // 加密处理
        return EncryptionUtil.encrypt(this.phoneNumber);
    }

    public String getIdCard() {
        // 加密处理
        return EncryptionUtil.encrypt(this.idCard);
    }

    public String getAddress() {
        // 加密处理
        return EncryptionUtil.encrypt(this.address);
    }

    public String getEmail() {
        // 加密处理
        return EncryptionUtil.encrypt(this.email);
    }
}
