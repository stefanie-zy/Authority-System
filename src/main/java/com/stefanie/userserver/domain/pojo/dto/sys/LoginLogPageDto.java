package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonPageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 15:21
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-登录日志分页查询")
public class LoginLogPageDto extends CommonPageDto implements Serializable {

    private static final long serialVersionUID = -63456962303421261L;

    @ApiModelProperty(value = "操作者")
    private String loginName;
    @ApiModelProperty(value = "ip")
    private String ip;
    @ApiModelProperty(value = "起始时间")
    private Date startTime;
    @ApiModelProperty(value = "截止时间")
    private Date endTime;
    @ApiModelProperty(value = "登录类型：1-登录；2-退出")
    private Integer type;
}
