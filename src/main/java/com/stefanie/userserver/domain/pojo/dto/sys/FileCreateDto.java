package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-12 22:12
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-创建文件")
public class FileCreateDto extends CommonDto implements Serializable {

    private static final long serialVersionUID = -8328992919957840637L;
    @ApiModelProperty(value = "文件名称", notes = "为空则使用文件本身的名称")
    private String name;
    @ApiModelProperty(value = "文件路径", notes = "不需要拼接“/“。为空则使用默认路径")
    private String path;
    @ApiModelProperty(value = "用途", notes = "为空则使用使用默认用途")
    private Integer userType;
}
