package com.stefanie.userserver.domain.pojo.vo.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-20 19:32
 * @description
 */
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-系统日志")
public class LogVo implements Serializable {

    private static final long serialVersionUID = 3431181504625244176L;

    @ApiModelProperty(value = "主键")
    private Long id;
    @ApiModelProperty(value = "模块标题")
    private String title;
    @ApiModelProperty(value = "业务类型：1-新增；2-修改;3-删除；4-其他")
    private Integer businessType;
    @ApiModelProperty(value = "方法名称")
    private String method;
    @ApiModelProperty(value = "请求方式")
    private String requestMethod;
    @ApiModelProperty(value = "操作者")
    private String loginName;
    @ApiModelProperty(value = "请求Url")
    private String url;
    @ApiModelProperty(value = "ip")
    private String ip;
    @ApiModelProperty(value = "请求参数")
    private String param;
    @ApiModelProperty(value = "返回参数")
    private String result;
    @ApiModelProperty(value = "状态：1-正常；2-异常")
    private Integer status;
    @ApiModelProperty(value = "异常信息")
    private String errorMsg;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "操作时间")
    private Date time;
    @ApiModelProperty(value = "耗时")
    private String costTime;
    @ApiModelProperty(value = "日志类型：1-操作日志；2-登录日志")
    private Integer type;
}
