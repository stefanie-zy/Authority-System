package com.stefanie.userserver.domain.pojo.vo.sys;

import com.stefanie.userserver.domain.core.CommonVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-12 22:14
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-系统文件")
public class FileVo extends CommonVo implements Serializable {
    private static final long serialVersionUID = -5014404162666693920L;
    @ApiModelProperty(value = "文件名称")
    private String name;
    @ApiModelProperty(value = "文件路径")
    private String path;
    @ApiModelProperty(value = "文件访问地址")
    private String url;
    @ApiModelProperty(value = "文件类型")
    private String type;
    @ApiModelProperty(value = "用途")
    private Integer userType;
    @ApiModelProperty(value = "文件大小")
    private Long size;
    @ApiModelProperty(value = "下载次数")
    private Integer downloadCount;
    @ApiModelProperty(value = "MD5")
    private String md5;
}
