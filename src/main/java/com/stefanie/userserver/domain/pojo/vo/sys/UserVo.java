package com.stefanie.userserver.domain.pojo.vo.sys;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.stefanie.userserver.domain.core.CommonVo;
import com.stefanie.userserver.utils.grep.GrepUtil;
import com.stefanie.userserver.utils.other.EncryptionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:43
 * @description Vo-用户
 */
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-用户")
public class UserVo extends CommonVo implements Serializable {
    private static final long serialVersionUID = -8310379856596549441L;

    @ApiModelProperty(value = "用户名称")
    private String userName;
    @ApiModelProperty(value = "用户昵称")
    private String nickName;
    @ApiModelProperty(value = "登录名称")
    private String loginName;
    @ApiModelProperty(value = "用户类型：1-系统用户；2-业务用户")
    private Integer userType;
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;
    @ApiModelProperty(value = "邮箱")
    private String email;
    @ApiModelProperty(value = "性别：1-男；2-女；3-未知")
    private Integer sex;
    @ApiModelProperty(value = "身份证号")
    private String idCard;
    @ApiModelProperty(value = "住址")
    private String address;
    @ApiModelProperty(value = "头像地址")
    private String icoUrl;
    @ApiModelProperty(value = "状态：1-正常；2-锁定；3-注销")
    private Integer status;
    @ApiModelProperty(value = "删除标记：1-未删除；2-删除")
    private Integer delFlag;
    @ApiModelProperty(value = "登录状态：1-未登录；2-已登录")
    private Integer loginStatus;
    @ApiModelProperty(value = "最后一次登录ip")
    private String loginIp;
    @ApiModelProperty(value = "最后一次登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date loginTime;
    @ApiModelProperty(value = "密码错误次数：>=6 用户锁定")
    private Integer wrPasswordNumber;
    @ApiModelProperty(value = "用户token", hidden = true)
    private String token;
    // 脱敏数据
    @ApiModelProperty(value = "脱敏：手机号")
    private String phoneNumberVague;
    @ApiModelProperty(value = "脱敏：身份证")
    private String idCardVague;
    @ApiModelProperty(value = "脱敏：地址")
    private String addressVague;
    @ApiModelProperty(value = "脱敏：邮箱")
    private String emailVague;

    public String getPhoneNumber() {
        // 解密处理
        return EncryptionUtil.decode(this.phoneNumber);
    }

    public String getIdCard() {
        // 解密处理
        return EncryptionUtil.decode(this.idCard);
    }

    public String getAddress() {
        // 解密处理
        return EncryptionUtil.decode(this.address);
    }

    public String getEmail() {
        // 解密处理
        return EncryptionUtil.decode(this.email);
    }

    public String getPhoneNumberVague() {
        // 脱敏处理
        return GrepUtil.phoneNumberHide(getPhoneNumber());
    }

    public String getIdCardVague() {
        // 脱敏处理
        return GrepUtil.idCardHide(getIdCard());
    }

    public String getAddressVague() {
        // 脱敏处理
        return GrepUtil.addressHide(getAddress());
    }

    public String getEmailVague() {
        // 脱敏处理
        return GrepUtil.emailHide(getEmail());
    }
}
