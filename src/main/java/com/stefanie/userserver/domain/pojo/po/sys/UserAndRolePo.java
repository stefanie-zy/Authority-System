package com.stefanie.userserver.domain.pojo.po.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-02 22:23
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_user_role")
@ApiModel(value = "Po-用户/角色")
public class UserAndRolePo implements Serializable {
    private static final long serialVersionUID = -4218836766189494335L;

    @ApiModelProperty(value = "用户ID")
    private Long userId;
    @ApiModelProperty(value = "角色ID")
    private Long roleId;
}
