package com.stefanie.userserver.domain.pojo.vo.sys;

import com.stefanie.userserver.domain.core.CommonVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 16:39
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(value = "Vo-菜单")
public class MenuVo extends CommonVo implements Serializable {
    private static final long serialVersionUID = 5238610956327304902L;

    @ApiModelProperty(value = "菜单名称")
    private String name;
    @ApiModelProperty(value = "父级Id")
    private Long parentId;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "路由地址")
    private String path;
    @ApiModelProperty(value = "组件路径")
    private String component;
    @ApiModelProperty(value = "路由参数")
    private String query;
    @ApiModelProperty(value = "是否为外链：1-否；2-是")
    private Integer isFrame;
    @ApiModelProperty(value = "是否缓存：1-缓存；2-不缓存")
    private Integer isCache;
    @ApiModelProperty(value = "类型：1-目录；2-菜单；3-按钮")
    private Integer type;
    @ApiModelProperty(value = "菜单状态（是否显示）：1-显示；2-隐藏")
    private Integer visible;
    @ApiModelProperty(value = "状态：1-正常；2-停用")
    private Integer status;
    @ApiModelProperty(value = "权限标识")
    private String perms;
    @ApiModelProperty(value = "菜单图标")
    private String icon;
}
