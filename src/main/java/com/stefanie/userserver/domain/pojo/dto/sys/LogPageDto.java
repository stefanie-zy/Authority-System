package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonPageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-20 19:54
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-系统日志分页查询")
public class LogPageDto extends CommonPageDto implements Serializable {

    private static final long serialVersionUID = -8579242138302003237L;

    @ApiModelProperty(value = "模块标题")
    private String title;
    @ApiModelProperty(value = "业务类型：1-新增；2-修改;3-删除；4-其他")
    private Integer businessType;
    @ApiModelProperty(value = "操作者")
    private String loginName;
    @ApiModelProperty(value = "ip")
    private String ip;
    @ApiModelProperty(value = "状态：1-正常；2-异常")
    private Integer status;
    @ApiModelProperty(value = "起始时间")
    private Date startTime;
    @ApiModelProperty(value = "截止时间")
    private Date endTime;
    @ApiModelProperty(value = "关键字", notes = "请求当中的关键字，暂时未生效", hidden = true)
    private String keyWord;
}
