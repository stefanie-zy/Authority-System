package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/07/06 14:19
 * @description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-修改字典")
public class DictDataUpdateDto extends CommonDto implements Serializable {
    private static final long serialVersionUID = 3721499576778552295L;

    @NotNull(message = "请输入字典Id")
    @ApiModelProperty(value = "字典Id", notes = "不支持修改", required = true)
    private Long id;
    @ApiModelProperty(value = "字典编码", required = true)
    @NotBlank(message = "请输入字典编码")
    private String dictCode;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "标签", required = true)
    @NotBlank(message = "请输入标签")
    private String label;
    @ApiModelProperty(value = "是否默认值", notes = "1-否；2-是", required = true)
    @NotNull(message = "请选择是否是默认值")
    private Integer isDefault;
    @ApiModelProperty(value = "键值", required = true)
    @NotBlank(message = "请输入键值")
    private String value;
}
