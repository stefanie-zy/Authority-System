package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-02 20:41
 * @description
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-修改角色")
public class RoleUpdateDto extends CommonDto implements Serializable {

    private static final long serialVersionUID = -2927034391064489266L;

    @NotNull(message = "请输入用户Id")
    @ApiModelProperty(value = "角色Id",notes = "不支持修改",required = true)
    private Long id;
    @NotBlank(message = "请输入角色名称")
    @ApiModelProperty(value = "角色名称", required = true)
    private String name;
    @NotBlank(message = "请输入角色权限")
    @ApiModelProperty(value = "角色权限字符串",required = true)
    private String roleKey;
}
