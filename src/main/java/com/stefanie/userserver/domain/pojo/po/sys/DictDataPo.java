package com.stefanie.userserver.domain.pojo.po.sys;

import com.baomidou.mybatisplus.annotation.TableName;
import com.stefanie.userserver.domain.core.CommonPo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-30 17:15
 * @description Po-字典
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_dict_data")
@ApiModel(value = "Po-字典")
public class DictDataPo extends CommonPo implements Serializable {

    private static final long serialVersionUID = -6794834476054362312L;

    @ApiModelProperty(value = "字典编码")
    private String dictCode;
    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "标签")
    private String label;
    @ApiModelProperty(value = "键值")
    private String value;
    @ApiModelProperty(value = "类型：对应sys_dict_type中的'type'")
    private String type;
    @ApiModelProperty(value = "是否默认：1-否；2-是")
    private Integer isDefault;
    @ApiModelProperty(value = "状态：1-正常；2-停用")
    private Integer status;
}
