package com.stefanie.userserver.domain.pojo.po.sys;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-17 20:06
 * @description
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_login_log")
@ApiModel(value = "Po-登录日志")
public class LoginLogPo implements Serializable {

    private static final long serialVersionUID = -1479288973278171602L;

    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "登录名称")
    private String loginName;
    @ApiModelProperty(value = "状态：1-成功；2-失败")
    private Integer status;
    @ApiModelProperty(value = "登录地址")
    private String address;
    @ApiModelProperty(value = "ip")
    private String ip;
    @ApiModelProperty(value = "登录时间")
    private Date time;
    @ApiModelProperty(value = "浏览器类型")
    private String browser;
    @ApiModelProperty(value = "操作系统")
    private String os;
    @ApiModelProperty(value = "登录类型：1-登录；2-退出")
    private Integer type;
}
