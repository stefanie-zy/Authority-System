package com.stefanie.userserver.domain.pojo.dto.sys;

import com.stefanie.userserver.domain.core.CommonPageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-24 20:40
 * @description Dto-用户分页查询
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "Dto-用户分页查询")
public class UserPageDto extends CommonPageDto implements Serializable {

    private static final long serialVersionUID = -2978729062140704660L;

    @ApiModelProperty(value = "用户名称")
    private String userName;

    @ApiModelProperty(value = "登录状态：1-未登录；2-已登录")
    private Integer loginStatus;
}
