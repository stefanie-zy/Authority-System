package com.stefanie.userserver.domain.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-24 20:41
 * @description Dto-公共分页请求
 */
@Data
@ApiModel(value = "Dto-公共分页请求")
@Accessors(chain = true)
public class CommonPageDto implements Serializable {

    private static final long serialVersionUID = 3428958266383343305L;

    @NotNull(message = "请输入页数")
    @ApiModelProperty(value = "页数", required = true)
    private Integer pageNum;

    @NotNull(message = "请输入分页大小")
    @ApiModelProperty(value = "分页大小", required = true)
    private Integer pageSize;

    public Integer getPageSize() {
        return (this.pageSize <= 10) ? 10 : this.pageSize;
    }
}
