package com.stefanie.userserver.domain.core;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-17 22:59
 * @description Po-公共
 */
@Data
@ApiModel(value = "Po-公共")
@Accessors(chain = true)
public class CommonPo implements Serializable {

    private static final long serialVersionUID = 3445085738548991339L;

    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;
    @ApiModelProperty(value = "创建者")
    private String createName;
    @OrderBy(sort = Short.MAX_VALUE)
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @ApiModelProperty(value = "更新者")
    private String updateName;
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}
