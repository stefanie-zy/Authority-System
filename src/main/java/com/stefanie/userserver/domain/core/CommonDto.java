package com.stefanie.userserver.domain.core;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-05-24 22:11
 * @description Dto-公共
 */
@Data
@ApiModel(value = "Dto-公共")
@Accessors(chain = true)
public class CommonDto implements Serializable {

    private static final long serialVersionUID = 223729668171244230L;

    @ApiModelProperty(value = "创建者")
    private String createName;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @ApiModelProperty(value = "更新者")
    private String updateName;
    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
