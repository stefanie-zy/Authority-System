package com.stefanie.userserver.domain.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:45
 * @description Vo-公共
 */
@Data
@ApiModel(value = "Vo-公共")
@Accessors(chain = true)
public class CommonVo implements Serializable {

    private static final long serialVersionUID = 3116670775785197896L;

    @ApiModelProperty(value = "主键")
    private Long id;
    @ApiModelProperty(value = "创建者")
    private String createName;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    @ApiModelProperty(value = "更新者")
    private String updateName;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    @ApiModelProperty(value = "备注")
    private String remark;
}
