package com.stefanie.userserver;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author zy
 */
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@Slf4j
@EnableCaching
@SpringBootApplication
public class UserserverApplication {

    public static void main(String[] args) {
        log.error("error log");
        log.warn("warn log");
        log.info("info log");
        log.debug("debug log");
        log.trace("trace log");
        //设置热部署
//        System.setProperty("spring.devtools.restart.enabled", "true");
        SpringApplication.run(UserserverApplication.class, args);
    }

}
