package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.common.enums.core.CommonEnum;
import com.stefanie.userserver.common.enums.error.OtherErrorEnum;
import com.stefanie.userserver.common.enums.sys.StatusEnum;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataPageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataUpdateDto;
import com.stefanie.userserver.domain.pojo.po.sys.DictDataPo;
import com.stefanie.userserver.domain.pojo.vo.sys.DictDataVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.manager.PageManager;
import com.stefanie.userserver.mapper.sys.DictDataMapper;
import com.stefanie.userserver.service.sys.DictDataService;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.DateUtil;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-05 22:10
 * @description
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DictDataServiceImpl extends ServiceImpl<DictDataMapper, DictDataPo> implements DictDataService {

    private final DictDataMapper dictDataMapper;
    private final BeanMapStructMapper beanMapStructMapper;

    @ApiOperation(value = "新增字典")
    @Override
    public Boolean create(DictDataCreateDto dictDataCreateDto) {
        // 判断是否唯一
        String uniqueCheck = check(dictDataCreateDto.getType(), dictDataCreateDto.getDictCode(), dictDataCreateDto.getValue(), 0L);
        SysException.check(CommonConstant.UNIQUE.equals(uniqueCheck), OtherErrorEnum.B_DICT_EXIT);

        DictDataPo po = beanMapStructMapper.createDtoToPo(dictDataCreateDto);
        po.setStatus(StatusEnum.USABLE.getCode());
        return save(po);
    }

    @ApiOperation(value = "修改字典")
    @Override
    public Boolean update(DictDataUpdateDto dictDataUpdateDto) {
        DictDataPo po = dictDataMapper.selectById(dictDataUpdateDto.getId());
        // 判断是否唯一
        String uniqueCheck = check(po.getType(), dictDataUpdateDto.getDictCode(), dictDataUpdateDto.getValue(), dictDataUpdateDto.getId());
        SysException.check(CommonConstant.UNIQUE.equals(uniqueCheck), OtherErrorEnum.B_DICT_EXIT);

        po.setSort(dictDataUpdateDto.getSort());
        po.setDictCode(dictDataUpdateDto.getDictCode());
        po.setLabel(dictDataUpdateDto.getLabel());
        po.setValue(dictDataUpdateDto.getValue());
        po.setIsDefault(dictDataUpdateDto.getIsDefault());

        return updateById(po);
    }

    @ApiOperation(value = "删除字典")
    @Override
    public void delete(Long id) {
        dictDataMapper.deleteById(id);
    }

    @ApiOperation(value = "分页查询字典")
    @Override
    public IPage<DictDataVo> page(DictDataPageDto dictDataPageDto) {
        IPage<DictDataVo> pageVo = new Page<>();
        IPage<DictDataPo> pageParam = new Page<>(dictDataPageDto.getPageNum(), dictDataPageDto.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        // 字典编码
        if (StringUtil.isNotEmpty(dictDataPageDto.getDictCode())) {
            queryWrapper.like(ColumnConstant.DICT_CODE, dictDataPageDto.getDictCode());
        }
        // 类型
        if ((StringUtil.isNotEmpty(dictDataPageDto.getType()))) {
            queryWrapper.like(ColumnConstant.DICT_TYPE, dictDataPageDto.getType());
        }
        // 标签
        if ((StringUtil.isNotEmpty(dictDataPageDto.getLabel()))) {
            queryWrapper.like(ColumnConstant.DICT_LABEL, dictDataPageDto.getLabel());
        }
        // 键值
        if ((StringUtil.isNotEmpty(dictDataPageDto.getValue()))) {
            queryWrapper.eq(ColumnConstant.DICT_VALUE, dictDataPageDto.getValue());
        }
        IPage pagePo = super.page(pageParam, queryWrapper);
        // Po转Vo
        List<DictDataVo> voList = (List<DictDataVo>) pagePo.getRecords().stream().map(po -> beanMapStructMapper.createPoToVo((DictDataPo) po)).collect(Collectors.toList());
        return PageManager.poToVo(pagePo, pageVo, voList);
    }

    @ApiOperation(value = "字典唯一性校验", notes = "新增：id=0L；修改：id=被修改字典id。字典类型+字典值+字典标签多重判断唯一性")
    @Override
    public String check(String type, String label, String value, Long id) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ColumnConstant.DICT_TYPE, type);
        queryWrapper.eq(ColumnConstant.DICT_LABEL, label);
        queryWrapper.eq(ColumnConstant.DICT_VALUE, value);
        DictDataPo po = dictDataMapper.selectOne(queryWrapper);
        // 逻辑说明：
        // -如果字典不存在则肯定唯一
        // -如果字典存在且 id=0L 说明是新增字典，则字典不是唯一
        // -如果字典存在且 id!=0L 说明是修改字典，且 id!=获取到的字典Id，则说明字典不是唯一
        if (po != null && (id == 0L || id.longValue() != po.getId())) {
            return CommonConstant.NOT_UNIQUE;
        }
        return CommonConstant.UNIQUE;
    }

    @ApiOperation(value = "修改字典状态")
    @Override
    public void updateStatus(Long id, Integer status, String userName) {
        DictDataPo po = dictDataMapper.selectById(id);
        SysException.check(StringUtil.isNotNull(po), OtherErrorEnum.B_DICT_NULL);
        SysException.check(CommonEnum.check(StatusEnum.class, status), OtherErrorEnum.S_DICT_STATUS_ERROR);
        po.setStatus(status);
        po.setRemark(po.getRemark() + "\n" + DateUtil.getDfDateTime() + ":被用户'" + userName + "'" + CommonEnum.getMessageByCode(StatusEnum.class, status) + "！");
        updateById(po);
    }

    @ApiOperation(value = "根据字典类型获取字典列表")
    @Override
    public List<DictDataVo> listByType(String type) {
        List<DictDataVo> voList = new ArrayList<>();
        List<DictDataPo> poList = dictDataMapper.listByType(type);
        if (0 == poList.size()) {
            return null;
        }
        poList.forEach(po -> voList.add(beanMapStructMapper.createPoToVo(po)));
        return voList;
    }

}
