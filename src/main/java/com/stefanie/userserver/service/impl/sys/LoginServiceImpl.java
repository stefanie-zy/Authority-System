package com.stefanie.userserver.service.impl.sys;

import com.stefanie.userserver.common.enums.error.UserErrorEnum;
import com.stefanie.userserver.common.enums.sys.LoginStatusEnum;
import com.stefanie.userserver.common.enums.sys.OperationEnum;
import com.stefanie.userserver.common.enums.sys.UserStatusEnum;
import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.domain.pojo.dto.sys.LoginDto;
import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.manager.TokenManager;
import com.stefanie.userserver.mapper.sys.UserMapper;
import com.stefanie.userserver.service.sys.LoginLogService;
import com.stefanie.userserver.service.sys.LoginService;
import com.stefanie.userserver.utils.ip.IpUtil;
import com.stefanie.userserver.utils.other.ServletUtil;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 16:45
 * @description
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final UserMapper userMapper;
    private final TokenManager tokenManager;
    private final StefanieConfig stefanieConfig;
    private final AuthenticationManager authenticationManager;
    private final LoginLogService loginLogService;

    @ApiOperation(value = "用户登录")
    @Override
    public String login(LoginDto loginDto) {
        // 操作状态
        Integer status = OperationEnum.SUCCESS.getCode();
        // 验证码
        if (stefanieConfig.getVerificationCode()) {
            // TODO 验证码验证
        }
        // 用户验证
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getLoginName(), loginDto.getPassword()));
        } catch (Exception e) {
            // 操作状态
            status = OperationEnum.ERROR.getCode();
            if (e instanceof BadCredentialsException) {
                log.error("用户登录：{}", UserErrorEnum.P_USER_PASSWORD_ERROR.getMessage());
                // 密码错误次数添加
                passwordWrNumCheck(loginDto.getLoginName());
                throw new UserException(UserErrorEnum.P_USER_PASSWORD_ERROR);
            } else {
                log.error("用户登录：{}", e.getMessage());
                throw new UserException(UserErrorEnum.S_USER_LOGIN_ERROR.getCode(), e.getMessage());
            }
        } finally {
            // 添加登录日志
            try {
                loginLogService.create(loginDto.getLoginName(), status, LoginStatusEnum.IN.getCode());
            } catch (Exception e) {
                log.error("用户登录：保存登录信息失败！{}", e.getMessage());
            }
        }
        LoginUserBo loginUserBo = (LoginUserBo) authentication.getPrincipal();
        // 保存登录信息
        saveLoginInfo(loginUserBo.getUserPo().getId());
        // 生成token
        return tokenManager.createToken(loginUserBo);
    }

    @ApiOperation(value = "退出登录")
    @Override
    public void logout(LoginUserBo loginUserBo) {
        Integer status = OperationEnum.SUCCESS.getCode();
        try {
            String token = loginUserBo.getToken();
            if (!StringUtil.isEmpty(token)) {
                // 删除Redis缓存信息
                tokenManager.deleteRedisToken(token);
                // 更新用户信息
                UserPo userPo = userMapper.selectById(loginUserBo.getUserPo().getId());
                userPo.setLoginStatus(LoginStatusEnum.OUT.getCode());
                userPo.setLoginIp(null);
                userPo.setLoginTime(null);
                userPo.setToken(null);
                userMapper.updateById(userPo);
            }
        } catch (Exception e) {
            status = OperationEnum.ERROR.getCode();
        } finally {
            try {
                loginLogService.create(loginUserBo.getUsername(), status, LoginStatusEnum.OUT.getCode());
            } catch (Exception e) {
                log.error("用户退出：保存登录信息失败！{}", e.getMessage());
            }
        }
    }

    @ApiOperation(value = "保存登录信息")
    private void saveLoginInfo(Long id) {
        UserPo userPo = userMapper.selectById(id);
        if (StringUtil.isNull(userPo)) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        userPo.setLoginStatus(LoginStatusEnum.IN.getCode());
        userPo.setLoginTime(new Date());
        userPo.setLoginIp(IpUtil.getIpAddr(ServletUtil.getRequest()));
        userMapper.updateById(userPo);
    }

    @ApiOperation(value = "密码错误次数校验")
    private void passwordWrNumCheck(String loginName) {
        UserPo userPo = userMapper.selectUserPoByLoginName(loginName);
        Integer wrPasswordNumber = userPo.getWrPasswordNumber();
        userPo.setWrPasswordNumber(!wrPasswordNumber.equals(stefanieConfig.getPasswordWrNum()) ? userPo.getWrPasswordNumber() + 1 : wrPasswordNumber);
        if (wrPasswordNumber.equals(stefanieConfig.getPasswordWrNum())) {
            userPo.setStatus(UserStatusEnum.LOCK.getCode());
        }
        userMapper.updateById(userPo);
    }


}
