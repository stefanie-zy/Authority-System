package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.domain.pojo.dto.sys.LogPageDto;
import com.stefanie.userserver.domain.pojo.po.sys.LogPo;
import com.stefanie.userserver.domain.pojo.vo.sys.LogVo;
import com.stefanie.userserver.manager.PageManager;
import com.stefanie.userserver.mapper.sys.LogMapper;
import com.stefanie.userserver.service.sys.LogService;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-20 19:36
 * @description
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class LogServiceImpl extends ServiceImpl<LogMapper, LogPo> implements LogService {

    private final LogMapper logMapper;
    private final BeanMapStructMapper beanMapStructMapper;

    @ApiOperation(value = "分页查询系统日志")
    @Override
    public IPage<LogVo> page(LogPageDto logPageDto) {
        Page<LogVo> pageVo = new Page<>();
        IPage<LogPo> pageParam = new Page<>(logPageDto.getPageNum(), logPageDto.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        // ip
        if (StringUtil.isNotEmpty(logPageDto.getIp())) {
            queryWrapper.like(ColumnConstant.IP, logPageDto.getIp());
        }
        // 模块
        if (StringUtil.isNotEmpty(logPageDto.getTitle())) {
            queryWrapper.like(ColumnConstant.TITLE, logPageDto.getTitle());
        }
        // 业务类型
        if (StringUtil.isNotNull(logPageDto.getBusinessType()) && (0 != logPageDto.getBusinessType())) {
            queryWrapper.eq(ColumnConstant.BUSINESS_TYPE, logPageDto.getBusinessType());
        }
        // 操作者
        if (StringUtil.isNotEmpty(logPageDto.getLoginName())) {
            queryWrapper.like(ColumnConstant.LONIN_NAME, logPageDto.getLoginName());
        }
        // 请求结果
        if (StringUtil.isNotNull(logPageDto.getStatus()) && (0 != logPageDto.getStatus())) {
            queryWrapper.eq(ColumnConstant.STATUS, logPageDto.getStatus());
        }
        // 时间段
        if (StringUtil.isNotNull(logPageDto.getStartTime()) && StringUtil.isNotNull(logPageDto.getEndTime())) {
            queryWrapper.between(ColumnConstant.TIME, logPageDto.getStartTime(), logPageDto.getEndTime());
        }
        // 关键字
        if (StringUtil.isNotEmpty(logPageDto.getKeyWord())) {
            queryWrapper.eq(ColumnConstant.PARAM, logPageDto.getKeyWord());
        }
        IPage pagePo = super.page(pageParam, queryWrapper);
        List<LogVo> voList = (List<LogVo>) pagePo.getRecords().stream().map(po -> beanMapStructMapper.createPoToVo((LogPo) po)).collect(Collectors.toList());
        return PageManager.poToVo(pagePo, pageVo, voList);
    }

    @ApiOperation(value = "清空表数据")
    @Override
    public void clear() {
        logMapper.delete(null);
    }
}
