package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.domain.pojo.dto.sys.LoginLogPageDto;
import com.stefanie.userserver.domain.pojo.po.sys.LoginLogPo;
import com.stefanie.userserver.domain.pojo.vo.sys.LoginLogVo;
import com.stefanie.userserver.manager.PageManager;
import com.stefanie.userserver.mapper.sys.LoginLogMapper;
import com.stefanie.userserver.service.sys.LoginLogService;
import com.stefanie.userserver.utils.ip.AddressUtil;
import com.stefanie.userserver.utils.ip.IpUtil;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.ServletUtil;
import com.stefanie.userserver.utils.other.StringUtil;
import eu.bitwalker.useragentutils.UserAgent;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 15:16
 * @description
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLogPo> implements LoginLogService {

    private final BeanMapStructMapper beanMapStructMapper;
    private final LoginLogMapper loginLogMapper;

    @ApiOperation(value = "分页查询登录日志")
    @Override
    public IPage<LoginLogVo> page(LoginLogPageDto loginLogPageDto) {
        Page<LoginLogVo> pageVo = new Page<>();
        IPage<LoginLogPo> pageParam = new Page<>(loginLogPageDto.getPageNum(), loginLogPageDto.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        // ip
        if (StringUtil.isNotEmpty(loginLogPageDto.getIp())) {
            queryWrapper.like(ColumnConstant.IP, loginLogPageDto.getIp());
        }
        // 用户
        if (StringUtil.isNotEmpty(loginLogPageDto.getLoginName())) {
            queryWrapper.like(ColumnConstant.LONIN_NAME, loginLogPageDto.getLoginName());
        }
        // 时间段
        if (StringUtil.isNotNull(loginLogPageDto.getStartTime()) && StringUtil.isNotNull(loginLogPageDto.getEndTime())) {
            queryWrapper.between(ColumnConstant.TIME, loginLogPageDto.getStartTime(), loginLogPageDto.getEndTime());
        }
        // 登录类型
        if (StringUtil.isNotNull(loginLogPageDto.getType()) && 0 != loginLogPageDto.getType()) {
            queryWrapper.eq(ColumnConstant.TYPE, loginLogPageDto.getType());
        }
        IPage pagePo = super.page(pageParam, queryWrapper);
        List<LoginLogVo> voList = (List<LoginLogVo>) pagePo.getRecords().stream().map(po -> beanMapStructMapper.createPoToVo((LoginLogPo) po)).collect(Collectors.toList());
        return PageManager.poToVo(pagePo, pageVo, voList);
    }

    @ApiOperation(value = "清空表数据")
    @Override
    public void clear() {
        loginLogMapper.delete(null);
    }

    @ApiOperation(value = "新增登录日志")
    @Override
    public void create(String loginName, Integer status, Integer type) {
        LoginLogPo loginLogPo = new LoginLogPo();
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtil.getRequest().getHeader("User-Agent"));
        loginLogPo.setTime(new Date());
        String ip = IpUtil.getIpAddr(ServletUtil.getRequest());
        loginLogPo.setIp(ip);
        loginLogPo.setBrowser(userAgent.getBrowser().getName());
        loginLogPo.setOs(userAgent.getOperatingSystem().getName());
        loginLogPo.setAddress(AddressUtil.getRealAddressByIp(ip));

        loginLogPo.setLoginName(loginName);
        loginLogPo.setStatus(status);
        loginLogPo.setType(type);
        loginLogMapper.insert(loginLogPo);
    }
}
