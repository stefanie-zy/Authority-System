package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.common.enums.error.MenuAndRoleErrorEnum;
import com.stefanie.userserver.common.enums.error.SqlErrorEnum;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserAndRoleDto;
import com.stefanie.userserver.domain.pojo.po.sys.RolePo;
import com.stefanie.userserver.domain.pojo.po.sys.UserAndRolePo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.mapper.sys.RoleAndMenuMapper;
import com.stefanie.userserver.mapper.sys.RoleMapper;
import com.stefanie.userserver.mapper.sys.UserAndRoleMapper;
import com.stefanie.userserver.service.sys.RoleService;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 17:00
 * @description
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class RoleServiceImpl extends ServiceImpl<RoleMapper, RolePo> implements RoleService {

    private final RoleMapper roleMapper;
    private final BeanMapStructMapper beanMapStructMapper;
    private final RoleAndMenuMapper roleAndMenuMapper;
    private final UserAndRoleMapper userAndRoleMapper;

    @ApiOperation(value = "新增角色")
    @Override
    public Boolean create(RoleCreateDto roleCreateDto) {
        paramCheck(roleCreateDto);
        RolePo po = beanMapStructMapper.createDtoToPo(roleCreateDto);
        return save(po);
    }

    @ApiOperation(value = "修改角色")
    @Override
    public Boolean update(RoleUpdateDto roleUpdateDto) {
        RolePo rolePo = roleMapper.selectById(roleUpdateDto.getId());
        rolePo.setRoleKey(roleUpdateDto.getRoleKey());
        rolePo.setName(roleUpdateDto.getName());
        return updateById(rolePo);
    }

    @ApiOperation(value = "删除角色")
    @Override
    // TODO 事务
    public void delete(Long roleId) {
        List<UserAndRolePo> userAndRolePoList = userAndRoleMapper.listByRoleId(roleId);
        if (userAndRolePoList.size() > 0) {
            throw new UserException(MenuAndRoleErrorEnum.B_USER_AND_ROLE_NOT_NULL);
        }
        userAndRoleMapper.deleteByRoleId(roleId);
        roleAndMenuMapper.deleteByRoleId(roleId);
        roleMapper.deleteById(roleId);
    }

    @ApiOperation(value = "用户角色绑定")
    @Override
    // TODO 事务
    public void build(UserAndRoleDto userAndRoleDto) {
        Long userId = userAndRoleDto.getUserId();
        // 参数校验
        paramCheck(userAndRoleDto);
        // 删除旧绑定信息
        userAndRoleMapper.deleteByUserId(userId);
        // 新增绑定信息
        List<UserAndRolePo> poList = new ArrayList<>();
        userAndRoleDto.getRoleIds().forEach(roleId -> {
            UserAndRolePo po = new UserAndRolePo();
            po.setUserId(userId);
            po.setRoleId(roleId);
            poList.add(po);
        });
        userAndRoleMapper.batchInsert(poList);
    }

    @ApiOperation(value = "删除角色")
    @Override
    public void unbuild(Long userId) {
        userAndRoleMapper.deleteByUserId(userId);
    }

    @ApiOperation(value = "新增-角色参数校验")
    private void paramCheck(RoleCreateDto roleCreateDto) {
        // 角色名称唯一性
        if (CommonConstant.NOT_UNIQUE.equals(roleColumnCheck(ColumnConstant.NAME, roleCreateDto.getName(), 0L))) {
            throw new UserException(MenuAndRoleErrorEnum.B_ROLE_NAME_EXIT);
        }
        // 角色权限字符串唯一性
        if (CommonConstant.NOT_UNIQUE.equals(roleColumnCheck(ColumnConstant.ROLE_KEY, roleCreateDto.getRoleKey(), 0L))) {
            throw new UserException(MenuAndRoleErrorEnum.B_ROLE_KEY_EXIT);
        }
    }

    @ApiOperation(value = "角色绑定-参数校验")
    private void paramCheck(UserAndRoleDto userAndRoleDto) {
        List<Long> roleIdList = userAndRoleDto.getRoleIds();
        if (roleIdList.size() == 0) {
            throw new UserException(MenuAndRoleErrorEnum.P_ROLE_PARAM_NULL);
        }
        // 角色是否都存在
        roleIdList.forEach(roleId -> {
            RolePo po = roleMapper.selectById(roleId);
            if (StringUtil.isNull(po)) {
                throw new UserException(MenuAndRoleErrorEnum.B_ROLE_NULL.getCode(), "角色异常：角色ID" + roleId + "不存在，请重新选择！");
            }
        });
    }

    @ApiOperation(value = "角色唯一性校验")
    public String roleColumnCheck(String column, String value, Long id) {
        RolePo rolePo = selectRolePoByQuery(column, value);
        // 逻辑说明：
        // -如果角色不存在则肯定唯一
        // -如果角色存在且 id=0L 说明是新增角色，则角色不是唯一
        // -如果角色存在且 id!=0L 说明是修改角色，且 id!=获取到的角色ID，则说明角色不是唯一
        if (rolePo != null && (id == 0L || id.longValue() != rolePo.getId())) {
            return CommonConstant.NOT_UNIQUE;
        }
        return CommonConstant.UNIQUE;
    }

    @ApiOperation(value = "根据单个条件查询角色信息", notes = "column:字段值；value:值")
    private RolePo selectRolePoByQuery(String column, String value) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(column, value);
        RolePo rolePo = null;
        try {
            rolePo = roleMapper.selectOne(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SysException(SqlErrorEnum.S_ONE_DATE_SELECT_ERROR);
        }
        return rolePo;
    }
}
