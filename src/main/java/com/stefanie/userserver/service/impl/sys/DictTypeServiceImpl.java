package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.common.enums.core.CommonEnum;
import com.stefanie.userserver.common.enums.error.OtherErrorEnum;
import com.stefanie.userserver.common.enums.error.SqlErrorEnum;
import com.stefanie.userserver.common.enums.sys.StatusEnum;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypeCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypePageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypeUpdateDto;
import com.stefanie.userserver.domain.pojo.po.sys.DictDataPo;
import com.stefanie.userserver.domain.pojo.po.sys.DictTypePo;
import com.stefanie.userserver.domain.pojo.vo.sys.DictTypeVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.manager.PageManager;
import com.stefanie.userserver.mapper.sys.DictDataMapper;
import com.stefanie.userserver.mapper.sys.DictTypeMapper;
import com.stefanie.userserver.service.sys.DictTypeServie;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.DateUtil;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-05 22:09
 * @description
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class DictTypeServiceImpl extends ServiceImpl<DictTypeMapper, DictTypePo> implements DictTypeServie {

    private final DictTypeMapper dictTypeMapper;
    private final BeanMapStructMapper beanMapStructMapper;
    private final DictDataMapper dictDataMapper;

    @ApiOperation(value = "新增字典类型")
    @Override
    public Boolean create(DictTypeCreateDto dictTypeCreateDto) {
        paramCheck(dictTypeCreateDto);
        DictTypePo po = beanMapStructMapper.createDtoToPo(dictTypeCreateDto);
        po.setStatus(StatusEnum.USABLE.getCode());
        return save(po);
    }

    @ApiOperation(value = "修改字典类型")
    @Override
    public Boolean update(DictTypeUpdateDto dictTypeUpdateDto) {
        paramCheck(dictTypeUpdateDto);
        DictTypePo po = dictTypeMapper.selectById(dictTypeUpdateDto.getId());
        po.setType(dictTypeUpdateDto.getType());
        po.setName(dictTypeUpdateDto.getName());
        return super.updateById(po);
    }

    @ApiOperation(value = "删除字典类型")
    @Override
    public void delete(Long id) {
        DictTypePo po = dictTypeMapper.selectById(id);
        SysException.check(StringUtil.isNotNull(po), OtherErrorEnum.B_DICT_TYPE_NULL);
        // 查看字典类型是否有字典数据
        List<DictDataPo> dictDataPoList = dictDataMapper.listByType(po.getType());
        SysException.check(dictDataPoList.size() == 0, OtherErrorEnum.B_DICT_TYPE_HAS_DATA);
        dictTypeMapper.deleteById(id);
    }

    @ApiOperation(value = "查询字典列表", notes = "list()与系统自带list方法重复，改用voList()")
    @Override
    public List<DictTypeVo> voList() {
        List<DictTypeVo> dictTypeVoList = new ArrayList<>();
        List<DictTypePo> dictTypePoList = dictTypeMapper.selectList(null);
        if (dictTypePoList.size() == 0) {
            return null;
        }
        dictTypePoList.forEach(po -> dictTypeVoList.add(beanMapStructMapper.createPoToVo(po)));
        return dictTypeVoList;
    }

    @ApiOperation(value = "分页查询字典类型")
    @Override
    public IPage<DictTypeVo> page(DictTypePageDto dictTypePageDto) {
        IPage<DictTypeVo> pageVo = new Page<>();
        IPage<DictTypePo> pageParam = new Page<>(dictTypePageDto.getPageNum(), dictTypePageDto.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        // 类型名称
        if (StringUtil.isNotEmpty(dictTypePageDto.getName())) {
            queryWrapper.like(ColumnConstant.DICT_NAME, dictTypePageDto.getName());
        }
        // 类型
        if ((StringUtil.isNotEmpty(dictTypePageDto.getType()))) {
            queryWrapper.like(ColumnConstant.DICT_TYPE, dictTypePageDto.getType());
        }
        // 状态
        if (StringUtil.isNotNull(dictTypePageDto.getStatus()) && (0 != dictTypePageDto.getStatus())) {
            queryWrapper.eq(ColumnConstant.DICT_STATUS, dictTypePageDto.getStatus());
        }
        IPage pagePo = super.page(pageParam, queryWrapper);
        // Po转Vo
        List<DictTypeVo> voList = (List<DictTypeVo>) pagePo.getRecords().stream().map(po -> beanMapStructMapper.createPoToVo((DictTypePo) po)).collect(Collectors.toList());
        return PageManager.poToVo(pagePo, pageVo, voList);
    }

    @ApiModelProperty(value = "修改字典类型状态")
    @Override
    public void updateStatus(Long id, Integer status, String userName) {
        DictTypePo po = dictTypeMapper.selectById(id);
        // 判断是否存在，不存在抛出异常
        SysException.check(StringUtil.isNotNull(po), OtherErrorEnum.B_DICT_TYPE_NULL);
        // 判断状态码是否正确，不正确抛出异常
        SysException.check(CommonEnum.check(StatusEnum.class, status), OtherErrorEnum.S_DICT_TYPE_ERROR);
        po.setStatus(status);
        po.setRemark(po.getRemark() + "\n" + DateUtil.getDfDateTime() + ":被用户'" + userName + "'" + CommonEnum.getMessageByCode(StatusEnum.class, status) + "！");
        updateById(po);
    }

    @ApiOperation(value = "字典类型唯一性校验")
    @Override
    public String dictTypeColumnCheck(String column, String value, Long id) {
        DictTypePo po = selectDictTypePoByQuery(column, value);
        // 逻辑说明：
        // -如果字典类型不存在则肯定唯一
        // -如果字典类型存在且 id=0L 说明是新增字典类型，则字典类型不是唯一
        // -如果字典类型存在且 id!=0L 说明是修改字典类型，且 id!=获取到的字典类型ID，则说明字典类型不是唯一
        if (po != null && (id == 0L || id.longValue() != po.getId())) {
            return CommonConstant.NOT_UNIQUE;
        }
        return CommonConstant.UNIQUE;
    }

    @ApiOperation(value = "新增-参数校验")
    private void paramCheck(DictTypeCreateDto dictTypeCreateDto) {
        // 名称
        if (CommonConstant.NOT_UNIQUE.equals(dictTypeColumnCheck(ColumnConstant.DICT_NAME, dictTypeCreateDto.getName(), 0L))) {
            throw new SysException(OtherErrorEnum.B_DICT_TYPE_NAME_EXIT);
        }
        // 类型
        if (CommonConstant.NOT_UNIQUE.equals(dictTypeColumnCheck(ColumnConstant.DICT_TYPE, dictTypeCreateDto.getType(), 0L))) {
            throw new SysException(OtherErrorEnum.B_DICT_TYPE_EXIT);
        }
    }

    @ApiOperation(value = "修改-参数校验")
    private void paramCheck(DictTypeUpdateDto dictTypeUpdateDto) {
        // 名称
        if (CommonConstant.NOT_UNIQUE.equals(dictTypeColumnCheck(ColumnConstant.DICT_NAME, dictTypeUpdateDto.getName(), dictTypeUpdateDto.getId()))) {
            throw new SysException(OtherErrorEnum.B_DICT_TYPE_NAME_EXIT);
        }
        // 类型
        if (CommonConstant.NOT_UNIQUE.equals(dictTypeColumnCheck(ColumnConstant.DICT_TYPE, dictTypeUpdateDto.getType(), dictTypeUpdateDto.getId()))) {
            throw new SysException(OtherErrorEnum.B_DICT_TYPE_EXIT);
        }
    }

    @ApiOperation(value = "根据单个条件查询字典类型信息", notes = "column:字段值；value:值")
    private DictTypePo selectDictTypePoByQuery(String column, String value) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(column, value);
        DictTypePo po = null;
        try {
            po = dictTypeMapper.selectOne(queryWrapper);
        } catch (Exception e) {
            throw new SysException(SqlErrorEnum.S_ONE_DATE_SELECT_ERROR);
        }
        return po;
    }
}
