package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.common.enums.error.ErrorEnum;
import com.stefanie.userserver.config.param.FileConfig;
import com.stefanie.userserver.domain.pojo.dto.sys.FileCreateDto;
import com.stefanie.userserver.domain.pojo.po.sys.FilePo;
import com.stefanie.userserver.domain.pojo.vo.sys.FileVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.handle.result.GoFastdfsResult;
import com.stefanie.userserver.manager.GoFastdfsManager;
import com.stefanie.userserver.mapper.sys.FileMapper;
import com.stefanie.userserver.service.sys.FileService;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * @author: stefanie
 * @createTime: 2023/07/13 14:28
 * @description:
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class FileServiceImpl extends ServiceImpl<FileMapper, FilePo> implements FileService {

    private final FileMapper fileMapper;
    private final FileConfig fileConfig;
    private final BeanMapStructMapper beanMapStructMapper;
    private final GoFastdfsManager goFastdfsManager;

    @ApiOperation(value = "文件上传")
    @Override
    public String upload(MultipartFile file, String path) {
        String uploadPath = (StringUtil.isEmpty(path)) ? fileConfig.getGoFastdfs().getDefaultPath() : path;
        if (file.isEmpty()) {
            throw new SysException(ErrorEnum.P_FILE_EMPTY);
        }
        GoFastdfsResult goFastdfsResult = goFastdfsManager.upload(file, uploadPath);
        return goFastdfsResult.getUrl();
    }

    @ApiOperation(value = "新增文件")
    @Override
    public FileVo create(FileCreateDto fileCreateDto, MultipartFile file) {
        FilePo filePo = new FilePo();
        if (file.isEmpty()) {
            throw new SysException(ErrorEnum.P_FILE_EMPTY);
        }

        Integer useType = (0 == filePo.getUserType() || StringUtil.isNull(filePo.getType())) ? 1 : filePo.getUserType();
        String uploadPath = File.separator + useType + (StringUtil.isEmpty(fileCreateDto.getPath()) ? fileConfig.getGoFastdfs().getDefaultPath() : File.separator + fileCreateDto.getPath());

        filePo.setName(StringUtil.isEmpty(fileCreateDto.getName()) ? file.getName() : fileCreateDto.getName());
        filePo.setType(file.getContentType());
        filePo.setUserType(useType);

        //上传
        GoFastdfsResult goFastdfsResult = goFastdfsManager.upload(file, uploadPath);

        filePo.setSize(goFastdfsResult.getSize());
        filePo.setPath(goFastdfsResult.getPath());
        filePo.setMd5(goFastdfsResult.getMd5());
        filePo.setUrl(goFastdfsResult.getUrl());
        super.save(filePo);

        return beanMapStructMapper.createPoToVo(filePo);
    }

    @ApiOperation(value = "删除文件")
    @Override
    public void delete(Long id) {
        fileMapper.deleteById(id);
    }

    @ApiOperation(value = "更新文件下载次数")
    @Override
    public void download(Long id) {
        FilePo filePo = fileMapper.selectById(id);
        Long downloadCount = filePo.getDownloadCount() + 1;
        fileMapper.updateDownloadById(downloadCount, id);
    }

}
