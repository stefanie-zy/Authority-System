package com.stefanie.userserver.service.impl.sys;

import com.stefanie.userserver.common.enums.error.ErrorEnum;
import com.stefanie.userserver.common.enums.error.UserErrorEnum;
import com.stefanie.userserver.common.excel.UserImportExcel;
import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import com.stefanie.userserver.domain.pojo.vo.sys.ExcelResultVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.mapper.sys.UserMapper;
import com.stefanie.userserver.service.sys.UserExcelService;
import com.stefanie.userserver.service.sys.UserService;
import com.stefanie.userserver.utils.excel.ExcelUtils;
import com.stefanie.userserver.utils.other.StringUtil;
import com.stefanie.userserver.utils.security.SecurityUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 17:00
 * @description 用户Excel相关操作接口实现类
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserExcelServiceImpl implements UserExcelService {

    private final UserService userService;
    private final UserMapper userMapper;
    private final StefanieConfig stefanieConfig;

    @ApiOperation(value = "导入用户信息")
    @Override
    public ExcelResultVo importUser(MultipartFile file, String userName) throws ExecutionException, InterruptedException {
        long startTime = System.currentTimeMillis();

        // 异步任务处理
        CompletableFuture<ExcelResultVo> future = CompletableFuture.supplyAsync(() -> {
            log.info("用户导入：Excel导入用户异步任务正在执行...");
            // 读取文件内容
            List<UserImportExcel> userExcelList;
            try {
                userExcelList = ExcelUtils.readMultipartFile(file, UserImportExcel.class);
                log.info("用户导入：Excel中用户数据为：{}", userExcelList);
            } catch (Exception e) {
                log.error("用户导入：{}", ErrorEnum.P_FILE_READ_ERROR.getMessage());
                throw new SysException(ErrorEnum.P_FILE_READ_ERROR);
            }
            // 是否为空判断
            if (userExcelList.size() == 0) {
                throw new UserException(UserErrorEnum.P_USER_EXCEL_NULL);
            }
            return importUserData(userExcelList, userName);
        });

        // 异步结果处理
        CompletableFuture<ExcelResultVo> processFuture = future.thenApply(vo -> {
            long endTime = System.currentTimeMillis();
            vo.setTime((endTime - startTime) + "毫秒");
            log.info("Excel导入用户异步任务结束，结果如下：{}", vo);
            return vo;
        });
        return processFuture.get();
    }

    @ApiOperation(value = "导入用户信息")
    private ExcelResultVo importUserData(List<UserImportExcel> dataList, String userName) {
        ExcelResultVo vo = new ExcelResultVo();
        int successCount = 0;
        int errorCount = 0;
        StringBuilder errorMessage = new StringBuilder();
        String remark = "";
        int line = 1;
        List<UserPo> userPoList = new ArrayList<>();

        for (UserImportExcel userImportExcel : dataList) {
            UserPo po = new UserPo();
            // 验证用户名是否存在
            if (!checkUserName(userImportExcel.getUserName(), errorMessage, line)) {
                errorCount++;
                line++;
                continue;
            }
            po.setUserName(userImportExcel.getUserName());

            // 验证登录名是否存在
            if (!checkLoginName(userImportExcel.getLoginName(), errorMessage, line)) {
                errorCount++;
                line++;
                continue;
            }
            po.setLoginName(userImportExcel.getLoginName());

            // 验证身份证是否存在
            if (!checkIdCard(userImportExcel.getIdCard(), errorMessage, line)) {
                errorCount++;
                line++;
                continue;
            }
            po.setCreateName(userName);
            po.setIdCard(userImportExcel.getIdCard());
            po.setPassword(SecurityUtils.encryptPassword(stefanieConfig.getPassword()));
            userPoList.add(po);
        }
        // 批量导入
        userMapper.insertBatch(userPoList);
        vo.setRemark(remark);
        vo.setErrorMessage(errorMessage.toString());
        vo.setSuccessCount(successCount);
        vo.setErrorCount(errorCount);
        return vo;
    }

    @ApiOperation(value = "用户名校验")
    private Boolean checkUserName(String userName, StringBuilder errorMessage, int line) {
        if (StringUtil.isNull(userName)) {
            errorMessage.append("<br/>第").append(line).append("行用户名为空，导入失败！");
            return false;
        }
        if (CommonConstant.NOT_UNIQUE.equals(userService.userColumnCheck(ColumnConstant.USER_NAME, userName, 0L))) {
            errorMessage.append("<br/>第").append(line).append("行用户名[").append(userName).append("]存在，导入失败！");
            return false;
        }
        return true;
    }

    @ApiOperation(value = "登录名校验")
    private Boolean checkLoginName(String loginName, StringBuilder errorMessage, int line) {
        if (StringUtil.isNull(loginName)) {
            errorMessage.append("<br/>第").append(line).append("行登录名为空，导入失败！");
            return false;
        }
        if (CommonConstant.NOT_UNIQUE.equals(userService.userColumnCheck(ColumnConstant.USER_NAME, loginName, 0L))) {
            errorMessage.append("<br/>第").append(line).append("行登录名[").append(loginName).append("]存在，导入失败！");
            return false;
        }
        return true;
    }

    @ApiOperation(value = "身份证校验")
    private Boolean checkIdCard(String idCard, StringBuilder errorMessage, int line) {
        if (StringUtil.isNull(idCard)) {
            return true;
        }
        if (CommonConstant.NOT_UNIQUE.equals(userService.userColumnCheck(ColumnConstant.ID_CARD, idCard, 0L))) {
            errorMessage.append("<br/>第").append(line).append("行身份证[").append(idCard).append("]已存在，导入失败！");
            return false;
        }
        return true;
    }

}
