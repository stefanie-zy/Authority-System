package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.common.enums.core.CommonEnum;
import com.stefanie.userserver.common.enums.error.ErrorEnum;
import com.stefanie.userserver.common.enums.error.SqlErrorEnum;
import com.stefanie.userserver.common.enums.error.UserErrorEnum;
import com.stefanie.userserver.common.enums.sys.DelFlagEnum;
import com.stefanie.userserver.common.enums.sys.LoginStatusEnum;
import com.stefanie.userserver.common.enums.sys.UserStatusEnum;
import com.stefanie.userserver.common.excel.UserExportExcel;
import com.stefanie.userserver.config.param.StefanieConfig;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.bus.UserConstant;
import com.stefanie.userserver.constant.sys.CacheConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.domain.pojo.dto.sys.UserCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserPageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserUpdatePasswordDto;
import com.stefanie.userserver.domain.pojo.po.sys.UserPo;
import com.stefanie.userserver.domain.pojo.vo.sys.UserVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.manager.PageManager;
import com.stefanie.userserver.manager.RedisCacheManager;
import com.stefanie.userserver.mapper.sys.UserAndRoleMapper;
import com.stefanie.userserver.mapper.sys.UserMapper;
import com.stefanie.userserver.service.sys.UserService;
import com.stefanie.userserver.utils.grep.GrepUtil;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.DateUtil;
import com.stefanie.userserver.utils.other.StringUtil;
import com.stefanie.userserver.utils.security.SecurityUtils;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:59
 * @description 用户接口实现层
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, UserPo> implements UserService {

    private final UserMapper userMapper;
    private final StefanieConfig stefanieConfig;
    private final BeanMapStructMapper beanMapStructMapper;
    private final RedisCacheManager redisCacheManager;
    private final UserAndRoleMapper userAndRoleMapper;

    @ApiOperation(value = "新增系统用户")
    @Override
    public Boolean create(UserCreateDto userCreateDto) {
        paramCheck(userCreateDto);
        UserPo userPo = beanMapStructMapper.createDtoToPo(userCreateDto);
        userPo.setPassword(SecurityUtils.encryptPassword(stefanieConfig.getPassword()));
        // redisCacheManager.saveBean(userPo, 0, null);
        return save(userPo);
    }

    @ApiOperation(value = "修改系统用户")
    @Override
    public Boolean update(UserUpdateDto userUpdateDto) {
        UserPo userPo = userMapper.selectById(userUpdateDto.getId());
        if (StringUtil.isNull(userPo)) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        paramCheck(userUpdateDto, userPo);
        userPo.setLoginName(userUpdateDto.getLoginName());
        userPo.setNickName(userUpdateDto.getNickName());
        userPo.setUserName(userUpdateDto.getUserName());
        userPo.setUserType(userUpdateDto.getUserType());
        userPo.setPhoneNumber(userUpdateDto.getPhoneNumber());
        userPo.setEmail(userUpdateDto.getEmail());
        userPo.setSex(userUpdateDto.getSex());
        userPo.setIdCard(userUpdateDto.getIdCard());
        userPo.setAddress(userUpdateDto.getAddress());
        userPo.setIcoUrl(userUpdateDto.getIcoUrl());
        userPo.setUpdateTime(new Date());
        userPo.setToken(null);
//        redisCacheManager.saveBean(userPo, 0, null);
        return updateById(userPo);
    }

    @ApiOperation(value = "删除用户")
    @Override
    // TODO 事务
    public Boolean delete(Long id, String currUserName) {
        UserPo userPo = userMapper.selectById(id);
        if (StringUtil.isNull(userPo)) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        // 删除校验
        deleteCheck(userPo.getDelFlag());
        // 删除标记
        userPo.setDelFlag(DelFlagEnum.YES.getCode());
        userPo.setToken(null);
        userPo.setLoginStatus(LoginStatusEnum.OUT.getCode());
        userPo.setUpdateTime(new Date());
        userPo.setUpdateName(currUserName);
        userPo.setRemark((StringUtil.isEmpty(userPo.getRemark()) ? "" : userPo.getRemark() + "\n") + DateUtil.getDfDateTime() + ":被用户'" + currUserName + "'删除！");
        //删除用户-角色信息
        userAndRoleMapper.deleteByUserId(id);
        return updateById(userPo);
    }

    @ApiOperation(value = "批量删除用户")
    @Override
    // TODO 事务
    public String deltetByIds(Long[] ids, String currUserName) {
        StringBuilder stringBuffer = new StringBuilder();
        List<Long> idList = Arrays.asList(ids);
        // Stefanie判断
        if (idList.contains(UserConstant.STEFANIE_ID)) {
            stringBuffer.append("\n超级用户不允许被删除!");
            idList.remove(UserConstant.STEFANIE_ID);
        }
        List<UserPo> userPos = userMapper.selectBatchIds(idList);
        userPos.forEach(userPo -> {
            userPo.setDelFlag(DelFlagEnum.YES.getCode());
            userPo.setToken(null);
            userPo.setUpdateTime(new Date());
            userPo.setUpdateName(currUserName);
            userPo.setLoginStatus(LoginStatusEnum.OUT.getCode());
            userPo.setRemark(userPo.getRemark() + "\n" + DateUtil.getDfDateTime() + ":被用户'" + currUserName + "'删除！");
        });
        saveOrUpdateBatch(userPos);
        // 删除用户-角色信息
        userAndRoleMapper.deleteByUserIds(idList);
        return stringBuffer.toString();
    }

    @ApiOperation(value = "分页查询系统用户")
    @Override
    public IPage<UserVo> page(UserPageDto userPageDto) {
        IPage<UserVo> pageVo = new Page<>();
        IPage<UserPo> pageParam = new Page<>(userPageDto.getPageNum(), userPageDto.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        // 用户名称
        if (StringUtil.isNotEmpty(userPageDto.getUserName())) {
            queryWrapper.like(ColumnConstant.USER_NAME, userPageDto.getUserName());
        }
        // 登录状态
        if ((StringUtil.isNotNull(userPageDto.getLoginStatus())) && (0 != userPageDto.getLoginStatus())) {
            queryWrapper.eq(ColumnConstant.LOGIN_STATUS, userPageDto.getLoginStatus());
        }
        queryWrapper.eq(ColumnConstant.DEL_FLAG, DelFlagEnum.NO.getCode());
        IPage pagePo = super.page(pageParam, queryWrapper);
        // Po转Vo
        List<UserVo> voList = (List<UserVo>) pagePo.getRecords().stream().map(po -> beanMapStructMapper.createPoToVo((UserPo) po)).collect(Collectors.toList());
        return PageManager.poToVo(pagePo, pageVo, voList);
    }

    @ApiOperation(value = "导出系统用户信息")
    @Override
    public List<UserExportExcel> exportUserExcelList() {
        return userMapper.selectExcelDataByDelFlag(DelFlagEnum.NO.getCode());
    }

    @ApiOperation(value = "用户唯一性校验")
    @Override
    public String userColumnCheck(String column, String value, Long id) {
        UserPo userPo = selectUserPoByQuery(column, value);
        // 逻辑说明：
        // -如果用户不存在则肯定唯一
        // -如果用户存在且 id=0L 说明是新增用户，则用户不是唯一
        // -如果用户存在且 id!=0L 说明是修改用户，且 id!=获取到的用户ID，则说明用户不是唯一
        if (userPo != null && (id == 0L || id.longValue() != userPo.getId())) {
            return CommonConstant.NOT_UNIQUE;
        }
        return CommonConstant.UNIQUE;
    }

    @ApiOperation(value = "用户是否允许修改校验")
    @Override
    public String userAllowedCheck(Long id) {
        // 判断用户是否存在
        if (StringUtil.isNull(userMapper.selectById(id))) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        // 判断用户是否为超级用户
        if (isStefanie(id)) {
            throw new UserException(UserErrorEnum.B_USER_NO_PERMISSION_OPERATE);
        }
        return UserConstant.ALLOW;
    }

    @ApiOperation(value = "用户状态校验")
    @Override
    public void userStatusCheck(Long id, Integer status) {
        UserPo userPo = userMapper.selectById(id);
        UserStatusEnum userStatusEnum = CommonEnum.getEnumByCode(UserStatusEnum.class, userPo.getStatus());
        switch (userStatusEnum) {
            case USABLE:
            case LOCK:
                break;
            case WRITE_OFF:
                if (UserStatusEnum.LOCK.getCode().equals(status)) {
                    throw new UserException(UserErrorEnum.B_USER_WRITE_OFF);
                }
                break;
            default:
                throw new SysException(ErrorEnum.S_ENUM_FORMAT_ERROR);
        }
    }

    @ApiOperation(value = "修改用户状态")
    @Override
    public Boolean updateStatus(Long id, Integer status, String currUserName) {
        UserPo userPo = userMapper.selectById(id);
        if (UserStatusEnum.USABLE.getCode().equals(status)) {
            // 解锁操作的时候默认将错误密码重置
            userPo.setWrPasswordNumber(0);
        }
        userPo.setStatus(status);
        userPo.setUpdateName(currUserName);
        userPo.setToken(null);
        userPo.setLoginStatus(LoginStatusEnum.OUT.getCode());
        userPo.setRemark(userPo.getRemark() + "\n" + DateUtil.getDfDateTime() + ":被用户'" + currUserName + "'" + CommonEnum.getMessageByCode(UserStatusEnum.class, status) + "！");
//        redisCacheManager.saveBean(userPo, 0, null);
        return updateById(userPo);
    }

    @ApiOperation(value = "根据用户ID查询用户")
    @Override
    @Cacheable(cacheNames = CacheConstant.CAFFEINE_LOCAL_NAME, key = "#id", unless = "#result==null")
    public UserVo selectById(Long id) {
        UserVo userVo = null;
        UserPo userPo = null;
        userPo = (UserPo) redisCacheManager.getBean(id, UserPo.class);
        if (StringUtil.isNull(userPo)) {
            userPo = userMapper.selectById(id);
            redisCacheManager.saveBean(userPo, 0, null);
        }
        if (StringUtil.isNull(userPo)) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        // 删除验证-抛出异常
        deleteCheck(userPo.getDelFlag());
        userVo = beanMapStructMapper.createPoToVo(userPo);
        return userVo;
    }

    @ApiOperation(value = "修改密码")
    @Override
    public Boolean updatePassword(UserUpdatePasswordDto userUpdatePasswordDto) {
        UserPo userPo = userMapper.selectById(userUpdatePasswordDto.getId());
        if (StringUtil.isNull(userPo)) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        // 新旧密码
        if (userUpdatePasswordDto.getOldPassword().equals(userUpdatePasswordDto.getNewPassword())) {
            throw new UserException(UserErrorEnum.P_USER_PASSWORD_MATCH);
        }
        // 密码确认
        if (!userUpdatePasswordDto.getNewPassword().equals(userUpdatePasswordDto.getConfirmPassword())) {
            throw new UserException(UserErrorEnum.P_USER_CONFIRM_PASSWORD_ERROR);
        }
        userPo.setPassword(SecurityUtils.encryptPassword(userUpdatePasswordDto.getNewPassword()));
        userPo.setUpdateName(userPo.getUserName());
        userPo.setUpdateTime(new Date());
        redisCacheManager.saveBean(userPo, 0, null);
        return updateById(userPo);
    }

    @ApiOperation(value = "重置密码")
    @Override
    public Boolean resetPassword(Long id, String currUserName) {
        UserPo userPo = userMapper.selectById(id);
        if (StringUtil.isNull(userPo)) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        userPo.setPassword(SecurityUtils.encryptPassword(stefanieConfig.getPassword()));
        userPo.setUpdateName(currUserName);
        userPo.setUpdateTime(new Date());
        redisCacheManager.saveBean(userPo, 0, null);
        return updateById(userPo);
    }

    @ApiOperation(value = "旧密码校验")
    @Override
    public void oldPasswordCheck(Long id, String oldPassword) {
        UserPo userPo = userMapper.selectById(id);
        if (StringUtil.isNull(userPo)) {
            throw new UserException(UserErrorEnum.B_USER_NOT_EXIT);
        }
        // 密码校验
        if (!SecurityUtils.matchsPassword(oldPassword, userPo.getPassword())) {
            throw new UserException(UserErrorEnum.P_USER_OLD_PASSWORD_ERROR_ENUM);
        }
        return;
    }

    @ApiOperation(value = "判断是否为超级用户", notes = "true-是超级用户，false-不是超级用户")
    private boolean isStefanie(Long id) {
        UserPo userPo = userMapper.selectById(id);
        // 判断用户名、用户ID、登录名
        return userPo != null && userPo.getUserName().equals(UserConstant.STEFANIE_NAME) && id.longValue() == UserConstant.STEFANIE_ID.longValue() && userPo.getLoginName().equals(UserConstant.STEFANIE_NAME);
    }

    @ApiOperation(value = "修改-用户参数校验")
    private void paramCheck(UserUpdateDto userUpdateDto, UserPo userPo) {
        // 状态判断
        UserStatusEnum userStatusEnum = CommonEnum.getEnumByCode(UserStatusEnum.class, userPo.getStatus());
        switch (userStatusEnum) {
            case USABLE:
                break;
            case LOCK:
                throw new UserException(UserErrorEnum.B_USER_LOCK);
            case WRITE_OFF:
                throw new UserException(UserErrorEnum.B_USER_WRITE_OFF);
            default:
                throw new SysException(ErrorEnum.S_ENUM_FORMAT_ERROR);
        }
        // 删除标记判断
        deleteCheck(userPo.getDelFlag());
        // 登录名称:不包含中文
        if (GrepUtil.isContainsChinese(userUpdateDto.getLoginName())) {
            throw new UserException(UserErrorEnum.P_USER_LOGIN_NAME_CAN_NOT_BE_CHINESERROR);
        }
        // 防重复
        // -登录名称
        if (CommonConstant.NOT_UNIQUE.equals(userColumnCheck(ColumnConstant.LONIN_NAME, userUpdateDto.getLoginName(), userUpdateDto.getId()))) {
            throw new UserException(UserErrorEnum.B_USER_LOGIN_NAME_EXIT);
        }
        // -手机号
        if (CommonConstant.NOT_UNIQUE.equals(userColumnCheck(ColumnConstant.PHONE_NUMBER, userUpdateDto.getPhoneNumber(), userUpdateDto.getId()))) {
            throw new UserException(UserErrorEnum.B_USER_PHONE_NUMBER_EXIT);
        }
        // -身份证
        if (CommonConstant.NOT_UNIQUE.equals(userColumnCheck(ColumnConstant.ID_CARD, userUpdateDto.getIdCard(), userUpdateDto.getId()))) {
            throw new UserException(UserErrorEnum.B_USER_ID_CARD_EXIT);
        }
    }

    @ApiOperation(value = "新增-用户参数校验")
    private void paramCheck(UserCreateDto userCreateDto) {
        // 登录名称:不包含中文
        if (GrepUtil.isContainsChinese(userCreateDto.getLoginName())) {
            throw new UserException(UserErrorEnum.P_USER_LOGIN_NAME_CAN_NOT_BE_CHINESERROR);
        }
        // 登录名
        if (CommonConstant.NOT_UNIQUE.equals(userColumnCheck(ColumnConstant.LONIN_NAME, userCreateDto.getLoginName(), 0L))) {
            throw new UserException(UserErrorEnum.B_USER_LOGIN_NAME_EXIT);
        }
        // 手机号
        if (CommonConstant.NOT_UNIQUE.equals(userColumnCheck(ColumnConstant.PHONE_NUMBER, userCreateDto.getPhoneNumber(), 0L))) {
            throw new UserException(UserErrorEnum.B_USER_PHONE_NUMBER_EXIT);
        }
        // 身份证
        if (CommonConstant.NOT_UNIQUE.equals(userColumnCheck(ColumnConstant.ID_CARD, userCreateDto.getIdCard(), 0L))) {
            throw new UserException(UserErrorEnum.B_USER_ID_CARD_EXIT);
        }
    }

    @ApiOperation(value = "根据单个条件查询系统用户信息", notes = "column:字段值；value:值")
    private UserPo selectUserPoByQuery(String column, String value) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(column, value);
        UserPo userPo = null;
        try {
            userPo = userMapper.selectOne(queryWrapper);
        } catch (Exception e) {
            throw new SysException(SqlErrorEnum.S_ONE_DATE_SELECT_ERROR);
        }
        return userPo;
    }

    @ApiOperation(value = "删除校验")
    private void deleteCheck(Integer delFlag) {
        DelFlagEnum delFlagEnum = CommonEnum.getEnumByCode(DelFlagEnum.class, delFlag);
        switch (delFlagEnum) {
            case YES:
                throw new UserException(UserErrorEnum.B_USER_DELETE);
            case NO:
                break;
            default:
                throw new SysException(ErrorEnum.S_ENUM_FORMAT_ERROR);
        }
    }

}
