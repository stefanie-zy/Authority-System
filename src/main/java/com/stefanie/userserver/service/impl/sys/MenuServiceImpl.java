package com.stefanie.userserver.service.impl.sys;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stefanie.userserver.common.enums.core.CommonEnum;
import com.stefanie.userserver.common.enums.error.MenuAndRoleErrorEnum;
import com.stefanie.userserver.common.enums.error.SqlErrorEnum;
import com.stefanie.userserver.common.enums.sys.StatusEnum;
import com.stefanie.userserver.common.enums.sys.VisibleEnum;
import com.stefanie.userserver.constant.bus.ColumnConstant;
import com.stefanie.userserver.constant.bus.MenuConstant;
import com.stefanie.userserver.constant.bus.UserConstant;
import com.stefanie.userserver.constant.sys.CommonConstant;
import com.stefanie.userserver.domain.pojo.dto.sys.MenuCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.MenuUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleAndMenuDto;
import com.stefanie.userserver.domain.pojo.po.sys.MenuPo;
import com.stefanie.userserver.domain.pojo.po.sys.RoleAndMenuPo;
import com.stefanie.userserver.domain.pojo.vo.sys.MenuVo;
import com.stefanie.userserver.domain.pojo.vo.sys.TreeMenuVo;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.exception.sys.UserException;
import com.stefanie.userserver.mapper.sys.MenuMapper;
import com.stefanie.userserver.mapper.sys.RoleAndMenuMapper;
import com.stefanie.userserver.mapper.sys.UserAndRoleMapper;
import com.stefanie.userserver.service.sys.MenuService;
import com.stefanie.userserver.utils.mapstruct.BeanMapStructMapper;
import com.stefanie.userserver.utils.other.DateUtil;
import com.stefanie.userserver.utils.other.StringUtil;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 16:50
 * @description
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuPo> implements MenuService {

    private final MenuMapper menuMapper;
    private final BeanMapStructMapper beanMapStructMapper;
    private final RoleAndMenuMapper roleAndMenuMapper;
    private final UserAndRoleMapper userAndRoleMapper;

    @ApiOperation(value = "新增系统菜单")
    @Override
    public Boolean create(MenuCreateDto menuCreateDto) {
        paramCheck(menuCreateDto);
        MenuPo menuPo = beanMapStructMapper.createDtoToPo(menuCreateDto);
        return save(menuPo);
    }

    @ApiOperation(value = "修改系统菜单")
    @Override
    public Boolean update(MenuUpdateDto menuUpdateDto) {
        if (menuUpdateDto.getParentId().equals(menuUpdateDto.getId())) {
            throw new UserException(MenuAndRoleErrorEnum.B_MENU_PARENT_ID_ERROR);
        }
        MenuPo menuPo = menuMapper.selectById(menuUpdateDto.getId());
        if (StringUtil.isNull(menuPo)) {
            throw new UserException(MenuAndRoleErrorEnum.B_MENU_NOT_EXIT);
        }
        paramCheck(menuUpdateDto);
        menuPo.setName(menuUpdateDto.getName());
        menuPo.setParentId(menuUpdateDto.getParentId());
        menuPo.setSort(menuUpdateDto.getSort());
        menuPo.setComponent(menuUpdateDto.getComponent());
        menuPo.setPath(menuUpdateDto.getPath());
        menuPo.setIsFrame(menuUpdateDto.getIsFrame());
        menuPo.setQuery(menuUpdateDto.getQuery());
        menuPo.setIsCache(menuUpdateDto.getIsCache());
        menuPo.setType(menuUpdateDto.getType());
        menuPo.setPerms(menuUpdateDto.getPerms());
        menuPo.setIcon(menuUpdateDto.getIcon());
        menuPo.setUpdateName(menuUpdateDto.getUpdateName());
        return updateById(menuPo);
    }

    @ApiOperation(value = "修改菜单状态")
    @Override
    public Boolean updateStatus(Long id, Integer status, String currUserName) {
        MenuPo menuPo = menuMapper.selectById(id);
        if (StringUtil.isNull(menuPo)) {
            throw new UserException(MenuAndRoleErrorEnum.B_MENU_NOT_EXIT);
        }
        menuPo.setStatus(status);
        menuPo.setUpdateName(currUserName);
        menuPo.setRemark(menuPo.getRemark() + "\n" + DateUtil.getDfDateTime() + ":被用户'" + currUserName + "'" + CommonEnum.getMessageByCode(StatusEnum.class, status) + "！");
        return updateById(menuPo);
    }

    @ApiOperation(value = "修改菜单状态")
    @Override
    public Boolean updateVisible(Long id, Integer visible, String currUserName) {
        MenuPo menuPo = menuMapper.selectById(id);
        if (StringUtil.isNull(menuPo)) {
            throw new UserException(MenuAndRoleErrorEnum.B_MENU_NOT_EXIT);
        }
        menuPo.setVisible(visible);
        menuPo.setUpdateName(currUserName);
        menuPo.setRemark(menuPo.getRemark() + "\n" + DateUtil.getDfDateTime() + ":被用户'" + currUserName + "'" + CommonEnum.getMessageByCode(VisibleEnum.class, visible) + "！");
        return updateById(menuPo);
    }

    @ApiOperation(value = "删除菜单")
    @Override
    // TODO  事务
    public void delete(Long id) {
        roleAndMenuMapper.deleteByMenuId(id);
        menuMapper.deleteById(id);
    }

    @ApiOperation(value = "批量删除菜单")
    @Override
    // TODO 事务
    public void deleteByIds(Long[] ids) {
        List<Long> menuIdList = Arrays.asList(ids);
        roleAndMenuMapper.deleteByMenuIds(menuIdList);
        menuMapper.deleteBatchIds(menuIdList);
    }

    @ApiOperation(value = "查询树状菜单")
    @Override
    public List<TreeMenuVo> listTree(Long userId) {
        List<MenuPo> menuPoList = listByUserId(userId);
        if (menuPoList.size() == 0) {
            return null;
        }
        return listTree(menuPoList);
    }

    @ApiOperation(value = "查询用户所拥有的菜单")
    @Override
    public List<MenuVo> list(Long userId) {
        List<MenuVo> menuVoList = new ArrayList<>();
        List<MenuPo> menuPoList = listByUserId(userId);
        if (menuPoList.size() == 0) {
            return null;
        }
        menuPoList.forEach(po -> menuVoList.add(beanMapStructMapper.createPoToVo(po)));
        return menuVoList;
    }

    @ApiOperation(value = "角色-菜单解绑")
    @Override
    public void unbuild(Long roleId) {
        roleAndMenuMapper.deleteByRoleId(roleId);
    }

    @ApiOperation(value = "角色-菜单绑定")
    @Override
    public void build(RoleAndMenuDto roleAndMenuDto) {
        Long roleId = roleAndMenuDto.getRoleId();
        // 参数校验
        paramCheck(roleAndMenuDto);
        // 删除旧绑定信息
        roleAndMenuMapper.deleteByRoleId(roleId);
        // 新增绑定信息
        List<RoleAndMenuPo> poList = new ArrayList<>();
        roleAndMenuDto.getMenuIds().forEach(menuId -> {
            RoleAndMenuPo po = new RoleAndMenuPo();
            po.setRoleId(roleId);
            po.setMenuId(menuId);
            poList.add(po);
        });
        roleAndMenuMapper.batchInsert(poList);
    }

    @ApiOperation(value = "菜单绑定-参数校验")
    private void paramCheck(RoleAndMenuDto roleAndMenuDto) {
        List<Long> menuIdList = roleAndMenuDto.getMenuIds();
        if (menuIdList.size() == 0) {
            throw new UserException(MenuAndRoleErrorEnum.P_MENU_PARAM_NULL);
        }
        // 菜单是否都存在
        menuIdList.forEach(menuId -> {
            MenuPo po = menuMapper.selectById(menuId);
            if (StringUtil.isNull(po)) {
                throw new UserException(MenuAndRoleErrorEnum.B_MENU_NOT_EXIT.getCode(), "菜单异常：菜单ID" + menuId + "不存在，请重新选择！");
            }
        });
    }

    @ApiOperation(value = "将菜单列表转换为树状列表")
    private List<TreeMenuVo> listTree(List<MenuPo> menuPoList) {
        List<TreeMenuVo> treeMenuVoList = new ArrayList<>();
        menuPoList.forEach(po -> {
            // 所有菜单的最顶级菜单ID都是0L，因此从0L开始递归循环
            if (po.getParentId().equals(MenuConstant.SUPER_PARENT_ID)) {
                treeMenuVoList.add(getTreeMenuVo(menuPoList, po));
            }
        });
        return treeMenuVoList;
    }

    @ApiOperation(value = "根据用户ID查询用户所有的菜单列表")
    private List<MenuPo> listByUserId(Long userId) {
        if (UserConstant.STEFANIE_ID.equals(userId)) {
            return menuMapper.selectList(null);
        }
        // 查询角色、菜单
        List<Long> roleIdList = userAndRoleMapper.roleIdlistByUserId(userId);
        if (roleIdList.size() > 0) {
            List<Long> menuIdList = roleAndMenuMapper.menuIdListByRoleIdIn(roleIdList);
            if (menuIdList.size() > 0) {
                return menuMapper.selectBatchIds(menuIdList);
            }
            return null;
        }
        return null;
    }

    @ApiOperation(value = "根据菜单列表和父级菜单递归父级菜单的树状结构")
    private TreeMenuVo getTreeMenuVo(List<MenuPo> menuPoList, MenuPo menuPo) {
        TreeMenuVo treeMenuVo = beanMapStructMapper.createPoToTreeVo(menuPo);
        // 查询子菜单个数
        List<MenuPo> childList = menuPoList.stream().filter(po -> po.getParentId().equals(menuPo.getId())).collect(Collectors.toList());
        if (childList.size() == 0L) {
            return treeMenuVo;
        }
        List<TreeMenuVo> childVoList = new ArrayList<>();
        // 递归子菜单
        childList.forEach(po -> childVoList.add(getTreeMenuVo(menuPoList, po)));
        treeMenuVo.setChildren(childVoList);
        return treeMenuVo;
    }

    @ApiOperation(value = "新增-菜单参数校验")
    private void paramCheck(MenuCreateDto menuCreateDto) {
        // 名称
        if (CommonConstant.NOT_UNIQUE.equals(rmenuColumnCheck(ColumnConstant.NAME, menuCreateDto.getName(), 0L))) {
            throw new UserException(MenuAndRoleErrorEnum.B_MENU_NAME_EXIT);
        }
        // 父级ID
        if (menuCreateDto.getParentId() > 0 && menuCreateDto.getParentId() != 0) {
            MenuPo menuPo = menuMapper.selectById(menuCreateDto.getParentId());
            if (menuPo == null) {
                throw new UserException(MenuAndRoleErrorEnum.B_PARENT_MENU_NOT_EXIT);
            }
        }
        if (0 > menuCreateDto.getParentId()) {
            throw new UserException(MenuAndRoleErrorEnum.P_MENU_PARENT_ID_ERROR);
        }
    }

    @ApiOperation(value = "修改-菜单参数校验")
    private void paramCheck(MenuUpdateDto menuUpdateDto) {
        if (CommonConstant.NOT_UNIQUE.equals(rmenuColumnCheck(ColumnConstant.NAME, menuUpdateDto.getName(), menuUpdateDto.getId()))) {
            throw new UserException(MenuAndRoleErrorEnum.B_MENU_NAME_EXIT);
        }
        // 父级ID
        if (menuUpdateDto.getParentId() > 0 && menuUpdateDto.getParentId() != 0) {
            MenuPo menuPo = menuMapper.selectById(menuUpdateDto.getParentId());
            if (menuPo == null) {
                throw new UserException(MenuAndRoleErrorEnum.B_PARENT_MENU_NOT_EXIT);
            }
        }
        if (0 > menuUpdateDto.getParentId()) {
            throw new UserException(MenuAndRoleErrorEnum.P_MENU_PARENT_ID_ERROR);
        }
    }

    @ApiOperation(value = "菜单唯一性校验")
    private String rmenuColumnCheck(String column, String value, Long id) {
        MenuPo menuPo = selectMenuPoByQuery(column, value);
        // 逻辑说明：
        // -如果菜单不存在则肯定唯一
        // -如果菜单存在且 id=0L 说明是新增菜单，则菜单不是唯一
        // -如果菜单存在且 id!=0L 说明是修改菜单，且 id!=获取到的菜单ID，则说明菜单不是唯一
        if (menuPo != null && (id == 0L || id.longValue() != menuPo.getId())) {
            return CommonConstant.NOT_UNIQUE;
        }
        return CommonConstant.UNIQUE;
    }

    @ApiOperation(value = "根据单个条件查询菜单信息", notes = "column:字段值；value:值")
    private MenuPo selectMenuPoByQuery(String column, String value) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(column, value);
        MenuPo menuPo = null;
        try {
            menuPo = menuMapper.selectOne(queryWrapper);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SysException(SqlErrorEnum.S_ONE_DATE_SELECT_ERROR);
        }
        return menuPo;
    }
}
