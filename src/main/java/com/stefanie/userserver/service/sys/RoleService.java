package com.stefanie.userserver.service.sys;

import com.stefanie.userserver.domain.pojo.dto.sys.RoleCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserAndRoleDto;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 17:00
 * @description
 */
public interface RoleService {
    Boolean create(RoleCreateDto roleCreateDto);

    Boolean update(RoleUpdateDto roleUpdateDto);

    void delete(Long roleId);

    void build(UserAndRoleDto userAndRoleDto);

    void unbuild(Long userId);
}
