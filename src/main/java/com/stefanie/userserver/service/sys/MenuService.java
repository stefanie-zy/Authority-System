package com.stefanie.userserver.service.sys;

import com.stefanie.userserver.domain.pojo.dto.sys.MenuCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.MenuUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.RoleAndMenuDto;
import com.stefanie.userserver.domain.pojo.vo.sys.MenuVo;
import com.stefanie.userserver.domain.pojo.vo.sys.TreeMenuVo;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-24 16:49
 * @description
 */
public interface MenuService {
    Boolean create(MenuCreateDto menuCreateDto);

    Boolean update(MenuUpdateDto menuUpdateDto);

    Boolean updateStatus(Long id, Integer status, String currUserName);

    Boolean updateVisible(Long id, Integer visible, String currUserName);

    void delete(Long id);

    void deleteByIds(Long[] ids);

    List<TreeMenuVo> listTree(Long userId);

    List<MenuVo> list(Long userId);

    void unbuild(Long roleId);

    void build(RoleAndMenuDto roleAndMenuDto);
}
