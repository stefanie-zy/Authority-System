package com.stefanie.userserver.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataPageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictDataUpdateDto;
import com.stefanie.userserver.domain.pojo.vo.sys.DictDataVo;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-05 22:08
 * @description
 */
public interface DictDataService {
    Boolean create(DictDataCreateDto dictDataCreateDto);

    Boolean update(DictDataUpdateDto dictDataUpdateDto);

    void delete(Long id);

    IPage<DictDataVo> page(DictDataPageDto dictDataPageDto);

    void updateStatus(Long id, Integer status, String userName);

    String check(String type, String label, String value, Long id);

    List<DictDataVo> listByType(String type);
}
