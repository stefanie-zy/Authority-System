package com.stefanie.userserver.service.sys;

import com.stefanie.userserver.domain.pojo.vo.sys.ExcelResultVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.concurrent.ExecutionException;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 17:00
 * @description
 */
public interface UserExcelService {
    ExcelResultVo importUser(MultipartFile file, String currUserName) throws ExecutionException, InterruptedException;
}
