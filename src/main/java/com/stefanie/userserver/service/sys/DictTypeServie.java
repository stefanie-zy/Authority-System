package com.stefanie.userserver.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypeCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypePageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.DictTypeUpdateDto;
import com.stefanie.userserver.domain.pojo.vo.sys.DictTypeVo;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-07-05 22:09
 * @description
 */
public interface DictTypeServie {

    Boolean create(DictTypeCreateDto dictTypeCreateDto);

    Boolean update(DictTypeUpdateDto dictTypeUpdateDto);

    void delete(Long id);

    List<DictTypeVo> voList();

    IPage<DictTypeVo> page(DictTypePageDto dictTypePageDto);

    void updateStatus(Long id, Integer status, String userName);

    String dictTypeColumnCheck(String column, String value, Long id);
}
