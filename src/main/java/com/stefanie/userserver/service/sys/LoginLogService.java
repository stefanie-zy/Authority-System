package com.stefanie.userserver.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.stefanie.userserver.domain.pojo.dto.sys.LoginLogPageDto;
import com.stefanie.userserver.domain.pojo.vo.sys.LoginLogVo;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-23 15:16
 * @description
 */
public interface LoginLogService {
    IPage<LoginLogVo> page(LoginLogPageDto loginLogPageDto);

    void clear();

    void create(String loginName, Integer status, Integer type);
}
