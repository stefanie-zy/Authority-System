package com.stefanie.userserver.service.sys;

import com.stefanie.userserver.domain.pojo.bo.sys.LoginUserBo;
import com.stefanie.userserver.domain.pojo.dto.sys.LoginDto;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-04 16:43
 * @description 登录接口
 */
public interface LoginService {

    String login(LoginDto loginDto);

    void logout(LoginUserBo loginUserBo);
}
