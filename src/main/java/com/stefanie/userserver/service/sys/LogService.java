package com.stefanie.userserver.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.stefanie.userserver.domain.pojo.dto.sys.LogPageDto;
import com.stefanie.userserver.domain.pojo.vo.sys.LogVo;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-06-20 19:36
 * @description
 */
public interface LogService {
    IPage<LogVo> page(LogPageDto logPageDto);

    void clear();
}
