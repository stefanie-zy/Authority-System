package com.stefanie.userserver.service.sys;

import com.stefanie.userserver.domain.pojo.dto.sys.FileCreateDto;
import com.stefanie.userserver.domain.pojo.vo.sys.FileVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author: stefanie
 * @createTime: 2023/07/13 14:27
 * @description:
 */
public interface FileService {
    String upload(MultipartFile file, String path);

    FileVo create(FileCreateDto fileCreateDto, MultipartFile file);

    void delete(Long id);

    void download(Long id);
}
