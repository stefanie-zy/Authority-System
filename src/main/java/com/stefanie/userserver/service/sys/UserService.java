package com.stefanie.userserver.service.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.stefanie.userserver.common.excel.UserExportExcel;
import com.stefanie.userserver.domain.pojo.dto.sys.UserCreateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserPageDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserUpdateDto;
import com.stefanie.userserver.domain.pojo.dto.sys.UserUpdatePasswordDto;
import com.stefanie.userserver.domain.pojo.vo.sys.UserVo;

import java.util.List;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-19 16:59
 * @description 用户接口层
 */
public interface UserService {

    Boolean create(UserCreateDto userCreateDto);

    Boolean update(UserUpdateDto userUpdateDto);

    Boolean delete(Long id, String currUserName);

    IPage<UserVo> page(UserPageDto userPageDto);

    List<UserExportExcel> exportUserExcelList();

    String userColumnCheck(String column, String value, Long id);

    String userAllowedCheck(Long id);

    String deltetByIds(Long[] ids, String currUserName);

    void userStatusCheck(Long id, Integer status);

    Boolean updateStatus(Long id, Integer status, String currUserName);

    UserVo selectById(Long id);

    Boolean updatePassword(UserUpdatePasswordDto userUpdatePasswordDto);

    Boolean resetPassword(Long id, String currUserName);

    void oldPasswordCheck(Long id, String oldPassword);
}
