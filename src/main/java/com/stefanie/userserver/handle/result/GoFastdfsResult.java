package com.stefanie.userserver.handle.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: stefanie
 * @createTime: 2023/07/14 14:31
 * @description:
 */
@ApiModel(value = "返回结果-Go-Fastdfs")
@Data
public class GoFastdfsResult implements Serializable {
    private static final long serialVersionUID = -162467266110764290L;

    @ApiModelProperty(value = "文件服务器地址")
    private String domain;
    @ApiModelProperty(value = "md5")
    private String md5;
    @ApiModelProperty(value = "上传时间")
    private String mtime;
    @ApiModelProperty(value = "文件服务器存放地址")
    private String path;
    @ApiModelProperty(value = "返回结果编码")
    private Long retcode;
    @ApiModelProperty(value = "返回结果说明")
    private String retmsg;
    @ApiModelProperty(value = "文件服务器场景")
    private String scene;
    @ApiModelProperty(value = "文件服务器场景")
    private String scenes;
    @ApiModelProperty(value = "文件大小")
    private Long size;
    @ApiModelProperty(value = "文件源地址")
    private String src;
    @ApiModelProperty(value = "文件下载地址")
    private String url;
}
