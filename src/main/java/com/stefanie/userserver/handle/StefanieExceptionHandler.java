package com.stefanie.userserver.handle;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.stefanie.userserver.common.enums.error.ErrorEnum;
import com.stefanie.userserver.exception.sys.SysException;
import com.stefanie.userserver.exception.sys.UserException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 16:40
 * @description 全局异常处理类
 */
@RestControllerAdvice
@Slf4j
public class StefanieExceptionHandler {

    /**
     * 用户异常
     */
    @ExceptionHandler(UserException.class)
    public Stefanie<T> handleUserException(UserException e) {
        log.error("{}：", e.getMessage());
        log.info("RESPONSE:{}", JSONUtil.toJsonStr(e));
        return Stefanie.fail(e.getCode(), e.getMessage());
    }

    /**
     * 系统异常
     */
    @ExceptionHandler(SysException.class)
    public Stefanie<T> handleSysException(SysException e) {
        log.error("{}：", e.getMessage());
        log.info("RESPONSE:{}", JSONUtil.toJsonStr(e));
        return Stefanie.fail(e.getCode(), e.getMessage());
    }

    /**
     * 参数校验异常
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Stefanie<T> handleValidatedException(HttpServletRequest request, MethodArgumentNotValidException e) {
        log.error("{}：", e.getMessage());
        log.info("RESPONSE:{}", JSONUtil.toJsonStr(e));
        JSONObject errorMsg = new JSONObject();
        for (FieldError error : e.getBindingResult().getFieldErrors()) {
            errorMsg.put(error.getField(), error.getDefaultMessage());
        }
        return Stefanie.fail(ErrorEnum.P_PARAM_CHECK_ERROR_ENUM.getCode(), ErrorEnum.P_PARAM_CHECK_ERROR_ENUM.getMessage(), errorMsg);

    }

}
