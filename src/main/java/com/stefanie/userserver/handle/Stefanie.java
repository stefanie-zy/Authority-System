package com.stefanie.userserver.handle;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.SystemClock;
import com.stefanie.userserver.common.enums.error.ErrorEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author zy
 * @version v1.0.1
 * @create 2022-12-24 15:32
 * @description 接口返回值
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "接口返回值")
public class Stefanie<T> {

    @ApiModelProperty(value = "响应码", required = true)
    private Integer code;
    @ApiModelProperty(value = "响应信息")
    private String message;
    @ApiModelProperty(value = "响应数据")
    private T data;
    @ApiModelProperty(value = "响应时间")
    private long timestamp;

    public static Stefanie success() {
        return new Stefanie(ErrorEnum.SUCCESS.getCode(), ErrorEnum.SUCCESS.getMessage(), null, SystemClock.now());
    }

    public static <T> Stefanie<T> success(T data) {
        return new Stefanie(ErrorEnum.SUCCESS.getCode(), ErrorEnum.SUCCESS.getMessage(), data, SystemClock.now());
    }

    public static <T> Stefanie<T> success(String message, T data) {
        return new Stefanie(ErrorEnum.SUCCESS.getCode(), message, data, SystemClock.now());
    }

    public static Stefanie fail(ErrorEnum error) {
        return new Stefanie(error.getCode(), error.getMessage(), null, SystemClock.now());
    }

    public static Stefanie fail(Integer code, String message) {
        return new Stefanie(code, message, null, SystemClock.now());
    }

    public static Stefanie fail(Integer code, String message, JSONObject data) {
        return new Stefanie(code, message, data, SystemClock.now());
    }
}
