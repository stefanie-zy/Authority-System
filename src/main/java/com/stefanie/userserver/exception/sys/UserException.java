package com.stefanie.userserver.exception.sys;

import com.stefanie.userserver.common.enums.error.MenuAndRoleErrorEnum;
import com.stefanie.userserver.common.enums.error.UserErrorEnum;
import com.stefanie.userserver.exception.core.BeanException;
import io.swagger.annotations.ApiModel;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 16:19
 * @description 用户异常类
 */
@ApiModel(value = "异常-用户")
public class UserException extends BeanException {
    private static final long serialVersionUID = -3092366122489609231L;

    /**
     * 自定义用户异常
     */
    public UserException(Integer code, String message) {
        super(code, message);
    }

    /**
     * 用户基础异常
     */
    public UserException(UserErrorEnum userErrorEnum) {
        super(userErrorEnum.getCode(), userErrorEnum.getMessage());
    }

    /**
     * 菜单角色异常
     */
    public UserException(MenuAndRoleErrorEnum menuAndRoleErrorEnum) {
        super(menuAndRoleErrorEnum.getCode(), menuAndRoleErrorEnum.getMessage());
    }

    public static void check(boolean result, UserErrorEnum userErrorEnum) {
        if (!result) {
            throw new UserException(userErrorEnum);
        }
    }

    public static void check(boolean result, MenuAndRoleErrorEnum menuAndRoleErrorEnum) {
        if (!result) {
            throw new UserException(menuAndRoleErrorEnum);
        }
    }

}
