package com.stefanie.userserver.exception.sys;

import com.stefanie.userserver.common.enums.error.*;
import com.stefanie.userserver.exception.core.BeanException;
import io.swagger.annotations.ApiModel;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-07 22:18
 * @description 系统异常
 * 集成系统异常
 * Sql异常
 * 工具类异常
 */
@ApiModel(value = "异常-系统")
public class SysException extends BeanException {
    private static final long serialVersionUID = 6791853711532661548L;

    /**
     * 自定义异常
     */
    public SysException(Integer code, String message) {
        super(code, message);
    }

    /**
     * SQL异常
     */
    public SysException(SqlErrorEnum sqlErrorEnum) {
        super(sqlErrorEnum.getCode(), sqlErrorEnum.getMessage());
    }

    /**
     * 系统异常
     */
    public SysException(ErrorEnum errorEnum) {
        super(errorEnum.getCode(), errorEnum.getMessage());
    }

    /**
     * 工具类异常
     */
    public SysException(UtilErrorEnum utilErrorEnum) {
        super(utilErrorEnum.getCode(), utilErrorEnum.getMessage());
    }

    /**
     * 服务调用异常
     */
    public SysException(ServerCallErrorEnum serverCallErrorEnum) {
        super(serverCallErrorEnum.getCode(), serverCallErrorEnum.getMessage());
    }

    /**
     * 其他异常
     */
    public SysException(OtherErrorEnum otherErrorEnum) {
        super(otherErrorEnum.getCode(), otherErrorEnum.getMessage());
    }

    public static void check(boolean result, ErrorEnum errorEnum) {
        if (!result) {
            throw new SysException(errorEnum);
        }
    }

    public static void check(boolean result, UtilErrorEnum utilErrorEnum) {
        if (!result) {
            throw new SysException(utilErrorEnum);
        }
    }

    public static void check(boolean result, ServerCallErrorEnum serverCallErrorEnum) {
        if (!result) {
            throw new SysException(serverCallErrorEnum);
        }
    }

    public static void check(boolean result, OtherErrorEnum otherErrorEnum) {
        if (!result) {
            throw new SysException(otherErrorEnum);
        }
    }

    public static void check(boolean result, SqlErrorEnum sqlErrorEnum) {
        if (!result) {
            throw new SysException(sqlErrorEnum);
        }
    }

}
