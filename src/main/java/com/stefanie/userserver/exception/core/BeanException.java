package com.stefanie.userserver.exception.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zy
 * @version v1.0.1
 * @create 2023-01-06 16:10
 * @description 基础异常类
 */
@Data
@Slf4j
@ApiModel(value = "异常-公共")
@EqualsAndHashCode(callSuper = true)
public class BeanException extends RuntimeException {
    private static final long serialVersionUID = -8333875598740794493L;

    @ApiModelProperty(value = "错误码")
    private Integer code;

    @ApiModelProperty(value = "错误描述")
    private String message;

    public BeanException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static void check(boolean result, Integer code, String message) {
        if (!result) {
            throw new BeanException(code, message);
        }
    }

}
